(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*           Paolo Torrini      Grenoble-INP, VERIMAG                  *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Correctness proof for RISC-V generation: main proof. *)

Require Import Coqlib Errors.
Require Import Integers Floats AST Linking.
Require Import Values Memory Events Globalenvs Smallstep.
Require Import Op Locations Mach Conventions Asm.
Require Import Asmgen Asmgenproof0 Asmgenproof1.

Definition match_prog (p: Mach.program) (tp: Asm.program) :=
  match_program (fun _ f tf => transf_fundef f = OK tf) eq p tp.

Lemma transf_program_match:
  forall p tp, transf_program p = OK tp -> match_prog p tp.
Proof.
  intros. eapply match_transform_partial_program; eauto.
Qed.

Section PRESERVATION.

Variable prog: Mach.program.
Variable tprog: Asm.program.
Hypothesis TRANSF: match_prog prog tprog.
Let ge := Genv.globalenv prog.
Let tge := Genv.globalenv tprog.


(********************************************************************)

Definition is_ptr (rs: regset) (r: preg) : bool :=
  match (rs#r) with
  | Vptr b ofs => true
  | _          => false
  end.

Definition is_pointer (rs: regset) (r: preg) : Prop :=
  (is_ptr rs r) = true \/ (rs#r = Vnullptr).

(********************************************************************)

Inductive msk_valid_state : state -> Prop :=
| is_msk_valid_state : forall rs m,
    valid_mask_at_pc (rs PC) (rs MSK_CNT) -> msk_valid_state (State rs m).


(********************************************************************)
(* Require Import Coq.Program.Equality.  *)

Lemma destruct_exec_straight_dec : forall tge tf fb k0 k1 rs0 m0 rs1 m1,
    exec_straight tge tf fb k0 rs0 m0 k1 rs1 m1 ->
    (length k0 > length k1)%nat.
  intros.
  induction H.
  eauto.
  eapply gt_trans; eauto.
  eauto.
Qed.

Lemma destruct_exec_straight1 : forall tge tf fb k i rs0 m0 rs1 m1,
   exec_straight tge tf fb (i :: k) rs0 m0 k rs1 m1 ->
   exec_instr tge fb tf i rs0 m0 = Next rs1 m1.
Proof.
  intros.
  inversion H; subst.
  exact H3.
  eapply destruct_exec_straight_dec in H12.
  intuition.
Qed.


Lemma exec_straight_opt_trans:
  forall tge tf blk c1 rs1 m1 c2 rs2 m2 c3 rs3 m3,
  exec_straight_opt tge tf blk c1 rs1 m1 c2 rs2 m2 ->
  exec_straight_opt tge tf blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight_opt tge tf blk c1 rs1 m1 c3 rs3 m3.
Proof.
  intros.
  inversion H; subst.
  inversion H0; subst.
  econstructor.
  econstructor; auto.
  inversion H0; subst.
  econstructor; auto.
  econstructor.
  eapply  exec_straight_trans.
  exact H1.
  auto.
Qed.


Lemma exec_straight_opt_trans1:
  forall tge tf blk c1 rs1 m1 c2 rs2 m2 c3 rs3 m3,
  exec_straight_opt tge tf blk c1 rs1 m1 c2 rs2 m2 ->
  exec_straight tge tf blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight tge tf blk c1 rs1 m1 c3 rs3 m3.
Proof.
  intros.
  inversion H; subst.
  auto.
  eapply exec_straight_trans; eauto.
Qed.

Lemma exec_straight_opt_trans2:
  forall tge tf blk c1 rs1 m1 c2 rs2 m2 c3 rs3 m3,
  exec_straight tge tf blk c1 rs1 m1 c2 rs2 m2 ->
  exec_straight_opt tge tf blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight tge tf blk c1 rs1 m1 c3 rs3 m3.
Proof.
  intros.
  inversion H0; subst.
  auto.
  eapply exec_straight_trans; eauto.
Qed.


(********************************************************************)

Lemma symbols_preserved:
  forall (s: ident), Genv.find_symbol tge s = Genv.find_symbol ge s.
eapply (Genv.find_symbol_match TRANSF).
Qed.

Lemma senv_preserved:
  Senv.equiv ge tge.
 eapply (Genv.senv_match TRANSF).
Qed.

Lemma functions_translated:
  forall b f,
  Genv.find_funct_ptr ge b = Some f ->
  exists tf,
  Genv.find_funct_ptr tge b = Some tf /\ transf_fundef f = OK tf.
Proof (Genv.find_funct_ptr_transf_partial TRANSF).


Lemma functions_transl:
  forall fb f tf,
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  transf_function f = OK tf ->
  Genv.find_funct_ptr tge fb = Some (Internal tf).
Proof.
  intros. exploit functions_translated; eauto. intros [tf' [A B]].
  monadInv B. rewrite H0 in EQ; inv EQ; auto.
Qed.

Lemma functions_transl_ext:
  forall fb f1,
  Genv.find_funct_ptr ge fb = Some (External f1) ->
  Genv.find_funct_ptr tge fb = Some (External f1).
Proof.
  intros. exploit functions_translated; eauto. intros [tf' [A B]].
  monadInv B. auto.
Qed.


(** * Properties of control flow *)

Lemma transf_function_no_overflow:
  forall f tf,
    transf_function f = OK tf ->
    list_length_z tf.(fn_code) <= Ptrofs.max_unsigned.
Proof.
  intros. monadInv H.
  destruct (zlt Ptrofs.max_unsigned (list_length_z x.(fn_code))); inv EQ0.
  omega.
Qed.

Lemma exec_straight_exec:
  forall fb f c ep tf tc c' rs m rs' m',
  transl_code_at_pc ge (rs PC) fb f c ep tf tc ->
  exec_straight tge tf fb tc rs m c' rs' m' ->
  plus step tge (State rs m) E0 (State rs' m').
Proof.
  intros. inv H.
  eapply exec_straight_steps_1; eauto.
  eapply transf_function_no_overflow; eauto.
  eapply functions_transl. eauto. auto.
Qed.

Lemma exec_straight_at:
  forall blk fb f c ep tf tc c' ep' tc' rs m rs' m',
  transl_code_at_pc ge (rs PC) fb f c ep tf tc ->
  transl_code f c' ep' = OK tc' ->
  exec_straight tge tf blk tc rs m tc' rs' m' ->
  transl_code_at_pc ge (rs' PC) fb f c' ep' tf tc'.
Proof.
  intros. inv H.
  exploit exec_straight_steps_2; eauto.
  eapply transf_function_no_overflow; eauto.
  eapply functions_transl; eauto.
  intros [ofs' [PC' CT']].
  rewrite PC'. constructor; auto.
Qed.


(** The following lemmas show that the translation from Mach to Asm
  preserves labels, in the sense that the following diagram commutes:
<<
                          translation
        Mach code ------------------------ Asm instr sequence
            |                                          |
            | Mach.find_label lbl       find_label lbl |
            |                                          |
            v                                          v
        Mach code tail ------------------- Asm instr seq tail
                          translation
>>
  The proof demands many boring lemmas showing that Asm constructor
  functions do not introduce new labels.
*)

Section TRANSL_LABEL.

Remark loadimm32_label:
  forall r n k, tail_nolabel k (loadimm32 r n k).
Proof.
  intros; unfold loadimm32. destruct (make_immed32 n); TailNoLabel.
  unfold load_hilo32. destruct (Int.eq lo Int.zero); TailNoLabel.
Qed.
Hint Resolve loadimm32_label: labels.

Remark opimm32_label:
  forall op opimm r1 r2 n k,
  (forall r1 r2 r3, nolabel (op r1 r2 r3)) ->
  (forall r1 r2 n, nolabel (opimm r1 r2 n)) ->
  tail_nolabel k (opimm32 op opimm r1 r2 n k).
Proof.
  intros; unfold opimm32. destruct (make_immed32 n); TailNoLabel.
  unfold load_hilo32. destruct (Int.eq lo Int.zero); TailNoLabel.
Qed.
Hint Resolve opimm32_label: labels.

Remark loadimm64_label:
  forall r n k, tail_nolabel k (loadimm64 r n k).
Proof.
  intros; unfold loadimm64. destruct (make_immed64 n); TailNoLabel.
  unfold load_hilo64. destruct (Int64.eq lo Int64.zero); TailNoLabel.
Qed.
Hint Resolve loadimm64_label: labels.

Remark opimm64_label:
  forall op opimm r1 r2 n k,
  (forall r1 r2 r3, nolabel (op r1 r2 r3)) ->
  (forall r1 r2 n, nolabel (opimm r1 r2 n)) ->
  tail_nolabel k (opimm64 op opimm r1 r2 n k).
Proof.
  intros; unfold opimm64. destruct (make_immed64 n); TailNoLabel.
  unfold load_hilo64. destruct (Int64.eq lo Int64.zero); TailNoLabel.
Qed.
Hint Resolve opimm64_label: labels.

Remark addptrofs_label:
  forall r1 r2 n k, tail_nolabel k (addptrofs r1 r2 n k).
Proof.
  unfold addptrofs; intros. destruct (Ptrofs.eq_dec n Ptrofs.zero).
  TailNoLabel.
  destruct Archi.ptr64. apply opimm64_label; TailNoLabel.
  apply opimm32_label; TailNoLabel.
Qed.
Hint Resolve addptrofs_label: labels.

Remark transl_cond_float_nolabel:
  forall c r1 r2 r3 insn normal,
  transl_cond_float c r1 r2 r3 = (insn, normal) -> nolabel insn.
Proof.
  unfold transl_cond_float; intros. destruct c; inv H; exact I.
Qed.

Remark transl_cond_single_nolabel:
  forall c r1 r2 r3 insn normal,
  transl_cond_single c r1 r2 r3 = (insn, normal) -> nolabel insn.
Proof.
  unfold transl_cond_single; intros. destruct c; inv H; exact I.
Qed.

Remark transl_cbranch_label:
  forall cond args lbl k c,
  transl_cbranch cond args lbl k = OK c -> tail_nolabel k c.
Proof.
  intros. unfold transl_cbranch in H; destruct cond; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- destruct (Int.eq n Int.zero).
  destruct c0; simpl; TailNoLabel.
  apply tail_nolabel_trans with (transl_cbranch_int32s c0 x X31 lbl :: k).
  auto with labels. destruct c0; simpl; TailNoLabel.
- destruct (Int.eq n Int.zero).
  destruct c0; simpl; TailNoLabel.
  apply tail_nolabel_trans with (transl_cbranch_int32u c0 x X31 lbl :: k).
  auto with labels. destruct c0; simpl; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- destruct (Int64.eq n Int64.zero).
  destruct c0; simpl; TailNoLabel.
  apply tail_nolabel_trans with (transl_cbranch_int64s c0 x X31 lbl :: k).
  auto with labels. destruct c0; simpl; TailNoLabel.
- destruct (Int64.eq n Int64.zero).
  destruct c0; simpl; TailNoLabel.
  apply tail_nolabel_trans with (transl_cbranch_int64u c0 x X31 lbl :: k).
  auto with labels. destruct c0; simpl; TailNoLabel.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_float_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_float_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_single_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_single_nolabel; eauto.
  destruct normal; TailNoLabel.
Qed.

Remark transl_cond_op_label:
  forall cond args r k c,
  transl_cond_op cond r args k = OK c -> tail_nolabel k c.
Proof.
  intros. unfold transl_cond_op in H; destruct cond; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- unfold transl_condimm_int32s.
  destruct (Int.eq n Int.zero).
+ destruct c0; simpl; TailNoLabel.
+ destruct c0; simpl.
* eapply tail_nolabel_trans;
      [apply opimm32_label; intros; exact I | TailNoLabel].
* eapply tail_nolabel_trans;
      [apply opimm32_label; intros; exact I | TailNoLabel].
* apply opimm32_label; intros; exact I.
* destruct (Int.eq n (Int.repr Int.max_signed)).
  apply loadimm32_label. apply opimm32_label; intros; exact I.
* eapply tail_nolabel_trans. apply loadimm32_label. TailNoLabel.
* eapply tail_nolabel_trans. apply loadimm32_label. TailNoLabel.
- unfold transl_condimm_int32u.
  destruct (Int.eq n Int.zero).
+ destruct c0; simpl; TailNoLabel.
+ destruct c0; simpl;
  try (eapply tail_nolabel_trans; [apply loadimm32_label | TailNoLabel]).
  apply opimm32_label; intros; exact I.
- destruct c0; simpl; TailNoLabel.
- destruct c0; simpl; TailNoLabel.
- unfold transl_condimm_int64s.
  destruct (Int64.eq n Int64.zero).
+ destruct c0; simpl; TailNoLabel.
+ destruct c0; simpl.
* eapply tail_nolabel_trans;
      [apply opimm64_label; intros; exact I | TailNoLabel].
* eapply tail_nolabel_trans;
      [apply opimm64_label; intros; exact I | TailNoLabel].
* apply opimm64_label; intros; exact I.
* destruct (Int64.eq n (Int64.repr Int64.max_signed)).
  apply loadimm32_label. apply opimm64_label; intros; exact I.
* eapply tail_nolabel_trans. apply loadimm64_label. TailNoLabel.
* eapply tail_nolabel_trans. apply loadimm64_label. TailNoLabel.
- unfold transl_condimm_int64u.
  destruct (Int64.eq n Int64.zero).
+ destruct c0; simpl; TailNoLabel.
+ destruct c0; simpl;
  try (eapply tail_nolabel_trans; [apply loadimm64_label | TailNoLabel]).
  apply opimm64_label; intros; exact I.
- destruct (transl_cond_float c0 r x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_float_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_float c0 r x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_float_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_single c0 r x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_single_nolabel; eauto.
  destruct normal; TailNoLabel.
- destruct (transl_cond_single c0 r x x0) as [insn normal] eqn:F; inv EQ2.
  apply tail_nolabel_cons. eapply transl_cond_single_nolabel; eauto.
  destruct normal; TailNoLabel.
Qed.

Remark transl_op_label:
  forall op args r k c,
  transl_op op args r k = OK c -> tail_nolabel k c.
Proof.
Opaque Int.eq.
  unfold transl_op; intros; destruct op; TailNoLabel.
- destruct (preg_of r); try discriminate;
    destruct (preg_of m); inv H; TailNoLabel.
- destruct (Float.eq_dec n Float.zero); TailNoLabel.
- destruct (Float32.eq_dec n Float32.zero); TailNoLabel.
- destruct (Archi.pic_code tt && negb (Ptrofs.eq ofs Ptrofs.zero)).
+ eapply tail_nolabel_trans; [|apply addptrofs_label]. TailNoLabel.
+ TailNoLabel.
- apply opimm32_label; intros; exact I.
- apply opimm32_label; intros; exact I.
- apply opimm32_label; intros; exact I.
- apply opimm32_label; intros; exact I.
- destruct (Int.eq n Int.zero); TailNoLabel.
- apply opimm64_label; intros; exact I.
- apply opimm64_label; intros; exact I.
- apply opimm64_label; intros; exact I.
- apply opimm64_label; intros; exact I.
- destruct (Int.eq n Int.zero); TailNoLabel.
- eapply transl_cond_op_label; eauto.
Qed.

Remark indexed_memory_access_label:
  forall (mk_instr: ireg -> offset -> instruction) base ofs k,
  (forall r o, nolabel (mk_instr r o)) ->
  tail_nolabel k (indexed_memory_access mk_instr base ofs k).
Proof.
  unfold indexed_memory_access; intros.
  destruct Archi.ptr64.
  destruct (make_immed64 (Ptrofs.to_int64 ofs)); TailNoLabel.
  destruct (make_immed32 (Ptrofs.to_int ofs)); TailNoLabel.
Qed.

Remark loadind_label:
  forall base ofs ty dst k c,
  loadind base ofs ty dst k = OK c -> tail_nolabel k c.
Proof.
  unfold loadind; intros.
  destruct ty, (preg_of dst); inv H;
    apply indexed_memory_access_label; intros; exact I.
Qed.

Remark storeind_label:
  forall src base ofs ty k c,
  storeind src base ofs ty k = OK c -> tail_nolabel k c.
Proof.
  unfold storeind; intros.
  destruct ty, (preg_of src); inv H; apply indexed_memory_access_label;
    intros; exact I.
Qed.

Remark loadind_ptr_label:
  forall base ofs dst k, tail_nolabel k (loadind_ptr base ofs dst k).
Proof.
  intros. apply indexed_memory_access_label.
  intros; destruct Archi.ptr64; exact I.
Qed.

Remark storeind_ptr_label:
  forall src base ofs k, tail_nolabel k (storeind_ptr src base ofs k).
Proof.
  intros. apply indexed_memory_access_label.
  intros; destruct Archi.ptr64; exact I.
Qed.

Remark storeind_mskptr_label:
  forall src base ofs k, tail_nolabel k (storeind_mskptr src base ofs k).
Proof.
  intros. apply indexed_memory_access_label.
  intros; destruct Archi.ptr64; exact I.
Qed.

Remark transl_memory_access_label:
  forall (mk_instr: ireg -> offset -> instruction) addr args k c,
  (forall r o, nolabel (mk_instr r o)) ->
  transl_memory_access mk_instr addr args k = OK c ->
  tail_nolabel k c.
Proof.
  unfold transl_memory_access; intros; destruct addr;
    TailNoLabel; apply indexed_memory_access_label; auto.
Qed.


Remark make_epilogue_label:
  forall f m k, tail_nolabel k (make_epilogue f m k).
Proof.
  unfold make_epilogue; intros. eapply tail_nolabel_trans.
  apply loadind_ptr_label.

  unfold loadind_mskptr.
  unfold indexed_memory_access.

  remember Archi.ptr64 as A.
  destruct A.
  destruct (make_immed64
              (Ptrofs.to_int64 (fn_retmsk_ofs f))).

  TailNoLabel.
  TailNoLabel.
  TailNoLabel.

  destruct (make_immed32 (Ptrofs.to_int (fn_retmsk_ofs f))).

  TailNoLabel.
  TailNoLabel.
Qed.


Lemma transl_instr_label:
  forall f i ep k c,
  transl_instr f i ep k = OK c ->
  match i with Mlabel lbl => c = Plabel lbl :: k | _ => tail_nolabel k c end.
Proof.
  unfold transl_instr; intros; destruct i; TailNoLabel.
- eapply loadind_label; eauto.
- eapply storeind_label; eauto.
- destruct ep. eapply loadind_label; eauto.
  eapply tail_nolabel_trans. apply loadind_ptr_label.
  eapply loadind_label; eauto.
- eapply transl_op_label; eauto.
- destruct m; monadInv H; eapply transl_memory_access_label;
    eauto; intros; exact I.
- destruct m; monadInv H; eapply transl_memory_access_label;
    eauto; intros; exact I.
- destruct s0; monadInv H; TailNoLabel.
- destruct s0; monadInv H;
    (eapply tail_nolabel_trans; [eapply make_epilogue_label|TailNoLabel]).
- eapply transl_cbranch_label; eauto.
- eapply tail_nolabel_trans; [eapply make_epilogue_label|TailNoLabel].
Qed.


Lemma transl_instr_label':
  forall lbl f i ep k c,
  transl_instr f i ep k = OK c ->
  find_label lbl c = if Mach.is_label lbl i then Some k else find_label lbl k.
Proof.
  intros. exploit transl_instr_label; eauto.
  destruct i; try (intros [A B]; apply B).
  intros. subst c. simpl. auto.
Qed.

Lemma transl_code_label:
  forall lbl f c ep tc,
  transl_code f c ep = OK tc ->
  match Mach.find_label lbl c with
  | None => find_label lbl tc = None
  | Some c' => exists tc', find_label lbl tc = Some tc' /\
                           transl_code f c' false = OK tc'
  end.
Proof.
  induction c; simpl; intros.
  inv H. auto.
  monadInv H. rewrite (transl_instr_label' lbl _ _ _ _ _ EQ0).
  generalize (Mach.is_label_correct lbl a).
  destruct (Mach.is_label lbl a); intros.
  subst a. simpl in EQ. exists x; auto.
  eapply IHc; eauto.
Qed.

Lemma transl_find_label:
  forall lbl f tf,
  transf_function f = OK tf ->
  match Mach.find_label lbl f.(Mach.fn_code) with
  | None => find_label lbl tf.(fn_code) = None
  | Some c => exists tc, find_label lbl tf.(fn_code) = Some tc /\
                         transl_code f c false = OK tc
  end.
Proof.

  intros. monadInv H.
  destruct (zlt Ptrofs.max_unsigned (list_length_z x.(fn_code))); inv EQ0.
  monadInv EQ. rewrite transl_code'_transl_code in EQ0. unfold fn_code.
  simpl.

  remember (storeind_ptr_label X1 X2 (fn_retaddr_ofs f)
                    (storeind_mskptr RET_MSK X2 (fn_retmsk_ofs f) x)) as q.

  destruct q as [C D].

  rewrite D.

  remember (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f) x) as q.
  destruct q as [A B].

  rewrite B.
  eapply transl_code_label; eauto.
Qed.


End TRANSL_LABEL.

(***********************************************************************)

Lemma transl_find_label_proj1:
  forall lbl f tf c,
  transf_function f = OK tf ->
  Mach.find_label lbl (Mach.fn_code f) = Some c ->
  exists tc, find_label lbl tf.(fn_code) = Some tc.
  intros.
  exploit transl_find_label; eauto.
  instantiate (1:=lbl).
  rewrite H0.
  intros.
  destruct H1.
  destruct H1.
  econstructor; eauto.
Qed.

Lemma label_pos_code_tail_aux0:
  forall f tf lbl c pos,
  transf_function f = OK tf ->
  find_label lbl tf.(fn_code) = Some c ->
  exists pos',
  label_pos lbl pos (fn_code tf) = Some pos'.
Proof.
  intros.
  exploit label_pos_code_tail; eauto.
  intros.
  destruct H1.
  intuition.
  econstructor; eauto.
Qed.

Lemma label_pos_code_tail_aux1:
  forall f tf lbl c pos,
  transf_function f = OK tf ->
  Mach.find_label lbl (Mach.fn_code f) = Some c ->
  exists pos',
  label_pos lbl pos (fn_code tf) = Some pos'.
Proof.
  intros.
  exploit transl_find_label_proj1; eauto.
  intros.
  destruct H1.
  eapply label_pos_code_tail_aux0; eauto.
Qed.


(*********************************************************************)

Lemma label_pos_code_aux1:
  forall lbl c c',
  find_label lbl c = Some c' ->
  exists z,
  label_pos lbl 0 c = Some z
  /\ 0 < z <= list_length_z c.
Proof.
  intros.
  exploit label_pos_code_tail; eauto.
  instantiate (1:=0).
  simpl.
  intros [pos [A [B C ]]].
  econstructor 1 with (x:=pos).
  split; eauto.
Qed.

Lemma label_pos_code_aux2:
  forall f tf lbl c,
  transf_function f = OK tf ->
  find_label lbl tf.(fn_code) = Some c ->
  exists z,
  label_pos lbl 0 tf.(fn_code) = Some z
  /\ 0 < z <= Ptrofs.max_unsigned.
Proof.
  intros.
  eapply transf_function_no_overflow in H.
  exploit label_pos_code_aux1; eauto.
  intros [pos [A [B C ]]].
  econstructor 1 with (x:=pos); split; eauto.
  omega.
Qed.

Lemma label_pos_code_aux3:
  forall f tf lbl c,
  transf_function f = OK tf ->
  find_label lbl tf.(fn_code) = Some c ->
  exists z,
  label_pos lbl 0 tf.(fn_code) = Some z
  /\ 0 <= z <= Ptrofs.max_unsigned.
Proof.
  intros.
  eapply transf_function_no_overflow in H.
  exploit label_pos_code_aux1; eauto.
  intros [pos [A [B C ]]].
  econstructor 1 with (x:=pos); split; eauto.
  omega.
Qed.

Lemma label_pos_code_aux4:
  forall f tf lbl c z,
  transf_function f = OK tf ->
  find_label lbl tf.(fn_code) = Some c ->
  label_pos lbl 0 tf.(fn_code) = Some z ->
  0 <= z <= Ptrofs.max_unsigned.
Proof.
  intros.
  exploit label_pos_code_aux3; eauto.
  intros [pos [A B]].
  rewrite H1 in A.
  inversion A; subst; auto.
Qed.

Lemma label_pos_code_aux5:
  forall f tf lbl c z,
  transf_function f = OK tf ->
  Mach.find_label lbl (Mach.fn_code f) = Some c ->
  label_pos lbl 0 tf.(fn_code) = Some z ->
  0 <= z <= Ptrofs.max_unsigned.
Proof.
  intros.
  exploit transl_find_label_proj1; eauto. intros [pos' A].
  eapply label_pos_code_aux4; eauto.
Qed.


(***********************************************************************)

Lemma msk_preserveJ1 :
      forall ge fb f tf lbl ofs rs0 m0 rs1 m1 rs2 m2 c
  (I1 : rs0 PC = Vptr fb ofs)
  (I2 : rs0 MSK_CNT = Vint (encrypt_msk (Ptrofs.unsigned ofs) fb))
  (A1: transf_function f = OK tf)
  (A2: find_label lbl tf.(fn_code) = Some c),

  exec_instr ge fb tf (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs0 m0 =
         Next rs1 m1 ->
      exec_instr ge fb tf (Pj_l lbl) rs1 m1 =
         Next rs2 m2 ->
      valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT).
  intros.
  simpl in H.
  unfold exec_loadCodeMSK_byLabel in H.
  unfold exec_loadCodeMSK in H.
  simpl in H0.
  unfold goto_label in H0.

  remember (label_pos lbl 0 tf.(fn_code)) as z.

  destruct z.
  2: { inversion H0. }
  remember (rs1 PC) as k.
  destruct k.
  inversion H0.
  inversion H0.
  inversion H0.
  inversion H0.
  inversion H0.
  inversion H; subst.
  clear H.
  inversion H0; subst.
  clear H0.
  unfold nextinstrTail.
  Simpl.
  econstructor.
  reflexivity.
  unfold nextinstr in Heqk.
  rewrite Pregmap.gso in Heqk.
  rewrite Pregmap.gss in Heqk.
  rewrite Pregmap.gso in Heqk.
  unfold Val.offset_ptr in Heqk.
  rewrite I1 in Heqk.
  inversion Heqk; subst.
  f_equal.
  f_equal.

  rewrite Ptrofs.unsigned_repr. reflexivity.
  eapply label_pos_code_aux4; eauto.

  congruence.
  congruence.
Qed.

Lemma msk_preserveJ1a :
      forall ge fb f tf lbl ofs rs0 m0 rs1 m1 rs2 m2 c
  (I1 : rs0 PC = Vptr fb ofs)
  (I2 : rs0 MSK_CNT = Vint (encrypt_msk (Ptrofs.unsigned ofs) fb))
  (A1: transf_function f = OK tf)
  (A2: Mach.find_label lbl (Mach.fn_code f) = Some c),

  exec_instr ge fb tf (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs0 m0 =
         Next rs1 m1 ->
      exec_instr ge fb tf (Pj_l lbl) rs1 m1 =
         Next rs2 m2 ->
      valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT).
Proof.
  intros.
  exploit transl_find_label_proj1; eauto. intros [pos A].
  eapply msk_preserveJ1; eauto.
Qed.

Lemma msk_preserveJ3 :
      forall ge fb f lbl ofs rs0 m0 rs1 m1 rs2 m2 jmp pos tf c
  (A1: transf_function f = OK tf)
  (A2: find_label lbl tf.(fn_code) = Some c)
  (I1 : rs0 PC = Vptr fb ofs)
  (I2 : rs0 MSK_CNT = Vint (encrypt_msk (Ptrofs.unsigned ofs) fb))
  (L: label_pos lbl 0 tf.(fn_code) = Some pos)
  (R: rs2 = nextinstrTail rs1 (Vptr fb (Ptrofs.repr pos))),

      exec_instr ge fb tf (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs0 m0 =
         Next rs1 m1 ->
      exec_instr ge fb tf jmp rs1 m1 =
         Next rs2 m2 ->
      valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT).

  intros.
  simpl in H.
  unfold exec_loadCodeMSK_byLabel in H.
  unfold exec_loadCodeMSK in H.
  inversion R; subst.
  clear H1.
  rewrite L in H.
  inversion H; subst.
  clear H.
  econstructor.
  unfold nextinstrTail.
  Simpl.

  unfold nextinstrTail.
  Simpl.
  f_equal.
  f_equal.

  rewrite Ptrofs.unsigned_repr. reflexivity.
  eapply label_pos_code_aux4; eauto.
Qed.

Lemma msk_preserveJ3a :
      forall ge fb f lbl ofs rs0 m0 rs1 m1 rs2 m2 jmp pos tf c
  (A1: transf_function f = OK tf)
  (A2: Mach.find_label lbl (Mach.fn_code f) = Some c)
  (I1 : rs0 PC = Vptr fb ofs)
  (I2 : rs0 MSK_CNT = Vint (encrypt_msk (Ptrofs.unsigned ofs) fb))
  (L: label_pos lbl 0 tf.(fn_code) = Some pos)
  (R: rs2 = nextinstrTail rs1 (Vptr fb (Ptrofs.repr pos))),

      exec_instr ge fb tf (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs0 m0 =
         Next rs1 m1 ->
      exec_instr ge fb tf jmp rs1 m1 =
         Next rs2 m2 ->
      valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT).
Proof.
  intros.
  exploit transl_find_label_proj1; eauto. intros [pos' A].
  eapply msk_preserveJ3; eauto.
Qed.


(***********************************************************************)

(** A valid branch in a piece of Mach code translates to a valid ``go to''
  transition in the generated Asm code. *)

Lemma find_label_goto_label:
  forall f tf lbl rs m c' b ofs,
  Genv.find_funct_ptr ge b = Some (Internal f) ->
  transf_function f = OK tf ->
  rs PC = Vptr b ofs ->
  Mach.find_label lbl f.(Mach.fn_code) = Some c' ->
  exists tc', exists rs',
    goto_label tf lbl rs m = Next rs' m
  /\ transl_code_at_pc ge (rs' PC) b f c' false tf tc'
  /\ forall r, r <> PC -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros. exploit (transl_find_label lbl f tf); eauto. rewrite H2.
  intros [tc [A B]].
  exploit label_pos_code_tail; eauto. instantiate (1 := 0).
  intros [pos' [P [Q R]]].
  exists tc; exists (rs#PC <-
                      (Vptr b (Ptrofs.repr pos')) # MSK_CNT <- (rs MSK_BRN)).
  split. unfold goto_label. rewrite P. rewrite H1.
  unfold nextinstrTail. auto.
  split. rewrite Pregmap.gso. rewrite Pregmap.gss. constructor; auto.
  rewrite Ptrofs.unsigned_repr. replace (pos' - 0) with pos' in Q.
  auto. omega.
  generalize (transf_function_no_overflow _ _ H0). omega. discriminate.
  intros. rewrite Pregmap.gso. apply Pregmap.gso; auto. assumption.
Qed.


(** Existence of return addresses *)

Lemma return_address_exists:
  forall f sg ros c, is_tail (Mcall sg ros :: c) f.(Mach.fn_code) ->
  exists ra, return_address_offset f c ra.
Proof.
  intros. eapply Asmgenproof0.return_address_exists; eauto.
- intros. exploit transl_instr_label; eauto.
  destruct i; try (intros [A B]; apply A). intros. subst c0.
  repeat constructor.
- intros. monadInv H0.
  destruct (zlt Ptrofs.max_unsigned (list_length_z x.(fn_code))); inv EQ0.
  monadInv EQ.
  rewrite transl_code'_transl_code in EQ0.
  exists x; exists true; split; auto. unfold fn_code.
  constructor.

  unfold storeind_ptr.
  unfold indexed_memory_access.

  destruct Archi.ptr64.
  destruct (make_immed64
              (Ptrofs.to_int64 (fn_retaddr_ofs f0))).

  constructor.

  apply (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f0) x).

  constructor.
  constructor.
  constructor.

  apply (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f0) x).

  constructor.
  constructor.
  constructor.

  apply (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f0) x).

  destruct (make_immed32 (Ptrofs.to_int (fn_retaddr_ofs f0))).

  constructor.

  apply (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f0) x).

  constructor.
  constructor.
  constructor.

  apply (storeind_mskptr_label RET_MSK X2 (fn_retmsk_ofs f0) x).

- exact transf_function_no_overflow.
Qed.

Lemma return_mask_exists :
     forall fb ofs, exists rm : Values.val, return_mask fb ofs rm.
  intros.
  econstructor 1 with (x:= Vint (encrypt_msk (Ptrofs.intval ofs) fb)).
  unfold Asmgenproof0.return_mask.
  reflexivity.
Qed.

Lemma return_mask_has_type : forall fb ofs rm,
    return_mask fb ofs rm ->
    exists i, rm = Vint i.
  intros.
  unfold return_mask in H.
  econstructor; eauto.
Qed.

(** * Proof of semantic preservation *)

(** Semantic preservation is proved using simulation diagrams
  of the following form.
<<
           st1 --------------- st2
            |                   |
           t|                  *|t
            |                   |
            v                   v
           st1'--------------- st2'
>>
  The invariant is the [match_states] predicate below, which includes:
- The Asm code pointed by the PC register is the translation of
  the current Mach code sequence.
- Mach register values and Asm register values agree.
*)

Inductive match_states: Mach.state -> Asm.state -> Prop :=
| match_states_intro:
    forall s fb sp c ep ms m m' rs f tf tc
    (STACKS: match_stack ge s)
    (FIND: Genv.find_funct_ptr ge fb = Some (Internal f))
    (MEXT: Mem.extends m m')
    (AT: transl_code_at_pc ge (rs PC) fb f c ep tf tc)
    (VALID_MASK: valid_mask_at_pc (rs PC) (rs MSK_CNT))
    (AG: agree ms sp rs)
    (DXP: ep = true -> rs#X30 = parent_sp s),
    match_states (Mach.State s fb sp c ms m)
                 (Asm.State rs m')
| match_states_call:
    forall s fb ms m m' rs
    (STACKS: match_stack ge s)
    (MEXT: Mem.extends m m')
    (AG: agree ms (parent_sp s) rs)
    (ATPC: rs PC = Vptr fb Ptrofs.zero)
    (VALID_MASK: valid_mask_at_pc (rs PC) (rs MSK_CNT))
(*    (ATMASK_COUNTER: valid_mask_at_pc (rs PC) (rs MSK_CNT))  *)
    (ATLR: rs RA = parent_ra s)
    (ATRM: rs RET_MSK = parent_msk s),
    match_states (Mach.Callstate s fb ms m) (Asm.State rs m')
| match_states_return:
    forall s ms m m' rs
    (STACKS: match_stack ge s)
    (MEXT: Mem.extends m m')
    (AG: agree ms (parent_sp s) rs)
    (ATPC: rs PC = parent_ra s)
    (ATMC: rs MSK_CNT = parent_msk s)
    (VALID_MASK_WK: valid_mask_at_reg (rs PC) (rs MSK_CNT)),
(*    (ATMASK_COUNTER: valid_mask_at_reg (rs PC) (rs MSK_CNT)),  *)
    match_states (Mach.Returnstate s ms m) (Asm.State rs m').


(********************************************************************)

(** We need to show that, in the simulation diagram, we cannot
  take infinitely many Mach transitions that correspond to zero
  transitions on the Asm side.  Actually, all Mach transitions
  correspond to at least one Asm transition, except the
  transition from [Machsem.Returnstate] to [Machsem.State].
  So, the following integer measure will suffice to rule out
  the unwanted behaviour. *)

Definition measure (s: Mach.state) : nat :=
  match s with
  | Mach.State _ _ _ _ _ _ => 0%nat
  | Mach.Callstate _ _ _ _ => 0%nat
  | Mach.Returnstate _ _ _ => 1%nat
  end.

Remark preg_of_not_X30: forall r, negb (mreg_eq r R30) = true ->
                                  IR X30 <> preg_of r.
Proof.
  intros. change (IR X30) with (preg_of R30). red; intros.
  exploit preg_of_injective; eauto. intros; subst r; discriminate.
Qed.

Lemma exec_loadCode_aux2A : forall fb0 tf fid rs0 m' f',
  Genv.find_symbol ge fid = Some f' ->
  exec_instr tge fb0 tf
                     (LoadCodeMSK_name MSK_BRN (MSK_ident fid)) rs0 m' =
  Next  (nextinstr rs0 # MSK_BRN <- (Vint (generate_mskA f'))) m'.
  intros.
  simpl.

  unfold exec_loadCodeMSK_byIdent.
  rewrite symbols_preserved.
  rewrite H.
  unfold exec_loadCodeMSK.
  reflexivity.
Qed.

(********************************************************************)

Lemma exec_straight_steps:
  forall s fb f rs1 i c ep tf tc m1' m2 m2' sp ms2
  (K: valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)),
  match_stack ge s ->
  Mem.extends m2 m2' ->
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  transl_code_at_pc ge (rs1 PC) fb f (i :: c) ep tf tc ->
  (forall k k1 (TR: transl_instr f i ep k = OK k1),
   exists rs2,
       exec_straight tge tf fb k1 rs1 m1' k rs2 m2'
    /\ agree ms2 sp rs2
    /\ (it1_is_parent ep i = true -> rs2#X30 = parent_sp s)) ->
  exists st',
  plus step tge (State rs1 m1') E0 st' /\
  match_states (Mach.State s fb sp c ms2 m2) st'.
Proof.
  intros. inversion H2. subst. monadInv H7.
  exploit H3; eauto. intros [rs2 [A [B C]]].
  exists (State rs2 m2'); split.
  eapply exec_straight_exec; eauto.
  econstructor; eauto. eapply exec_straight_at; eauto.
  eapply msk_straight_preserve.
  exact K. eauto.
Qed.

Lemma exec_straight_steps1:
  forall s fb f rs1 i c ep tf tc m1' m2 m2' sp ms2 k
         (K: valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT))
         (T: transl_code f c false = OK k)
         (W: it1_is_parent ep i = false),
  match_stack ge s ->
  Mem.extends m2 m2' ->
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  transl_code_at_pc ge (rs1 PC) fb f (i :: c) ep tf tc ->
  (forall k1 (TR: transl_instr f i ep k = OK k1),
   exists rs2,
       exec_straight tge tf fb k1 rs1 m1' k rs2 m2'
    /\ agree ms2 sp rs2
    /\ (it1_is_parent ep i = true -> rs2#X30 = parent_sp s)) ->
  exists st',
  plus step tge (State rs1 m1') E0 st' /\
  match_states (Mach.State s fb sp c ms2 m2) st'.
Proof.
  intros. inversion H2; subst. monadInv H7.
  rewrite W in EQ.
  rewrite EQ in T.
  inversion T; subst.

  exploit H3; eauto. intros [rs2 [A [B C]]].
  exists (State rs2 m2'); split.
  eapply exec_straight_exec; eauto.
  econstructor; eauto. eapply exec_straight_at; eauto.
  rewrite W. auto.
  eapply msk_straight_preserve.
  exact K. eauto.
Qed.

(********************************************************************)

(* the translation of a Mach jump instruction can be split into a part
that can be executed in a straight way, and in a jump that does not
affect the Mach state. given Asm continuation k, the Mach instruction
i translates to k1. k1 transitions straightly to jmp::k'. the Asm
state st', that can be instantiated with that obtained by executing
jmp, matches the Mach state associated with c' (the Mach continuation
after the jump). *)

Lemma exec_straight_steps_goto7:
  forall s fb f rs1 i c ep tf tc m1' m2 m2' sp ms2 lbl c' k
         (K: valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT))
         (T: transl_code f c false = OK k),

  match_stack ge s ->
  Mem.extends m2 m2' ->
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  Mach.find_label lbl f.(Mach.fn_code) = Some c' ->
  transl_code_at_pc ge (rs1 PC) fb f
                 (i :: c) ep tf tc ->
  it1_is_parent ep i = false ->

  (forall k1 (TR: transl_instr f i ep k = OK k1),
   exists k', exists rs2, exists rs3,
    exec_straight tge tf fb k1 rs1 m1'
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: (Pj_l lbl) :: k')
                  rs2 m2'
    /\ agree ms2 sp rs2
    /\ exec_straight tge tf fb
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: (Pj_l lbl) :: k')
                     rs2 m2'
            ((Pj_l lbl) :: k') rs3 m2'
    /\ agree ms2 sp rs3
    /\ exec_instr tge fb tf (Pj_l lbl) rs3 m2' = goto_label tf lbl rs3 m2') ->
  exists st',
  plus step tge (State rs1 m1') E0 st' /\
  match_states (Mach.State s fb sp c' ms2 m2) st'.
Proof.
  intros. inversion H3; subst. monadInv H9.
  rewrite H4 in EQ.
  rewrite EQ in T.
  inversion T; subst. clear T.

  exploit H5; eauto.
  intros [k' [rs2 [rs3 [A [B C]]]]].
  generalize (functions_transl _ _ _ H7 H8); intro FN.
  generalize (transf_function_no_overflow _ _ H8); intro NOOV.

  exploit exec_straight_steps_2; eauto.
  intros [ofs2 [PC2 CT2]].

  destruct C as [C D].
  destruct D as [D E].

  exploit exec_straight_steps_2; eauto.
  intros [ofs3 [PC3 CT3]].

  assert (valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT)) as K2.
  eapply msk_straight_preserve.
  exact K.
  eauto.

  assert (valid_mask_at_pc (rs3 PC) (rs3 MSK_CNT)) as K3.
  eapply msk_straight_preserve.
  exact K2.
  eauto.

  exploit find_label_goto_label; eauto.
  intros [tc' [rs4 [GOTO [AT' OTH]]]].
  instantiate (1:=m2') in GOTO.

  assert (exec_instr tge fb tf (Pj_l lbl) rs3 m2' = Next rs4 m2') as K4.
  rewrite E. eexact GOTO.

  exists (State rs4 m2'); split.

  eapply plus_right' with (s2 := State rs3 m2').

  assert (exec_straight tge tf fb tc rs1 m1'
                        ((Pj_l lbl) :: k') rs3 m2') as K5.
  eapply exec_straight_trans; eauto.

  eapply exec_straight_steps_1; eauto.

  econstructor; eauto.
  eapply find_instr_tail. eauto.
  traceEq.

  econstructor; eauto.
  inversion K2; subst.
  rewrite PC2 in H9.
  inversion H9; subst.

  eapply msk_preserveJ1a.

  5: { eapply destruct_exec_straight1; eauto. }
  eauto.
  auto.
  eauto.
  eauto.

  eexact K4.

  apply agree_exten with rs3; auto with asmgen.
  congruence.
Qed.


Lemma exec_straight_opt_steps_goto7:
  forall s fb f rs1 i c ep tf tc m1' m2 m2' sp ms2 lbl c' k
         (K: valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT))
         (T: transl_code f c false = OK k),

  match_stack ge s ->
  Mem.extends m2 m2' ->
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  Mach.find_label lbl f.(Mach.fn_code) = Some c' ->
  transl_code_at_pc ge (rs1 PC) fb f
                 (i :: c) ep tf tc ->
  it1_is_parent ep i = false ->

  (forall k1 (TR: transl_instr f i ep k = OK k1),
   exists k', exists rs2, exists rs3,
    exec_straight_opt tge tf fb k1 rs1 m1'
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: (Pj_l lbl) :: k')
                  rs2 m2'
    /\ agree ms2 sp rs2
    /\ exec_straight_opt tge tf fb
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: (Pj_l lbl) :: k')
                     rs2 m2'
            ((Pj_l lbl) :: k') rs3 m2'
    /\ agree ms2 sp rs3
    /\ exec_instr tge fb tf (Pj_l lbl) rs3 m2' = goto_label tf lbl rs3 m2') ->
  exists st',
  plus step tge (State rs1 m1') E0 st' /\
  match_states (Mach.State s fb sp c' ms2 m2) st'.
Proof.
  intros. inversion H3; subst. monadInv H9.
  rewrite H4 in EQ.
  rewrite EQ in T.
  inversion T; subst. clear T.

  exploit H5; eauto.
  intros [k' [rs2 [rs3 [A [B C]]]]].
  generalize (functions_transl _ _ _ H7 H8); intro FN.
  generalize (transf_function_no_overflow _ _ H8); intro NOOV.

  assert (valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT)) as K2.
  eapply msk_straight_opt_preserve.
  exact K.
  eauto.

  destruct C as [C D].
  destruct D as [D E].

  assert (valid_mask_at_pc (rs3 PC) (rs3 MSK_CNT)) as K3.
  eapply msk_straight_opt_preserve.
  exact K2.
  eauto.

  inversion A; subst.

- clear A.

  inversion C; subst.

  exploit exec_straight_steps_2; eauto.
  intros [ofs3 [PC3 CT3]].

  exploit find_label_goto_label; eauto.
  intros [tc' [rs4 [GOTO [AT' OTH]]]].
  instantiate (1:=m2') in GOTO.

  assert (exec_instr tge fb tf (Pj_l lbl) rs3 m2' = Next rs4 m2') as K4.
  rewrite E. eexact GOTO.

  exists (State rs4 m2'); split.

  eapply plus_right' with (s2 := State rs3 m2').
  eapply exec_straight_steps_1; eauto.

  econstructor; eauto.
  eapply find_instr_tail. eauto.
  traceEq.

  econstructor; eauto.

  inversion K2; subst.
  rewrite H11 in H6.
  inversion H6; subst.

  eapply msk_preserveJ1a.

  5: { eapply destruct_exec_straight1; eauto. }
  eauto.
  eauto.
  eauto.
  eauto.
  eexact K4.

  apply agree_exten with rs3; auto with asmgen.
  congruence.

- exploit exec_straight_steps_2; eauto.
  intros [ofs2 [PC2 CT2]].

  inversion C; subst.

  exploit exec_straight_steps_2; eauto.
  intros [ofs3 [PC3 CT3]].

  exploit find_label_goto_label; eauto.
  intros [tc' [rs4 [GOTO [AT' OTH]]]].
  instantiate (1:=m2') in GOTO.

  assert (exec_instr tge fb tf (Pj_l lbl) rs3 m2' = Next rs4 m2') as K4.
  rewrite E. eexact GOTO.

  exists (State rs4 m2'); split.

  eapply plus_right' with (s2 := State rs3 m2').

  assert (exec_straight tge tf fb tc rs1 m1'
                        ((Pj_l lbl) :: k') rs3 m2') as K5.
  eapply exec_straight_trans; eauto.

  eapply exec_straight_steps_1; eauto.

  econstructor; eauto.
  eapply find_instr_tail. eauto.
  traceEq.

  econstructor; eauto.

  inversion K2; subst.
  rewrite PC2 in H12.
  inversion H12; subst.

  eapply msk_preserveJ1a.

  5: { eapply destruct_exec_straight1; eauto. }
  eauto.
  eauto.
  eauto.
  eauto.
  eexact K4.

  apply agree_exten with rs3; auto with asmgen.
  congruence.
Qed.


Lemma exec_straight_opt_steps_goto11:
  forall s fb f rs1 i c ep tf tc m1' m2 m2' sp ms2 lbl c' k
         (K: valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT))
         (T: transl_code f c false = OK k),
  match_stack ge s ->
  Mem.extends m2 m2' ->
  Genv.find_funct_ptr ge fb = Some (Internal f) ->
  Mach.find_label lbl f.(Mach.fn_code) = Some c' ->
  transl_code_at_pc ge (rs1 PC) fb f
                 (i :: c) ep tf tc ->
  it1_is_parent ep i = false ->

  (forall k1 (TR: transl_instr f i ep k = OK k1),
      exists pos,
        exists jmp, exists k2, exists k3, exists rs2, exists rs3,
                  exists rs4,

     label_pos lbl 0 (fn_code tf) = Some pos

     /\ rs3 = (nextinstr rs2 # MSK_BRN <- (Vint (encrypt_msk pos fb)))
     /\ exec_straight_opt tge tf fb k1 rs1 m1'
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: k2)
                  rs2 m2'
    /\ agree ms2 sp rs2
    /\ exec_straight tge tf fb
           (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: k2)
           rs2 m2' k2 rs3 m2'
    /\ agree ms2 sp rs3
    /\ exec_straight_opt tge tf fb k2 rs3 m2' (jmp :: k3) rs4 m2'
    /\ agree ms2 sp rs4
    /\ rs3 MSK_BRN = rs4 MSK_BRN
    /\ exec_instr tge fb tf jmp rs4 m2' = goto_label tf lbl rs4 m2') ->
  exists st',
  plus step tge (State rs1 m1') E0 st' /\
  match_states (Mach.State s fb sp c' ms2 m2) st'.
Proof.
  intros. inversion H3; subst. monadInv H9.
  rewrite H4 in EQ.
  rewrite EQ in T.
  inversion T; subst. clear T.

  exploit H5; eauto.
  intros [pos [jmp [k2 [k3 [rs2 [rs3 [rs4 [G1 [G2 [A B]]]]]]]]]].

  generalize (functions_transl _ _ _ H7 H8); intro FN.
  generalize (transf_function_no_overflow _ _ H8); intro NOOV.

  assert (valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT)) as K2.
  eapply msk_straight_opt_preserve; eauto.

  destruct B as [B1 B2].
  destruct B2 as [C D].

  assert (valid_mask_at_pc (rs3 PC) (rs3 MSK_CNT)) as K3.
  eapply msk_straight_preserve; eauto.

  generalize C. intro C_rem.
  eapply destruct_exec_straight1 in C.
  simpl in C.
  unfold exec_loadCodeMSK_byLabel in C.
  rewrite G1 in C.
  unfold exec_loadCodeMSK in C.
  inversion C; subst.

  set (rs3 := nextinstr rs2 # MSK_BRN <- (Vint (encrypt_msk pos fb))).

  destruct D as [D1 D2].
  destruct D2 as [D2 D3].

  assert (valid_mask_at_pc (rs4 PC) (rs4 MSK_CNT)) as K4.
  eapply msk_straight_opt_preserve; eauto.

  assert (exec_straight_opt tge tf fb tc rs1 m1' (jmp :: k3) rs4 m2') as K5.
  eapply exec_straight_opt_trans; eauto.

  eapply exec_straight_opt_trans.
  eapply exec_straight_opt_intro.
  exact C_rem. exact D2.


  assert (exec_straight tge tf fb tc rs1 m1' (jmp :: k3) rs4 m2') as K5_doub.
  eapply exec_straight_opt_trans1. eexact A.

  eapply exec_straight_opt_trans2.
  exact C_rem. exact D2.

  exploit (exec_straight_opt_steps_2a _ _ _ _ _ _ _ _ _ K5); eauto.
  intros [ofs4 [PC4 CT4]].

  exploit find_label_goto_label; eauto.
  intros [tc' [rs5 [GOTO [AT' OTH]]]].
  instantiate (1:=m2') in GOTO.

  destruct D3 as [D3 D4].
  destruct D4 as [D4 D5].

  assert (exec_instr tge fb tf jmp rs4 m2' = Next rs5 m2') as K6.
  rewrite D5. eexact GOTO.

  exists (State rs5 m2'); split.

  eapply plus_right' with (s2 := State rs4 m2').

  eapply exec_straight_steps_1; eauto.

  unfold goto_label in GOTO.
  rewrite G1 in GOTO.
  rewrite PC4 in GOTO.

  econstructor; eauto.
  eapply find_instr_tail. eauto.
  traceEq.

  unfold goto_label in GOTO.
  rewrite G1 in GOTO.
  rewrite PC4 in GOTO.

  unfold nextinstrTail in GOTO.
  inversion GOTO; subst.

  econstructor; eauto.
  econstructor.

  Simpl.
  rewrite Pregmap.gss.
  rewrite <- D4.
  Simpl.

  f_equal.
  f_equal.
  rewrite Ptrofs.unsigned_repr. reflexivity.
  eapply label_pos_code_aux5; eauto.

  apply agree_exten with rs4; auto with asmgen.
  congruence.
Qed.


(***********************************************************************)

Lemma Mcond_false_case
  (s : list stackframe)
  (fb : Values.block)
  (sp : val)
  (cond : condition)
  (args : list RegEq.t)
  (lbl : Mach.label)
  (c : list Mach.instruction)
  (rs : RegEq.t -> val)
  (m : mem)
  (H : eval_condition cond (map rs args) m = Some false)
  (ep : bool)
  (m' : mem)
  (rs0 : preg -> val)
  (f0 : Mach.function)
  (tf : function)
  (tc : code)
  (STACKS : match_stack ge s)
  (FIND : Genv.find_funct_ptr ge fb = Some (Internal f0))
  (MEXT : Mem.extends m m')
  (AT : transl_code_at_pc ge (rs0 PC) fb f0 (Mcond cond args lbl :: c)
                          ep tf tc)
  (VALID_MASK : valid_mask_at_pc (rs0 PC) (rs0 MSK_CNT))
  (AG : agree rs sp rs0)
  (DXP : ep = true -> rs0 X30 = parent_sp s) :

  (exists S2' : state,
     plus step tge (State rs0 m') E0 S2' /\
     match_states
       (Mach.State s fb sp c (Mach.undef_regs (destroyed_by_cond cond) rs) m)
       S2') \/
  (measure
     (Mach.State s fb sp c (Mach.undef_regs (destroyed_by_cond cond) rs) m) <
   measure (Mach.State s fb sp (Mcond cond args lbl :: c) rs m))%nat /\
  E0 = E0 /\
  match_states
    (Mach.State s fb sp c (Mach.undef_regs (destroyed_by_cond cond) rs) m)
    (State rs0 m').

  exploit eval_condition_lessdef.
  eapply preg_vals; eauto. eauto. eauto.
  intros EC.

  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
    eapply transf_function_no_overflow; eauto.

  generalize H3; intro H3_rem.

  monadInv H3.

  assert (it1_is_parent ep (Mcond cond args lbl) = false) as qq.
  eauto.

  generalize (code_tail_next_int _ _ _ _ NOOV H4). intro CT1.

  left.

  eapply exec_straight_steps1; eauto.
  intros.
  simpl in TR.

  rename x into k0.

  exploit transl_cbranch_correct_false2; eauto.

  intros (rs' & A & B).

  exists rs'.
  split.

  unfold bind in TR.
  rewrite EQ1 in TR.
  inversion TR; subst.
  eexact A.

  split. apply agree_exten with rs0; auto with asmgen.

  intros.
  intuition.
Qed.


Lemma Mcond_true_case2
  (s : list stackframe)
  (fb : Values.block)
  (f : Mach.function)
  (sp : val)
  (cond : condition)
  (args : list RegEq.t)
  (lbl : Mach.label)
  (c : list Mach.instruction)
  (rs : RegEq.t -> val)
  (m : mem)
  (c' : Mach.code)
  (H : eval_condition cond (map rs args) m = Some true)
  (H0 : Genv.find_funct_ptr ge fb = Some (Internal f))
  (H1 : Mach.find_label lbl (Mach.fn_code f) = Some c')
  (ep : bool)
  (m' : mem)
  (rs0 : preg -> val)
  (f0 : Mach.function)
  (tf : function)
  (tc : code)
  (STACKS : match_stack ge s)
  (FIND : Genv.find_funct_ptr ge fb = Some (Internal f0))
  (MEXT : Mem.extends m m')
  (AT : transl_code_at_pc ge (rs0 PC) fb f0 (Mcond cond args lbl :: c)
                          ep tf tc)
  (VALID_MASK : valid_mask_at_pc (rs0 PC) (rs0 MSK_CNT))
  (AG : agree rs sp rs0)
  (DXP : ep = true -> rs0 X30 = parent_sp s) :
  (exists S2' : state,
     plus step tge (State rs0 m') E0 S2' /\
     match_states
       (Mach.State s fb sp c' (Mach.undef_regs (destroyed_by_cond cond) rs) m)
       S2') \/
  (measure
     (Mach.State s fb sp c' (Mach.undef_regs (destroyed_by_cond cond) rs) m) <
   measure (Mach.State s fb sp (Mcond cond args lbl :: c) rs m))%nat /\
  E0 = E0 /\
  match_states
    (Mach.State s fb sp c' (Mach.undef_regs (destroyed_by_cond cond) rs) m)
    (State rs0 m').

  assert (f0 = f) by congruence. subst f0.

  exploit eval_condition_lessdef.
  eapply preg_vals; eauto. eauto. eauto.
  intros EC.

  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
    eapply transf_function_no_overflow; eauto.

  generalize H5; intro H5_rem.

  monadInv H5.

  assert (it1_is_parent ep (Mcond cond args lbl) = false) as qq.
  eauto.

  generalize (code_tail_next_int _ _ _ _ NOOV H6). intro CT1.

  left.

  eapply exec_straight_opt_steps_goto11; eauto.
  intros.
  simpl in TR.

  unfold bind in TR.
  rewrite EQ1 in TR.
  inversion TR; subst.

  rename x into k0.
  rename x0 into k1.

  exploit label_pos_code_tail_aux1; eauto.
  intros.
  destruct H5.
  (***)

  exploit transl_cbranch_correct_trueB; eauto.

  instantiate (1:=fb).
  instantiate (2:=tge).
  set (rs3 := nextinstr rs0 # MSK_BRN <- (Vint (encrypt_msk x fb))).

  intros (rs1' & rs4 & jmp & A1 & A2 & E & B & C).
  inversion E; subst. clear H7.

  exists x; exists jmp; exists k1; exists k0.
  exists rs0. eexists rs3. eexists rs4.
  split; auto.
  split.
  subst rs3; auto.
  split; econstructor; eauto.
  split.
  econstructor; eauto.
  rewrite exec_loadCodeMSK_byLabel_lemma with (pos:=x); eauto.
  split.
  subst rs3.
  apply agree_exten with rs0; auto with asmgen.
  intros; Simpl.
  split.
  inversion A1; subst; clear A1. clear H15.
  - auto.
  - auto.
  - split.
    apply agree_exten with rs0; auto with asmgen.
    intros.
    rewrite C.
    subst rs3.
    unfold nextinstr.
    Simpl.
    eauto with asmgen.
    eauto with asmgen.
    eauto with asmgen.

    split.
    rewrite C; eauto with asmgen.
    auto.
Qed.


(**********************************************************************)

(** This is the simulation diagram.
We prove it by case analysis on the Mach transition. *)

(* Variable return_mask: Mach.function -> Mach.code -> val -> Prop. *)

Theorem step_simulation (Y0: Archi.ptr64 = false) :
  forall S1 t S2, Mach.step return_address_offset return_mask ge S1 t S2 ->
  forall S1' (MS: match_states S1 S1'),
  (exists S2', plus step tge S1' t S2' /\ match_states S2 S2')
  \/ (measure S2 < measure S1 /\ t = E0 /\ match_states S2 S1')%nat.
Proof.
  (* induction on Mach.step, inversion on MS *)
  induction 1; intros; inv MS.
- (* Mlabel *)
  left; eapply exec_straight_steps; eauto; intros.
  monadInv TR. econstructor; split. apply exec_straight_one.
  auto.
  simpl; eauto. auto. auto.
  split. apply agree_nextinstr; auto. simpl; congruence.

- (* Mgetstack *)
  unfold load_stack in H.
  exploit Mem.loadv_extends; eauto. intros [v' [A B]].
  rewrite (sp_val _ _ _ AG) in A.
  left; eapply exec_straight_steps; eauto. intros. simpl in TR.
  exploit loadind_correct; eauto with asmgen. intros [rs' [P [Q R]]].
  exists rs'; split. eauto.
  split. eapply agree_set_mreg; eauto with asmgen. congruence.
  simpl; congruence.

- (* Msetstack *)
  unfold store_stack in H.
  assert (Val.lessdef (rs src) (rs0 (preg_of src))). eapply preg_val; eauto.
  exploit Mem.storev_extends; eauto. intros [m2' [A B]].
  left; eapply exec_straight_steps; eauto.
  rewrite (sp_val _ _ _ AG) in A. intros. simpl in TR.
  exploit storeind_correct; eauto with asmgen. intros [rs' [P Q]].
  exists rs'; split. eauto.
  split. eapply agree_undef_regs; eauto with asmgen.
  simpl; intros. rewrite Q; auto with asmgen.

- (* Mgetparam *)
  assert (f0 = f) by congruence; subst f0.
  unfold load_stack in *.
  exploit Mem.loadv_extends. eauto. eexact H0. auto.
  intros [parent' [A B]]. rewrite (sp_val _ _ _ AG) in A.
  exploit lessdef_parent_sp; eauto. clear B; intros B; subst parent'.
  exploit Mem.loadv_extends. eauto. eexact H1. auto.
  intros [v' [C D]].
Opaque loadind.
  left; eapply exec_straight_steps; eauto; intros. monadInv TR.
  destruct ep.
(* X30 contains parent *)
  exploit loadind_correct. eexact EQ.
  instantiate (2 := rs0). rewrite DXP; eauto. congruence.
  eauto with asmgen.
  eauto with asmgen.
  assumption.
  intros [rs1 [P [Q R]]].
  exists rs1; split. eauto.
  split. eapply agree_set_mreg. eapply agree_set_mreg; eauto.
  congruence. auto with asmgen.
  simpl; intros. rewrite R; auto with asmgen.
  apply preg_of_not_X30; auto.
(* GPR11 does not contain parent *)
  rewrite chunk_of_Tptr in A.
  exploit loadind_ptr_correct. eexact A. congruence.
  assumption.
  intros [rs1 [P [Q R]]].
  exploit loadind_correct. eexact EQ. instantiate (2 := rs1).
  rewrite Q. eauto. congruence.
  eauto with asmgen.
  eauto with asmgen.

  eapply msk_straight_preserve; eauto.
  intros [rs2 [S [T U]]].
  exists rs2; split. eapply exec_straight_trans; eauto.
  split. eapply agree_set_mreg. eapply agree_set_mreg. eauto. eauto.
  instantiate (1 := rs1#X30 <- (rs2#X30)). intros.
  rewrite Pregmap.gso; auto with asmgen.
  congruence.
  intros. unfold Pregmap.set. destruct (PregEq.eq r' X30).
  congruence. auto with asmgen.
  simpl; intros. rewrite U; auto with asmgen.
  apply preg_of_not_X30; auto.

- (* Mop *)
  assert (eval_operation tge sp op (map rs args) m = Some v).
    rewrite <- H. apply eval_operation_preserved. exact symbols_preserved.
  exploit eval_operation_lessdef. eapply preg_vals; eauto. eauto. eexact H0.
  intros [v' [A B]]. rewrite (sp_val _ _ _ AG) in A.
  left; eapply exec_straight_steps; eauto; intros. simpl in TR.
  exploit transl_op_correct; eauto. intros [rs2 [P [Q R]]].
  exists rs2; split. eauto. split. auto.
  apply agree_set_undef_mreg with rs0; auto.
  apply Val.lessdef_trans with v'; auto.
  simpl; intros. destruct (andb_prop _ _ H1); clear H1.
  rewrite R; auto. apply preg_of_not_X30; auto.
Local Transparent destroyed_by_op.
  destruct op; simpl; auto; congruence.

- (* Mload *)
  assert (eval_addressing tge sp addr (map rs args) = Some a).
    rewrite <- H. apply eval_addressing_preserved. exact symbols_preserved.
  exploit eval_addressing_lessdef. eapply preg_vals; eauto. eexact H1.
  intros [a' [A B]]. rewrite (sp_val _ _ _ AG) in A.
  exploit Mem.loadv_extends; eauto. intros [v' [C D]].
  left; eapply exec_straight_steps; eauto; intros. simpl in TR.
  exploit transl_load_correct; eauto. intros [rs2 [P [Q R]]].
  exists rs2; split. eauto.
  split. eapply agree_set_undef_mreg; eauto. congruence.
  intros; auto with asmgen.
  simpl; congruence.

- (* Mstore *)
  assert (eval_addressing tge sp addr (map rs args) = Some a).
    rewrite <- H. apply eval_addressing_preserved. exact symbols_preserved.
  exploit eval_addressing_lessdef. eapply preg_vals; eauto. eexact H1.
  intros [a' [A B]]. rewrite (sp_val _ _ _ AG) in A.
  assert (Val.lessdef (rs src) (rs0 (preg_of src))). eapply preg_val; eauto.
  exploit Mem.storev_extends; eauto. intros [m2' [C D]].
  left; eapply exec_straight_steps; eauto.
  intros. simpl in TR. exploit transl_store_correct; eauto. intros [rs2 [P Q]].
  exists rs2; split. eauto.
  split. eapply agree_undef_regs; eauto with asmgen.
  simpl; congruence.

- (* Mcall ***********************************************************)

  rename c into msc.
  rename s into st.

  assert (f0 = f) by congruence.
  subst f0.
  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
     eapply transf_function_no_overflow; eauto.

  generalize H6; intro H6_rem.
  destruct ros as [rf|fid];
  simpl in H; monadInv H6; rename x into tc.

+ (* Indirect call *)
  (* call by Mach register (rf); corresponding to the Asm register x0,
     by EQ1  *)

  remember (rs rf) as rrf.
  destruct rrf; try discriminate.

  assert (Vptr b i = Vptr f' Ptrofs.zero) as K3.
    revert H; predSpec Ptrofs.eq Ptrofs.eq_spec i Ptrofs.zero; intros;
      congruence.

  inversion K3; subst.
  clear K3.

  assert (rs rf = Vptr f' Ptrofs.zero) as H6.
    eauto.

  assert (rs0 x0 = Vptr f' Ptrofs.zero) as H8.
    exploit ireg_val; eauto. rewrite H6; intros LD; inv LD; auto.

(* tc is the translation of the Mach code msc, by EQ.
   msc is the continuation associated with ra (by H1). See also AT *)

  generalize (code_tail_next_int _ _ _ _ NOOV H7). intro CT1.

  generalize (code_tail_next_int _ _ _ _ NOOV CT1). intro CT2.

  assert (TCA: transl_code_at_pc ge
                (Vptr fb (Ptrofs.add (Ptrofs.add ofs Ptrofs.one) Ptrofs.one))
                               fb f msc false tf tc).
  econstructor; eauto.

  assert (Genv.find_funct_ptr tge fb = Some (Internal tf)) as K1.
    eapply functions_transl; eauto.

  assert ( find_instr (Ptrofs.unsigned ofs) (fn_code tf) = Some
                                     (LoadCodeMSK_reg MSK_BRN x0)) as K2.
    eapply find_instr_tail. eauto.

  exploit return_address_offset_correct; eauto; eauto; intros; subst ra.

  (* remember the return address *)
  set (ra := Ptrofs.add (Ptrofs.add ofs Ptrofs.one) Ptrofs.one).

  assert (exec_instr tge fb tf (LoadCodeMSK_reg MSK_BRN x0) rs0 m' =
     Next (nextinstr rs0 # MSK_BRN <- (Vint (generate_mskA f'))) m') as S1.

  simpl.

  eapply exec_loadCode_aux1. assumption.

  left; econstructor; split.

  econstructor 1 with (t1:= E0) (t2:=E0).

  eapply exec_step_internal. eauto. eauto. eauto. auto.

  exact S1.

  eapply star_one.

  econstructor.

  Simpl. unfold Val.offset_ptr.
  rewrite <- H3.
  reflexivity.
  eauto.

  instantiate (1:= Pjal_r x0 sig).
  eapply find_instr_tail. eauto.

  * eapply msk_preserve; eauto. unfold no_jump. simpl; eauto.
    unfold no_branch. simpl; eauto.

  * simpl. Simpl.

  * eauto.

  * inversion VALID_MASK; subst.
    rewrite H9 in H3.
    inversion H3; subst.

    econstructor.

    assert (sp <> Vundef) as q.
      eapply agree_sp_def; eauto.

    econstructor; try eassumption.
    inversion H2; subst.
    econstructor.
    econstructor.
    reflexivity.
    f_equal.

    assumption.

    simpl.
    eapply agree_exten; eauto.
    intros.

    unfold nextinstrCall; Simpl.
    unfold nextinstrCall; Simpl.
    unfold nextinstrCall; Simpl.

    rewrite H8.
    econstructor; eauto.

    unfold nextinstrCall. Simpl.

    simpl.
    rewrite H9.
    simpl.
    reflexivity.

    unfold nextinstrCall; Simpl.
    rewrite H9.
    simpl.
    inversion H2; subst.

    unfold nextinstr.
    unfold next_msk_val_v.
    Simpl.
    simpl.

    rewrite H9.
    simpl.
    rewrite H10.

    rewrite <- valid_mask_incr2_aux.
    reflexivity.

+ (* Direct call *)
  generalize (code_tail_next_int _ _ _ _ NOOV H7). intro CT1.

  generalize (code_tail_next_int _ _ _ _ NOOV CT1). intro CT2.

  assert (TCA: transl_code_at_pc ge
                (Vptr fb (Ptrofs.add (Ptrofs.add ofs Ptrofs.one) Ptrofs.one))
                               fb f msc false tf tc).
  econstructor; eauto.

  assert (Genv.find_funct_ptr tge fb = Some (Internal tf)) as K1.
    eapply functions_transl; eauto.

  assert ( find_instr (Ptrofs.unsigned ofs) (fn_code tf) = Some
                  (LoadCodeMSK_name MSK_BRN (MSK_ident fid))) as K2.
    eapply find_instr_tail. eauto.

  exploit return_address_offset_correct; eauto. intros; subst ra.
  (* remember the return address *)
  set (ra := Ptrofs.add (Ptrofs.add ofs Ptrofs.one) Ptrofs.one).

  inversion VALID_MASK; subst.
  rewrite <- H3 in H6.
  inversion H6; subst.

  assert (exec_instr tge fb0 tf
                     (LoadCodeMSK_name MSK_BRN (MSK_ident fid)) rs0 m' =
  Next  (nextinstr rs0 # MSK_BRN <- (Vint (generate_mskA f'))) m') as S1.
  eapply exec_loadCode_aux2A. assumption.

  left; econstructor; split.

  econstructor 1 with (t1:= E0) (t2:=E0).

  eapply exec_step_internal. rewrite <- H3; simpl; eauto. eauto. eauto.
  auto.

  exact S1.

  eapply star_one.

  econstructor.

  Simpl.
  rewrite <- H3.
  simpl.
  reflexivity.
  eauto.

  instantiate (1:= Pjal_s fid sig).
  eapply find_instr_tail. eauto.

  * eapply msk_preserve; eauto.
    unfold no_jump; simpl; eauto.
    unfold no_branch; simpl; eauto.

  * simpl.
    unfold nextinstrCall.
    Simpl.

  * eauto.

  * inversion VALID_MASK; subst.
    rewrite H9 in H3.
    inversion H3; subst.

    econstructor.

    assert (sp <> Vundef) as q.
      eapply agree_sp_def; eauto.

    econstructor; try eassumption.

    inversion H2; subst.
    econstructor.
    econstructor.
    reflexivity.
    f_equal.

    assumption.

    simpl.
    eapply agree_exten; eauto.
    intros.

    unfold nextinstrCall; Simpl.
    unfold nextinstrCall; Simpl.

    unfold Genv.symbol_address.
    rewrite symbols_preserved.
    rewrite H.
    reflexivity.

    Simpl.
    unfold Genv.symbol_address.
    rewrite symbols_preserved.
    rewrite H.

    econstructor; eauto.

    Simpl.
    rewrite H9.
    simpl.
    reflexivity.

    Simpl.
    rewrite H9.
    simpl.
    inversion H2; subst.

    unfold nextinstr.
    unfold next_msk_val_v.
    Simpl.
    simpl.

    rewrite H9.
    simpl.
    rewrite H10.

    rewrite <- valid_mask_incr2_aux.
    reflexivity.

- (* Mtailcall **********************************************)

  rename c into msc.

  assert (f0 = f) by congruence.
  subst f0.
  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
     eapply transf_function_no_overflow; eauto.

  exploit Mem.loadv_extends.
  eauto. eexact H1. auto.
  simpl. intros [parent' [A B]].

  generalize H8; intro H8_rem.

  destruct ros as [rf|fid];
  simpl in H; monadInv H8; rename x into tc. rename x0 into dr.

+ (* Indirect call *)

  remember (rs rf) as rrf.
  destruct rrf; try discriminate.

  assert (Vptr b i = Vptr f' Ptrofs.zero) as K3.
    revert H; predSpec Ptrofs.eq Ptrofs.eq_spec i Ptrofs.zero; intros;
      congruence.

  rename b into fb1.
  rename i into ofs1.

  inversion K3; subst.
  clear K3.

  assert (rs rf = Vptr f' Ptrofs.zero) as K4.
  eauto.

  assert (rs0 dr = Vptr f' Ptrofs.zero) as K5.
    exploit ireg_val; eauto. rewrite K4; intros LD; inv LD; auto.

  exploit make_epilogue_correct; eauto.

  intros (rs1 & m1 & U & V & W & X & Y & Z & Z1).

  assert (rs1 dr = Vptr f' Ptrofs.zero) as qq.
  rewrite Z1 by (rewrite <- (ireg_of_eq _ _ EQ1); eauto with asmgen).
  assumption.

  exploit exec_straight_steps_2; eauto using functions_transl.
  instantiate (1:=fb) in U.

  intros (ofs' & P & Q).

  generalize (code_tail_next_int _ _ _ _ NOOV Q). intro CT1.

  generalize (code_tail_next_int _ _ _ _ NOOV CT1). intro CT2.

  left; econstructor; split.

  (* execution *)
  eapply plus_trans.
  eapply exec_straight_exec; eauto.

  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as JJ.
  eapply msk_straight_preserve; eauto.

  assert (exec_instr tge fb tf (LoadCodeMSK_reg MSK_BRN dr) rs1 m1 =
      Next (nextinstr rs1 # MSK_BRN <- (Vint (generate_mskA f'))) m1) as GG.

  simpl. unfold exec_loadCodeMSK_byReg.
  rewrite qq.
  assert (Ptrofs.eq Ptrofs.zero Ptrofs.zero = true) as kk.
  auto.
  rewrite kk.
  unfold exec_loadCodeMSK.
  reflexivity.

  eapply plus_two.

  econstructor. eexact P. eapply functions_transl; eauto.
  eapply find_instr_tail. eexact Q.

  exact JJ.
  exact GG.

  eapply msk_preserve in GG.

  econstructor.
  Simpl.
  unfold Val.offset_ptr.
  rewrite P.
  reflexivity.

  eapply functions_transl; eauto. eapply find_instr_tail. eexact CT1.
  exact GG.

  simpl.
  reflexivity.

  unfold no_jump; simpl; eauto.
  unfold no_branch; simpl; eauto.

  eauto. eauto.
  traceEq.
  traceEq.

  (* match states *)
  econstructor; eauto.

  apply agree_set_other; auto with asmgen.
  Simpl.
  unfold nextinstr. Simpl.

  econstructor.
  Simpl. rewrite <- Z.

  rewrite Z.
  eapply parent_sp_def.
  eauto.

  intro.
  Simpl.

  assert (Val.lessdef (rs r) (rs1 (preg_of r))).
  {- inversion AG; subst.
     rewrite Z1; eauto with asmgen. }
  auto.

  Simpl.
  unfold nextinstrTail. Simpl.
  rewrite qq.
  eapply valid_mask_zero.

+ (* Direct call *)
  exploit make_epilogue_correct; eauto.

  intros (rs1 & m1 & U & V & W & X & Y & Z & Z1).

  exploit exec_straight_steps_2; eauto using functions_transl.
  instantiate (1:=fb) in U.

  intros (ofs' & P & Q).

  generalize (code_tail_next_int _ _ _ _ NOOV Q). intro CT1.

  generalize (code_tail_next_int _ _ _ _ NOOV CT1). intro CT2.

  assert (Genv.find_funct_ptr tge fb = Some (Internal tf)) as K1.
    eapply functions_transl; eauto.

  assert (Genv.find_symbol tge fid = Some f') as K2.
    rewrite symbols_preserved. auto.

  left; econstructor; split.

  (* execution *)

  eapply plus_trans.
  eapply exec_straight_exec; eauto.

  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as JJ.
  eapply msk_straight_preserve; eauto.

  assert (exec_instr tge fb tf
                   (LoadCodeMSK_name MSK_BRN (MSK_ident fid)) rs1 m1
    = Next (nextinstr rs1 # MSK_BRN <- (Vint (generate_mskA f'))) m1) as GG.

  simpl. unfold exec_loadCodeMSK_byIdent. rewrite K2.

  unfold exec_loadCodeMSK.
  reflexivity.

  eapply plus_two.

  econstructor. eexact P. eapply functions_transl; eauto.
  eapply find_instr_tail. eexact Q.

  exact JJ.
  exact GG.

  eapply msk_preserve in GG.

  econstructor.
  Simpl.
  unfold Val.offset_ptr.
  rewrite P.
  reflexivity.

  eauto. eapply find_instr_tail. eexact CT1.

  exact GG.

  simpl.
  reflexivity.

  unfold no_jump; simpl; eauto.
  unfold no_branch; simpl; eauto.

  eauto. eauto.
  traceEq.
  traceEq.

  (* match states *)
  econstructor; eauto.

  apply agree_set_other; auto with asmgen.
  Simpl.
  unfold nextinstr. Simpl.

  econstructor.
  Simpl. rewrite <- Z.

  rewrite Z.
  eapply parent_sp_def.
  eauto.

  intro.
  Simpl.

  inversion AG; subst.
  rewrite Z1; eauto with asmgen.

  Simpl.
  unfold nextinstrTail. Simpl. unfold Genv.symbol_address.
  rewrite K2. reflexivity.

  unfold nextinstrTail. unfold nextinstr. Simpl.

  unfold Genv.symbol_address.
  rewrite K2.
  econstructor; eauto.

- (* NOT NEEDED Mbuiltin *)
  inversion AT; subst.
  simpl in H4.
  unfold bind in H4.
  destruct (transl_code f0 b false).
  inversion H4.
  inversion H4.

- (* Mgoto *)
  assert (f0 = f) by congruence. subst f0.
  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
    eapply transf_function_no_overflow; eauto.

  generalize H4; intro H4_rem.
  monadInv H4.

  rename x into k0.

  assert (it1_is_parent ep (Mgoto lbl) = false) as qq.
  eauto.

  generalize (code_tail_next_int _ _ _ _ NOOV H5). intro CT1.

  left.

  exploit label_pos_code_tail_aux1; eauto.
  intros.
  instantiate (1:=0) in H4.
  destruct H4.

  eapply exec_straight_opt_steps_goto7; eauto.
  intros.
  simpl in TR.
  inversion TR; subst. clear TR.

  econstructor 1 with (x:=k0).
  econstructor. econstructor. split.

  econstructor.

  split.
  eapply agree_exten; eauto with asmgen.

  split.
  eapply exec_straight_opt_intro.
  econstructor.
  auto.

  simpl.
  unfold exec_loadCodeMSK_byLabel.
  rewrite H4.
  unfold exec_loadCodeMSK.
  reflexivity.
  Simpl.
  unfold nextinstr.
  Simpl.

  split.
  eapply agree_exten; eauto with asmgen.
  intros.
  Simpl.

  Simpl.
  unfold nextinstr.
  Simpl.

- (* Mcond true ************************************************)
  eapply Mcond_true_case2; eauto.
  congruence.

- (* Mcond false *)
  eapply Mcond_false_case; eauto.

- (* Not supported: Mjumptable *)
  inversion AT; subst.
  simpl in H6.
  unfold bind in H6.
  destruct (transl_code f0 c false).
  inversion H6.
  inversion H6.

- (* Mreturn *)
  assert (f0 = f) by congruence. subst f0.
  inversion AT; subst.

  assert (NOOV: list_length_z tf.(fn_code) <= Ptrofs.max_unsigned).
  eapply transf_function_no_overflow; eauto.

  exploit Mem.loadv_extends.
  eauto. eexact H1. auto.
  simpl. intros [parent' [A B]].

  generalize H7; intro H7_rem.

  simpl in H7; monadInv H7; rename x into tc. rename f into ff.

  assert (Genv.find_funct_ptr tge fb = Some (Internal tf)) as K1.
    eapply functions_transl; eauto. unfold transf_function.

(*  Check make_epilogue_correct. *)
  exploit make_epilogue_correct; eauto.

  intros (rs1 & m1 & U & V & W & X & Y & Z & Z1).

  exploit exec_straight_steps_2; eauto using functions_transl.

  instantiate (1:=fb) in U.

  intros (ofs' & P & Q).

  generalize (code_tail_next_int _ _ _ _ NOOV Q). intro CT1.

  left; econstructor; split.

  eapply plus_trans.
  eapply exec_straight_exec; eauto.

  eapply plus_one.

  econstructor. eexact P. eapply functions_transl; eauto.
  eapply find_instr_tail. eexact Q.

  eapply msk_straight_preserve; eauto.

  simpl. reflexivity.

  traceEq.

  econstructor; eauto.

  apply agree_set_other; auto with asmgen.
  Simpl.

  rewrite <- Z. econstructor. Simpl.
  rewrite Z.
  eapply parent_sp_def; eauto.

  intro. Simpl.

  inversion AG; subst.
  rewrite Z1; eauto with asmgen.

  unfold nextinstrTail. unfold nextinstr. Simpl.
  rewrite X.
  rewrite Y.
  inversion STACKS; subst.

  simpl.
  eapply valid_mask_at_reg_null; eauto.

  simpl.
  econstructor.
  assumption.

(*  congruence. *)
- (* internal function *)
  exploit functions_translated; eauto. intros [tf [A B]]. monadInv B.
  generalize EQ; intros EQ'. monadInv EQ'.
  destruct (zlt Ptrofs.max_unsigned (list_length_z x0.(fn_code))); inversion EQ1. clear EQ1. subst x0.
  unfold store_stack in *.
  exploit Mem.alloc_extends. eauto. eauto. apply Z.le_refl. apply Z.le_refl.
  intros [m1' [C D]].
  exploit Mem.storev_extends. eexact D. eexact H1. eauto. eauto.
  intros [m2' [F G]].
  simpl chunk_of_type in F.
  exploit Mem.storev_extends. eexact G. eexact H2. eauto. eauto.
  intros [m3' [P Q]].
  exploit Mem.storev_extends. eexact Q. eexact H3. eauto. eauto.
  intros [m4' [R S]].

  (* Execution of function prologue *)
  monadInv EQ0. rewrite transl_code'_transl_code in EQ1.
  set (tfbody := Pallocframe (fn_stacksize f) (fn_link_ofs f) ::
                 storeind_ptr X1 X2 (fn_retaddr_ofs f)
                 (storeind_mskptr RET_MSK X2 (fn_retmsk_ofs f) x0)) in *.
  set (tf := {| fn_sig := Mach.fn_sig f; fn_code := tfbody |}) in *.
  set (rs2 := nextinstr (rs0#X30 <- (parent_sp s) #SP <- sp #X31 <- Vundef)).

  assert (exec_instr tge fb tf
             (Pallocframe (fn_stacksize f) (fn_link_ofs f)) rs0 m' =
      Next rs2 m2') as E1.
  { unfold exec_instr. rewrite C. fold sp.
    rewrite <- (sp_val _ _ _ AG). rewrite chunk_of_Tptr in F.
    rewrite F. reflexivity.
  }

  assert (valid_mask_at_pc (rs2 PC) (rs2 MSK_CNT)) as VM1.
  { eapply msk_preserve; eauto.
    unfold no_jump; simpl; eauto.
    unfold no_branch; simpl; eauto.
  }

  exploit (storeind_ptr_correct tge tf SP (fn_retaddr_ofs f) RA
                 (storeind_mskptr RET_MSK X2 (fn_retmsk_ofs f) x0) rs2 m2').
  { rewrite chunk_of_Tptr in P. change (rs2 X1) with (rs0 X1). rewrite ATLR.
    change (rs2 X2) with sp. eexact P.
  }

  congruence. congruence.
  exact VM1.

  intros (rs3 & U & V).

  assert (valid_mask_at_pc (rs3 PC) (rs3 MSK_CNT)) as VM2.
  { eapply msk_straight_preserve.
    exact VM1.
    exact U.
  }

  exploit (storeind_mskptr_correct tge tf SP (fn_retmsk_ofs f) RET_MSK
                                     x0 rs3 m3').
  { rewrite chunk_of_Tptr in R.

    assert (rs3 RET_MSK = rs2 RET_MSK) as q.
    { eapply V. congruence. congruence. congruence. }

    rewrite q.

    change (rs2 RET_MSK) with (rs0 RET_MSK).
    rewrite ATRM.

    assert (rs3 SP = rs2 SP) as q2.
    { eapply V. congruence. congruence. congruence. }

    rewrite q2.

    change (rs2 X2) with sp. eexact R.
  }

  congruence.

  exact VM2.

  assumption.
  intros (rs4 & U2 & V2).

  assert (EXEC_PROLOGUE:
            exec_straight tge tf
              fb tf.(fn_code) rs0 m'
                                           x0 rs4 m4').

  { change (fn_code tf) with tfbody; unfold tfbody.
    apply exec_straight_step with rs2 m2'. auto.
    exact E1.

    reflexivity.
    reflexivity.

    eapply exec_straight_trans.
    eexact U.
    eexact U2.
  }

  exploit exec_straight_steps_2; eauto using functions_transl.
  omega.
  constructor.
  intros (ofs' & X & Y).
  left; exists (State rs4 m4'); split.

  eapply exec_straight_steps_1; eauto.
  omega.
  constructor.

  econstructor; eauto.
  rewrite X; econstructor; eauto.

  eapply msk_straight_preserve.
  exact VM2.
  exact U2.

  apply agree_exten with rs2; eauto with asmgen.
  unfold rs2.
  apply agree_nextinstr. apply agree_set_other; auto with asmgen.
  apply agree_change_sp with (parent_sp s).
  apply agree_undef_regs with rs0. auto.
Local Transparent destroyed_at_function_entry.
  simpl; intros; Simpl.
  unfold sp; congruence.
  intros. rewrite V2 by auto with asmgen. rewrite V by auto with asmgen.
  reflexivity.

  intro.

  assert (rs2 X30 = parent_sp s) as q1.
  reflexivity.

  rewrite <- q1.
  rewrite V2.
  eapply V.
  congruence.
  congruence.
  congruence.
  congruence.
  congruence.
  congruence.

- (* external function *)
  exploit functions_transl_ext; eauto.
  intro G.
  eapply NoExternal in G.
  intuition.

- (* return *)
  inv STACKS. simpl in *.
  right. split. omega. split. auto.
  rewrite <- ATPC in H6.
  econstructor; eauto. rewrite ATPC. rewrite ATMC. assumption.

  congruence.
Qed.



Lemma transf_initial_states:
  forall st1, Mach.initial_state prog st1 ->
  exists st2, Asm.initial_state tprog st2 /\ match_states st1 st2.
Proof.
  intros. inversion H. unfold ge0 in *.
  econstructor; split.
  econstructor.
  eapply (Genv.init_mem_transf_partial TRANSF); eauto.
  replace (Genv.symbol_address (Genv.globalenv tprog)
                               (prog_main tprog) Ptrofs.zero)
    with (Vptr fb Ptrofs.zero).

  econstructor; eauto.
  constructor.
  apply Mem.extends_refl.
  split. auto. simpl. unfold Vnullptr; destruct Archi.ptr64; congruence.
  intros. rewrite Regmap.gi. auto.
  unfold Genv.symbol_address.

  Simpl.
  unfold Genv.find_symbol in H1.

  econstructor.
  reflexivity.
  unfold gen_msk_val.
  unfold encrypt_msk.
(*  unfold encrypt_block_msk. *)
  simpl.
  f_equal.
  unfold Genv.symbol_address.
  rewrite symbols_preserved.
  assert (prog_main tprog = prog_main prog) as qq.
  inversion TRANSF; subst.
  destruct H4.
  auto.
  rewrite qq.
  subst ge.
  rewrite H1.
  auto.
Qed.


Lemma transf_final_states:
  forall st1 st2 r,
  match_states st1 st2 -> Mach.final_state st1 r -> Asm.final_state st2 r.
Proof.
  intros. inv H0. inv H. constructor. assumption.
  compute in H1. inv H1.
  generalize (preg_val _ _ _ R10 AG). rewrite H2. intros LD; inv LD. auto.
Qed.

Theorem transf_program_correct
  (Y0 : Archi.ptr64 = false) :
  forward_simulation (Mach.semantics return_address_offset return_mask prog)
                     (Asm.semantics tprog).
Proof.
  eapply forward_simulation_star with (measure := measure).
  apply senv_preserved.
  eexact transf_initial_states.
  eexact transf_final_states.
  eapply step_simulation; eauto.
Qed.


End PRESERVATION.
