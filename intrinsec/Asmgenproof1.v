(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*           Prashanth Mundkur, SRI International                      *)
(*           Paolo Torrini      Grenoble-INP, VERIMAG                  *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(*  The contributions by Prashanth Mundkur are reused and adapted      *)
(*  under the terms of a Contributor License Agreement between         *)
(*  SRI International and INRIA.                                       *)
(*                                                                     *)
(* *********************************************************************)

Require Import Coqlib Errors Maps.
Require Import AST Zbits Integers Floats Values Memory Globalenvs.
Require Import Op Locations Mach Conventions.
Require Import Asm Asmgen Asmgenproof0 Lia.

(*************** LTAC ************************************************)

Ltac TranslOpSimpl :=
  econstructor; split;
  [ apply exec_straight_one; [simpl; eauto | reflexivity]
  | split; [ apply Val.lessdef_same; Simpl; fail | intros; Simpl; fail ] ].

Ltac TranslOpSimplA :=
    econstructor; split;
 [ eapply exec_straight_one; eauto; simpl; unfold nextinstr; Simpl; fail
  | split; [ eapply Val.lessdef_same; Simpl; fail | intros; Simpl; fail ] ].

Ltac TranslOpSimplB := TranslOpSimpl + TranslOpSimplA.


(**********************************************************************)


(** Decomposition of integer constants. *)

Lemma make_immed32_sound:
  forall n,
  match make_immed32 n with
  | Imm32_single imm => n = imm
  | Imm32_pair hi lo => n = Int.add (Int.shl hi (Int.repr 12)) lo
  end.
Proof.
  intros; unfold make_immed32. set (lo := Int.sign_ext 12 n).
  predSpec Int.eq Int.eq_spec n lo.
- auto.
- set (m := Int.sub n lo).
  assert (A: eqmod (two_p 12) (Int.unsigned lo) (Int.unsigned n))
    by (apply Int.eqmod_sign_ext'; compute; auto).
  assert (B: eqmod (two_p 12) (Int.unsigned n - Int.unsigned  lo) 0).
  { replace 0 with (Int.unsigned n - Int.unsigned n) by omega.
    auto using eqmod_sub, eqmod_refl. }
  assert (C: eqmod (two_p 12) (Int.unsigned m) 0).
  { apply eqmod_trans with (Int.unsigned n - Int.unsigned lo); auto.
    apply eqmod_divides with Int.modulus.
    apply Int.eqm_sym; apply Int.eqm_unsigned_repr.
    exists (two_p (32-12)); auto. }
  assert (D: Int.modu m (Int.repr 4096) = Int.zero).
  { apply eqmod_mod_eq in C. unfold Int.modu.
    change (Int.unsigned (Int.repr 4096)) with (two_p 12). rewrite C.
    reflexivity.
    apply two_p_gt_ZERO; omega. }
  rewrite <- (Int.divu_pow2 m (Int.repr 4096) (Int.repr 12)) by auto.
  rewrite Int.shl_mul_two_p.
  change (two_p (Int.unsigned (Int.repr 12))) with 4096.
  replace (Int.mul (Int.divu m (Int.repr 4096)) (Int.repr 4096)) with m.
  unfold m. rewrite Int.sub_add_opp. rewrite Int.add_assoc.
  rewrite <- (Int.add_commut lo).
  rewrite Int.add_neg_zero. rewrite Int.add_zero. auto.
  rewrite (Int.modu_divu_Euclid m (Int.repr 4096)) at 1
    by (vm_compute; congruence).
  rewrite D. apply Int.add_zero.
Qed.

Lemma make_immed64_sound:
  forall n,
  match make_immed64 n with
  | Imm64_single imm => n = imm
  | Imm64_pair hi lo =>
    n = Int64.add (Int64.sign_ext 32 (Int64.shl hi (Int64.repr 12))) lo
  | Imm64_large imm => n = imm
  end.
Proof.
  intros; unfold make_immed64. set (lo := Int64.sign_ext 12 n).
  predSpec Int64.eq Int64.eq_spec n lo.
- auto.
- set (m := Int64.sub n lo).
  set (p := Int64.zero_ext 20 (Int64.shru m (Int64.repr 12))).
  predSpec Int64.eq Int64.eq_spec n
           (Int64.add (Int64.sign_ext 32 (Int64.shl p (Int64.repr 12))) lo).
  auto.
  auto.
Qed.

(** Properties of registers *)

Lemma ireg_of_not_X31:
  forall m r, ireg_of m = OK r -> IR r <> IR X31.
Proof.
  intros. erewrite <- ireg_of_eq; eauto with asmgen.
Qed.

Lemma ireg_of_not_X31':
  forall m r, ireg_of m = OK r -> r <> X31.
Proof.
  intros. apply ireg_of_not_X31 in H. congruence.
Qed.

Hint Resolve ireg_of_not_X31 ireg_of_not_X31': asmgen.


(**********************************************************************)


Section CONSTRUCTORS.

Variable ge: genv.
Variable fn: function.

(** 32-bit integer constants and arithmetic *)

Lemma load_hilo32_correct:
  forall blk rd hi lo k rs m,
    valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (load_hilo32 rd hi lo k) rs m k rs' m
  /\ rs'#rd = Vint (Int.add (Int.shl hi (Int.repr 12)) lo)
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  unfold load_hilo32; intros.
(* cases: lo = 0, lo <> 0 *)
  predSpec Int.eq Int.eq_spec lo Int.zero.
  - subst lo.

    econstructor; split.
    apply exec_straight_one. auto. simpl; eauto. auto.
    auto. split. rewrite Int.add_zero. Simpl.
    intros; Simpl.

- econstructor; split.
  eapply exec_straight_two. simpl; eauto.

  instantiate (1:=(nextinstr rs # rd <- (Vint (Int.shl hi (Int.repr 12))))).

  eapply msk_preserve. instantiate (1:=Pluiw rd hi). constructor.
  unfold no_branch; eauto. unfold is_branch; eauto. exact H.

  instantiate (5:=ge). instantiate (4:=blk).
  instantiate (3:=fn).
  instantiate (1:=m). simpl. reflexivity.

  simpl; eauto. simpl; eauto.
  auto. auto. Simpl. auto. Simpl. split. Simpl.
  intros; Simpl.
Qed.


Lemma loadimm32_correct:
  forall blk rd n k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (loadimm32 rd n k) rs m k rs' m
  /\ rs'#rd = Vint n
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  unfold loadimm32; intros. generalize (make_immed32_sound n); intros E.
  destruct (make_immed32 n).
- subst imm. econstructor; split.
  apply exec_straight_one.
  auto.
  simpl; eauto. auto. auto.
  split. rewrite Int.add_zero_l; Simpl.
  intros; Simpl.
- rewrite E. apply load_hilo32_correct.
  auto.
Qed.

Lemma opimm32_correct:
  forall (op: ireg -> ireg0 -> ireg0 -> instruction)
         (opi: ireg -> ireg0 -> int -> instruction)
         (sem: val -> val -> val) m,
  (forall d s1 s2 rs b,
      exec_instr ge b fn (op d s1 s2) rs m =
      Next (nextinstr (rs#d <- (sem rs##s1 rs##s2))) m) ->
  (forall d s n rs b,
      exec_instr ge b fn (opi d s n) rs m =
      Next (nextinstr (rs#d <- (sem rs##s (Vint n)))) m) ->
  forall blk rd r1 n k rs,
  r1 <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (opimm32 op opi rd r1 n k) rs m k rs' m
  /\ rs'#rd = sem rs##r1 (Vint n)
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros. unfold opimm32. generalize (make_immed32_sound n); intros E.
  destruct (make_immed32 n).
- subst imm. econstructor; split.
  apply exec_straight_one.
  auto.
  rewrite H0. simpl; eauto. auto. auto.
  split. Simpl. intros; Simpl.
- destruct (load_hilo32_correct blk X31 hi lo (op rd r1 X31 :: k) rs m)
    as (rs' & A & B & C).
  auto.
  econstructor; split.
  eapply exec_straight_trans. eexact A. apply exec_straight_one.
  eapply msk_straight_preserve; eauto.
  rewrite H; eauto. auto. auto.
  split. Simpl. simpl. rewrite B, C, E. auto. congruence. congruence.
  discriminate.
  intros; Simpl.
Qed.


(** 64-bit integer constants and arithmetic *)

Lemma load_hilo64_correct:
  forall blk rd hi lo k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (load_hilo64 rd hi lo k) rs m k rs' m
  /\ rs'#rd =
     Vlong (Int64.add (Int64.sign_ext 32 (Int64.shl hi (Int64.repr 12))) lo)
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  unfold load_hilo64; intros.
  predSpec Int64.eq Int64.eq_spec lo Int64.zero.
- subst lo. econstructor; split.
  apply exec_straight_one. simpl; eauto.
  simpl. reflexivity.
  auto. auto.
  split. rewrite Int64.add_zero. Simpl.
  intros; Simpl.
- econstructor; split.
  eapply exec_straight_two. simpl; eauto.

  2: { simpl. reflexivity. }

  Simpl.
  unfold nextinstr.
  Simpl.

  eapply valid_mask_at_pc_lemma1; eauto.

  simpl. auto.
  auto. auto. auto. auto.
  split. Simpl.
  intros; Simpl.
Qed.



Lemma loadimm64_correct:
  forall blk rd n k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (loadimm64 rd n k) rs m k rs' m
  /\ rs'#rd = Vlong n
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  unfold loadimm64; intros. generalize (make_immed64_sound n); intros E.
  destruct (make_immed64 n).
- subst imm. econstructor; split.
  apply exec_straight_one. simpl; eauto.
(*  admit. *)
  simpl. auto.
  auto. auto.
  split. rewrite Int64.add_zero_l; Simpl.
  intros; Simpl.
- exploit load_hilo64_correct; eauto. intros (rs' & A & B & C).
  rewrite E. exists rs'; eauto.
- subst imm. econstructor; split.
  apply exec_straight_one; eauto.
(*  admit.  *)
  simpl; eauto. auto. auto.
  split. Simpl.
  intros; Simpl.
Qed.


Lemma opimm64_correct:
  forall (op: ireg -> ireg0 -> ireg0 -> instruction)
         (opi: ireg -> ireg0 -> int64 -> instruction)
         (sem: val -> val -> val) m,
  (forall d s1 s2 rs b,
      exec_instr ge b fn (op d s1 s2) rs m =
      Next (nextinstr (rs#d <- (sem rs###s1 rs###s2))) m) ->
  (forall d s n rs b,
      exec_instr ge b fn (opi d s n) rs m =
      Next (nextinstr (rs#d <- (sem rs###s (Vlong n)))) m) ->
  forall blk rd r1 n k rs,
  r1 <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (opimm64 op opi rd r1 n k) rs m k rs' m
  /\ rs'#rd = sem rs##r1 (Vlong n)
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros. unfold opimm64. generalize (make_immed64_sound n); intros E.
  destruct (make_immed64 n).
- subst imm. econstructor; split.
  apply exec_straight_one.
  auto.
  rewrite H0. simpl; eauto. auto. auto.
  split. Simpl. intros; Simpl.
- destruct (load_hilo64_correct blk X31 hi lo (op rd r1 X31 :: k) rs m)
  as (rs' & A & B & C). auto.
  econstructor; split.
  eapply exec_straight_trans. eexact A. apply exec_straight_one.
  eapply msk_straight_preserve; eauto.
  rewrite H; eauto. auto. auto.
  split. Simpl. simpl. rewrite B, C, E. auto. congruence. congruence.
  discriminate.
  intros; Simpl.
- subst imm. econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  Simpl.
  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  rewrite H. simpl; eauto. auto. auto. auto. auto.
  split. Simpl. intros; Simpl.
Qed.


(** Add offset to pointer *)

Lemma addptrofs_correct:
  forall blk rd r1 n k rs m,
  r1 <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (addptrofs rd r1 n k) rs m k rs' m
  /\ Val.lessdef (Val.offset_ptr rs#r1 n) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  unfold addptrofs; intros.
  destruct (Ptrofs.eq_dec n Ptrofs.zero).
- subst n. econstructor; split.
  apply exec_straight_one.
  auto.
  simpl; eauto. auto. auto.
  split. Simpl. destruct (rs r1); simpl; auto. rewrite Ptrofs.add_zero; auto.
  intros; Simpl.
- destruct Archi.ptr64 eqn:SF.
+ unfold addimm64.
  exploit (opimm64_correct Paddl Paddil Val.addl); eauto.
  intros (rs' & A & B & C).
  exists rs'; split. eexact A. split; auto.
  rewrite B. simpl. destruct (rs r1); simpl; auto. rewrite SF.
  rewrite Ptrofs.of_int64_to_int64 by auto. auto.
+ unfold addimm32.
  exploit (opimm32_correct Paddw Paddiw Val.add); eauto.
  intros (rs' & A & B & C).
  exists rs'; split. eexact A. split; auto.
  rewrite B. simpl. destruct (rs r1); simpl; auto. rewrite SF.
  rewrite Ptrofs.of_int_to_int by auto. auto.
Qed.

Lemma addptrofs_correct_2:
  forall blk rd r1 n k (rs: regset) m b ofs,
  r1 <> X31 -> rs#r1 = Vptr b ofs ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (addptrofs rd r1 n k) rs m k rs' m
  /\ rs'#rd = Vptr b (Ptrofs.add ofs n)
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros. exploit (addptrofs_correct blk rd r1 n); eauto.
  intros (rs' & A & B & C).
  exists rs'; intuition eauto.
  rewrite H0 in B. inv B. auto.
Qed.

(** Translation of conditional branches *)

Lemma transl_cbranch_int32s_correct:
  forall cmp r1 r2 lbl (rs: regset) m b blk,
  Val.cmp_bool cmp rs##r1 rs##r2 = Some b ->
  exec_instr ge blk fn (transl_cbranch_int32s cmp r1 r2 lbl) rs m =
  eval_branch fn lbl rs m (Some b).
Proof.
  intros. destruct cmp; simpl; rewrite ? H.
- destruct rs##r1; simpl in H; try discriminate. destruct rs##r2; inv H.
  simpl; auto.
- destruct rs##r1; simpl in H; try discriminate. destruct rs##r2; inv H.
  simpl; auto.
- auto.
- rewrite <- Val.swap_cmp_bool. simpl. rewrite H; auto.
- rewrite <- Val.swap_cmp_bool. simpl. rewrite H; auto.
- auto.
Qed.

Lemma transl_cbranch_int32u_correct:
  forall cmp r1 r2 lbl (rs: regset) m b blk,
  Val.cmpu_bool (Mem.valid_pointer m) cmp rs##r1 rs##r2 = Some b ->
  exec_instr ge blk fn (transl_cbranch_int32u cmp r1 r2 lbl) rs m =
  eval_branch fn lbl rs m (Some b).
Proof.
  intros. destruct cmp; simpl; rewrite ? H; auto.
- rewrite <- Val.swap_cmpu_bool. simpl. rewrite H; auto.
- rewrite <- Val.swap_cmpu_bool. simpl. rewrite H; auto.
Qed.

Lemma transl_cbranch_int64s_correct:
  forall cmp r1 r2 lbl (rs: regset) m b blk,
  Val.cmpl_bool cmp rs###r1 rs###r2 = Some b ->
  exec_instr ge blk fn (transl_cbranch_int64s cmp r1 r2 lbl) rs m =
  eval_branch fn lbl rs m (Some b).
Proof.
  intros. destruct cmp; simpl; rewrite ? H.
- destruct rs###r1; simpl in H; try discriminate. destruct rs###r2; inv H.
  simpl; auto.
- destruct rs###r1; simpl in H; try discriminate. destruct rs###r2; inv H.
  simpl; auto.
- auto.
- rewrite <- Val.swap_cmpl_bool. simpl. rewrite H; auto.
- rewrite <- Val.swap_cmpl_bool. simpl. rewrite H; auto.
- auto.
Qed.

Lemma transl_cbranch_int64u_correct:
  forall cmp r1 r2 lbl (rs: regset) m b blk,
  Val.cmplu_bool (Mem.valid_pointer m) cmp rs###r1 rs###r2 = Some b ->
  exec_instr ge blk fn (transl_cbranch_int64u cmp r1 r2 lbl) rs m =
  eval_branch fn lbl rs m (Some b).
Proof.
  intros. destruct cmp; simpl; rewrite ? H; auto.
- rewrite <- Val.swap_cmplu_bool. simpl. rewrite H; auto.
- rewrite <- Val.swap_cmplu_bool. simpl. rewrite H; auto.
Qed.

Lemma transl_cond_float_correct:
  forall (rs: regset) m cmp rd r1 r2 insn normal v blk,
  transl_cond_float cmp rd r1 r2 = (insn, normal) ->
  v = (if normal then Val.cmpf cmp rs#r1 rs#r2
       else Val.notbool (Val.cmpf cmp rs#r1 rs#r2)) ->
  exec_instr ge blk fn insn rs m = Next (nextinstr (rs#rd <- v)) m.
Proof.
  intros. destruct cmp; simpl in H; inv H; auto.
- rewrite Val.negate_cmpf_eq. auto.
- simpl. f_equal. f_equal. f_equal. destruct (rs r2), (rs r1); auto.
  unfold Val.cmpf, Val.cmpf_bool.
  rewrite <- Float.cmp_swap. auto.
- simpl. f_equal. f_equal. f_equal. destruct (rs r2), (rs r1); auto.
  unfold Val.cmpf, Val.cmpf_bool.
  rewrite <- Float.cmp_swap. auto.
Qed.

Lemma transl_cond_single_correct:
  forall (rs: regset) m cmp rd r1 r2 insn normal v blk,
  transl_cond_single cmp rd r1 r2 = (insn, normal) ->
  v = (if normal then Val.cmpfs cmp rs#r1 rs#r2
       else Val.notbool (Val.cmpfs cmp rs#r1 rs#r2)) ->
  exec_instr ge blk fn insn rs m = Next (nextinstr (rs#rd <- v)) m.
Proof.
  intros. destruct cmp; simpl in H; inv H; auto.
  - simpl. f_equal. f_equal. f_equal. destruct (rs r2), (rs r1); auto.
    unfold Val.cmpfs, Val.cmpfs_bool.
  rewrite Float32.cmp_ne_eq. destruct (Float32.cmp Ceq f0 f); auto.
  - simpl. f_equal. f_equal. f_equal. destruct (rs r2), (rs r1); auto.
    unfold Val.cmpfs, Val.cmpfs_bool.
  rewrite <- Float32.cmp_swap. auto.
  - simpl. f_equal. f_equal. f_equal. destruct (rs r2), (rs r1); auto.
    unfold Val.cmpfs, Val.cmpfs_bool.
  rewrite <- Float32.cmp_swap. auto.
Qed.

Remark branch_on_X31:
  forall normal lbl (rs: regset) m b blk,
  rs#X31 = Val.of_bool (eqb normal b) ->
  exec_instr ge blk fn (if normal then Pbnew X31 X0 lbl
                        else Pbeqw X31 X0 lbl) rs m =
  eval_branch fn lbl rs m (Some b).
Proof.
  intros. destruct normal; simpl; rewrite H; simpl; destruct b; reflexivity.
Qed.


(********************************************************************)

Lemma exec_loadCode_aux1 : forall (x: ireg) (rs: preg -> val) m f,
  rs x = Vptr f Ptrofs.zero ->
  exec_loadCodeMSK_byReg x MSK_BRN rs m =
     Next (nextinstr rs # MSK_BRN <- (Vint (generate_mskA f))) m.

    intros.
    unfold exec_loadCodeMSK_byReg.
    rewrite H.

    assert (Ptrofs.eq Ptrofs.zero Ptrofs.zero = true) as q1.
      intuition.
    rewrite q1.

    unfold exec_loadCodeMSK.
    reflexivity.
Qed.

Lemma exec_loadCode_aux2 : forall fb tf fid rs0 m' f',
  Genv.find_symbol ge fid = Some f' ->
  exec_instr ge fb tf
                     (LoadCodeMSK_name MSK_BRN (MSK_ident fid)) rs0 m' =
  Next  (nextinstr rs0 # MSK_BRN <- (Vint (generate_mskA f'))) m'.
  intros.
  simpl.

  unfold exec_loadCodeMSK_byIdent.
  rewrite H.
  unfold exec_loadCodeMSK.
  reflexivity.
Qed.

Lemma exec_loadCodeMSK_byLabel_lemma : forall tge fb tf lbl rs1 m pos,
 label_pos lbl 0 (fn_code tf) = Some pos ->
 exec_instr tge fb tf (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs1 m =
      Next (nextinstr rs1 # MSK_BRN <- (Vint (encrypt_msk pos fb))) m.
  intros.
  simpl.
  unfold exec_loadCodeMSK_byLabel.
  rewrite H.
  unfold exec_loadCodeMSK.
  reflexivity.
Qed.

(**************************************************************************)

Lemma transl_cbranch_correct_Aux5:
  forall lbl k c rs m' blk
         rs' insn (P: regset -> instruction -> Prop),
    (exists (rs0: regset),
  valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
  exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
  rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
  rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT)
  /\
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\ P rs' insn)
    ->
    exec_straight_opt ge fn blk
                      (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' (insn :: k) rs' m' /\ P rs' insn.

intros.
destruct H as [rs0 H].
destruct H as [H H0].
destruct H0 as [H0 H1].
destruct H1 as [H1 H2].
destruct H2 as [H2 H3].
destruct H3 as [H3 H4].
split; auto.

inversion H3; subst.
clear H3.
econstructor.
econstructor.
auto. auto. auto. auto.

econstructor.
eapply exec_straight_trans.
econstructor. auto. eauto. auto. auto. auto.
Qed.


Lemma transl_cbranch_correct_Aux6a:
  forall lbl k c b rs m' blk,
    (exists  (rs0: regset),
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                            (exists rs' insn,
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r) ))
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk
                        (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r).
  intros.
  destruct H as [rs0 H].
  destruct H.
  destruct H0.
  destruct H1.
  destruct H2.
  destruct H3 as [rs' H3].
  destruct H3 as [insn H3].
  destruct H3.
  destruct H4.
  econstructor 1 with (x:=rs'). econstructor 1 with (x:=insn).
  eapply (transl_cbranch_correct_Aux5 lbl k c rs m' blk
                                      rs' insn
      (fun rs' insn => exec_instr ge blk fn insn rs' m' =
                   eval_branch fn lbl rs' m' (Some b) /\
      (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r))).
  econstructor 1 with (x:=rs0).
  intuition.
Qed.


Lemma transl_cbranch_correct_1:
  forall cond args lbl k c m ms b sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some b ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs', exists insn,
  exec_straight_opt ge fn blk
                        (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
           (insn :: k) rs' m'
  /\ exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b)
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros until zpos; intros TRANSL EVAL AG MEXT LL MM.
  set (vl' := map rs (map preg_of args)).
  assert (EVAL': eval_condition cond vl' m' = Some b).
  { apply eval_condition_lessdef with (map ms args) m; auto.
    eapply preg_vals; eauto. }
  clear EVAL MEXT AG.

  eapply transl_cbranch_correct_Aux6a.

  econstructor. split. auto.

  split. simpl. unfold exec_loadCodeMSK_byLabel. rewrite LL.
  unfold exec_loadCodeMSK. unfold nextinstr. rewrite Pregmap.gso.
  rewrite Pregmap.gso.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  reflexivity.
  discriminate.
  discriminate.

  Simpl.

  split. auto.
  split. auto.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  assert (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs3#r = rs#r) as DD.
  intros.
  subst rs3.
  subst rs2.
  Simpl.

  destruct cond; simpl in TRANSL; ArgsInv.

- exists rs3, (transl_cbranch_int32s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
- exists rs3, (transl_cbranch_int32u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                 (transl_cbranch_int32s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                      (transl_cbranch_int32u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- exists rs3, (transl_cbranch_int64s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
- exists rs3, (transl_cbranch_int64u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
+ exploit (loadimm64_correct blk X31 n
                    (transl_cbranch_int64s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
+ exploit (loadimm64_correct blk X31 n
      (transl_cbranch_int64u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros. Simpl.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpf_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpfs_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.
Qed.


(***********************************************************************)


Lemma transl_cbranch_correct_Aux6B:
  forall lbl k c b rs m' blk zpos,
    (exists  (rs0: regset),
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                            (exists rs2 insn,
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs2 m' /\
    exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b) /\
    rs2 # MSK_BRN = Vint (encrypt_msk zpos blk) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs2#r = rs#r) ))
    ->
    label_pos lbl 0 (fn_code fn) = Some zpos ->
    exists (rs1 rs2 : regset) (insn : instruction),
     exec_straight ge fn blk
       (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
       c rs1 m'
    /\ exec_straight_opt ge fn blk c rs1 m' (insn :: k) rs2 m'
    /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
    /\ exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b)
    /\ (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r).
  intros.
  destruct H as [rs0 H].
  destruct H.
  destruct H1.
  destruct H2.
  destruct H3.
  destruct H4 as [rs2 H4].
  destruct H4 as [insn H4].
  destruct H4.
  destruct H5.
  destruct H6.
  econstructor 1 with (x:= nextinstr rs # MSK_BRN <-
                                     (Vint (encrypt_msk zpos blk))).
  econstructor 1 with (x:=rs2).
  econstructor 1 with (x:=insn).
  split.
  econstructor; eauto.
  eapply exec_loadCodeMSK_byLabel_lemma; eauto.

  rewrite exec_loadCodeMSK_byLabel_lemma with (pos := zpos) in H1.
  inversion H1; subst.

  split; auto.
  split; auto.
  split; auto.

  intros.
  unfold nextinstr.
  Simpl.

  assert ({r = MSK_BRN} + {r <> MSK_BRN}) as E.
  eapply preg_eq.

  destruct E.
  inversion e; subst.
  Simpl.
  Simpl.
  auto.
Qed.



Lemma transl_cbranch_correct_1B:
  forall cond args lbl k c m ms b sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some b ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs1, exists rs2, exists insn,
  exec_straight ge fn blk
       (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
       c rs1 m'
  /\ exec_straight_opt ge fn blk c rs1 m' (insn :: k) rs2 m'
  /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
  /\ exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b)
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r.
Proof.
  intros until zpos; intros TRANSL EVAL AG MEXT LL MM.
  set (vl' := map rs (map preg_of args)).
  assert (EVAL': eval_condition cond vl' m' = Some b).
  { apply eval_condition_lessdef with (map ms args) m; auto.
    eapply preg_vals; eauto. }
  clear EVAL MEXT AG.

  eapply transl_cbranch_correct_Aux6B.

  econstructor.
  split; auto.
  split. eapply exec_loadCodeMSK_byLabel_lemma; eauto.

  split; Simpl.

  split. unfold nextinstr. Simpl.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  assert (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs3#r = rs#r) as DD.
  intros.
  subst rs3.
  subst rs2.
  Simpl.

  destruct cond; simpl in TRANSL; ArgsInv.

- exists rs3, (transl_cbranch_int32s c0 x x0 lbl).
  intuition auto. constructor.
  apply transl_cbranch_int32s_correct; auto.
- exists rs3, (transl_cbranch_int32u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                 (transl_cbranch_int32s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                      (transl_cbranch_int32u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- exists rs3, (transl_cbranch_int64s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
- exists rs3, (transl_cbranch_int64u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
+ exploit (loadimm64_correct blk X31 n
                    (transl_cbranch_int64s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
+ exploit (loadimm64_correct blk X31 n
      (transl_cbranch_int64u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpf c0 rs#x rs#x0
            else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  split. Simpl.
  intros. Simpl.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpf_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpf c0 rs#x rs#x0
            else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  split. Simpl.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0
            else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  split. Simpl.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpfs_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0
            else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  split. Simpl.
  intros; Simpl.
 - auto.
Qed.




Lemma transl_cbranch_correct_trueB:
  forall cond args lbl k c m ms sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some true ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs1, exists rs2, exists insn,
  exec_straight ge fn blk
       (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
       c rs1 m'
  /\ exec_straight_opt ge fn blk c rs1 m' (insn :: k) rs2 m'
  /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
  /\ exec_instr ge blk fn insn rs2 m' = goto_label fn lbl rs2 m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r.
Proof.
  intros. eapply transl_cbranch_correct_1B with (b := true); eauto.
Qed.



Lemma transl_cbranch_correct_false:
  forall cond args lbl k c m ms sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some false ->
  agree ms sp rs ->
  Mem.extends m m' ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  exists rs',
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c)
                  rs m' k rs' m'
     /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
                  -> rs'#r = rs#r.
Proof.
  intros. exploit transl_cbranch_correct_1; eauto. simpl.
  intros (rs' & insn & A & B & C).
  exists (nextinstr rs').
  split. eapply exec_straight_opt_right; eauto.
  apply exec_straight_one; auto.

  eapply msk_straight_opt_preserve; eauto.
  intros; Simpl.
Qed.


(** Translation of condition operators *)

Lemma transl_cond_int32s_correct:
  forall blk cmp rd r1 r2 k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (transl_cond_int32s cmp rd r1 r2 k) rs m k rs' m
  /\ Val.lessdef (Val.cmp cmp rs##r1 rs##r2) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros. destruct cmp; simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl. destruct (rs##r1); auto. destruct (rs##r2); auto.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl. destruct (rs##r1); auto. destruct (rs##r2); auto.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
- econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  Simpl.
  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. unfold Val.cmp. rewrite <- Val.swap_cmp_bool.
  simpl. rewrite (Val.negate_cmp_bool Clt).
  destruct (Val.cmp_bool Clt rs##r2 rs##r1) as [[]|]; auto.
- econstructor; split. apply exec_straight_one. auto.
  simpl; auto. simpl; auto. simpl; auto.
  split; intros; Simpl. unfold Val.cmp. rewrite <- Val.swap_cmp_bool. auto.
- econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.

  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. unfold Val.cmp. rewrite (Val.negate_cmp_bool Clt).
  destruct (Val.cmp_bool Clt rs##r1 rs##r2) as [[]|]; auto.
 Qed.


Lemma transl_cond_int32u_correct:
  forall blk cmp rd r1 r2 k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (transl_cond_int32u cmp rd r1 r2 k) rs m k rs' m
  /\ rs'#rd = Val.cmpu (Mem.valid_pointer m) cmp rs##r1 rs##r2
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.

  intros. destruct cmp; simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
- econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. unfold Val.cmpu. rewrite <- Val.swap_cmpu_bool.
  simpl. rewrite (Val.negate_cmpu_bool (Mem.valid_pointer m) Cle).
  destruct (Val.cmpu_bool (Mem.valid_pointer m) Cle rs##r1 rs##r2)
    as [[]|]; auto.
- econstructor; split. apply exec_straight_one. auto.
  simpl; auto. simpl; auto. simpl; auto.
  split; intros; Simpl. unfold Val.cmpu. rewrite <- Val.swap_cmpu_bool. auto.
- econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. unfold Val.cmpu.
  rewrite (Val.negate_cmpu_bool (Mem.valid_pointer m) Clt).
  destruct (Val.cmpu_bool (Mem.valid_pointer m) Clt rs##r1 rs##r2)
    as [[]|]; auto.
Qed.

Lemma transl_cond_int64s_correct:
  forall blk cmp rd r1 r2 k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (transl_cond_int64s cmp rd r1 r2 k) rs m k rs' m
  /\ Val.lessdef (Val.maketotal (Val.cmpl cmp rs###r1 rs###r2)) rs'#rd
  /\ forall r, r <> PC -> r <> rd-> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros. destruct cmp; simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
  destruct (rs###r1); auto. destruct (rs###r2); auto.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
  destruct (rs###r1); auto. destruct (rs###r2); auto.
- econstructor; split.
  apply exec_straight_one; [auto | simpl; eauto | auto | auto].
  split; intros; Simpl.
- econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  Simpl.
  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  unfold nextinstr.
  Simpl. simpl; eauto.

  simpl; eauto. Simpl.
  destruct (Val.cmpl_bool Clt rs###r2 rs###r1) as [[]|]; auto.

  Simpl.
  split; intros; Simpl. unfold Val.cmpl. rewrite <- Val.swap_cmpl_bool.
  simpl. rewrite (Val.negate_cmpl_bool Clt).
  destruct (Val.cmpl_bool Clt rs###r2 rs###r1) as [[]|]; auto.
- econstructor; split.
  apply exec_straight_one; [auto | simpl; eauto|auto | auto ].
  split; intros; Simpl. unfold Val.cmpl. rewrite <- Val.swap_cmpl_bool. auto.
- econstructor; split.
  eapply exec_straight_two; auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.

  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. simpl; eauto. auto. auto.
  Simpl.

  split; intros; Simpl. unfold Val.cmpl. rewrite (Val.negate_cmpl_bool Clt).
  destruct (Val.cmpl_bool Clt rs###r1 rs###r2) as [[]|]; auto.
Qed.

Lemma transl_cond_int64u_correct:
  forall blk cmp rd r1 r2 k rs m,
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
  exec_straight ge fn blk (transl_cond_int64u cmp rd r1 r2 k) rs m k rs' m
  /\ rs'#rd =
        Val.maketotal (Val.cmplu (Mem.valid_pointer m) cmp rs###r1 rs###r2)
  /\ forall r, r <> PC -> r <> rd -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros. destruct cmp; simpl.
- econstructor; split. apply exec_straight_one. auto.
  simpl; eauto.
  simpl; auto.
  simpl; auto.
  split; intros; Simpl.
- econstructor; split.
  apply exec_straight_one; [auto | simpl; eauto|auto | auto].
  split; intros; Simpl.
- econstructor; split.
  apply exec_straight_one; [auto | simpl; eauto|auto | auto].
  split; intros; Simpl.
- econstructor; split.
  eapply exec_straight_two; auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. simpl; eauto. auto. auto.
  Simpl.

  split; intros; Simpl. unfold Val.cmplu. rewrite <- Val.swap_cmplu_bool.
  simpl. rewrite (Val.negate_cmplu_bool (Mem.valid_pointer m) Cle).
  destruct (Val.cmplu_bool (Mem.valid_pointer m) Cle rs###r1 rs###r2)
    as [[]|]; auto.
- econstructor; split.
  apply exec_straight_one; [auto | simpl; eauto|auto | auto].
  split; intros; Simpl. unfold Val.cmplu.
  rewrite <- Val.swap_cmplu_bool. auto.
- econstructor; split.
  eapply exec_straight_two; auto.
  2: { simpl; eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. simpl; eauto. auto. auto. Simpl.
  split; intros; Simpl. unfold Val.cmplu.
  rewrite (Val.negate_cmplu_bool (Mem.valid_pointer m) Clt).
  destruct (Val.cmplu_bool (Mem.valid_pointer m) Clt rs###r1 rs###r2)
    as [[]|]; auto.
Qed.


Lemma transl_condimm_int32s_correct:
  forall blk cmp rd r1 n k rs m,
    r1 <> X31 ->
    valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (transl_condimm_int32s cmp rd r1 n k) rs m k rs' m
  /\ Val.lessdef (Val.cmp cmp rs#r1 (Vint n)) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r.
Proof.
  intros. unfold transl_condimm_int32s.
  predSpec Int.eq Int.eq_spec n Int.zero.
- subst n. exploit transl_cond_int32s_correct. eauto.
    intros (rs' & A & B & C).
  exists rs'; eauto.
- assert (DFL:
    exists rs',
      exec_straight ge fn blk
         (loadimm32 X31 n (transl_cond_int32s cmp rd r1 X31 k)) rs m k rs' m
   /\ Val.lessdef (Val.cmp cmp rs#r1 (Vint n)) rs'#rd
   /\ forall r, r <> PC -> r <> rd -> r <> X31  -> r <> MSK_CNT ->
               r <> MSK_BRN-> rs'#r = rs#r).
  { exploit loadimm32_correct; eauto. intros (rs1 & A1 & B1 & C1).
    assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
    clear H0.
    exploit transl_cond_int32s_correct; eauto.
    intros (rs2 & A2 & B2 & C2).
    exists rs2; split.
    eapply exec_straight_trans. eexact A1. eexact A2.
    split. simpl in B2. rewrite B1, C1 in B2 by auto with asmgen. auto.
    intros; transitivity (rs1 r); eauto.
  }
  destruct cmp.
+ unfold xorimm32.
  exploit (opimm32_correct Pxorw Pxoriw Val.xor); eauto.
  intros (rs1 & A1 & B1 & C1).
  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
  clear H0.
  exploit transl_cond_int32s_correct; eauto. intros (rs2 & A2 & B2 & C2).
  exists rs2; split.
  eapply exec_straight_trans. eexact A1. eexact A2.
  split. simpl in B2; rewrite B1 in B2; simpl in B2. destruct (rs#r1); auto.
  unfold Val.cmp in B2; simpl in B2; rewrite Int.xor_is_zero in B2. exact B2.
  intros; transitivity (rs1 r); auto.
+ unfold xorimm32.
  exploit (opimm32_correct Pxorw Pxoriw Val.xor); eauto.
  intros (rs1 & A1 & B1 & C1).
  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
  clear H0.
  exploit transl_cond_int32s_correct; eauto. intros (rs2 & A2 & B2 & C2).
  exists rs2; split.
  eapply exec_straight_trans. eexact A1. eexact A2.
  split. simpl in B2; rewrite B1 in B2; simpl in B2. destruct (rs#r1); auto.
  unfold Val.cmp in B2; simpl in B2; rewrite Int.xor_is_zero in B2. exact B2.
  intros; transitivity (rs1 r); auto.
+ exploit (opimm32_correct Psltw Psltiw (Val.cmp Clt)); eauto.
  intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto. rewrite B1; auto.
+ predSpec Int.eq Int.eq_spec n (Int.repr Int.max_signed).
* subst n. exploit loadimm32_correct; eauto. intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto.
  unfold Val.cmp; destruct (rs#r1); simpl; auto. rewrite B1.
  unfold Int.lt. rewrite zlt_false. auto.
  change (Int.signed (Int.repr Int.max_signed)) with Int.max_signed.
  generalize (Int.signed_range i); omega.
* exploit (opimm32_correct Psltw Psltiw (Val.cmp Clt)); eauto.
  intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto.
  rewrite B1. unfold Val.cmp; simpl; destruct (rs#r1); simpl; auto.
  unfold Int.lt. replace (Int.signed (Int.add n Int.one))
                   with (Int.signed n + 1).
  destruct (zlt (Int.signed n) (Int.signed i)).
  rewrite zlt_false by omega. auto.
  rewrite zlt_true by omega. auto.
  rewrite Int.add_signed. symmetry; apply Int.signed_repr.
  assert (Int.signed n <> Int.max_signed).
  { red; intros E. elim H2. rewrite <- (Int.repr_signed n). rewrite E. auto. }
  generalize (Int.signed_range n); omega.
+ apply DFL.
+ apply DFL.
Qed.


Lemma transl_condimm_int32u_correct:
  forall blk cmp rd r1 n k rs m,
  r1 <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (transl_condimm_int32u cmp rd r1 n k) rs m k rs' m
  /\ Val.lessdef (Val.cmpu (Mem.valid_pointer m) cmp rs#r1 (Vint n)) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r.
Proof.
  intros. unfold transl_condimm_int32u.
  predSpec Int.eq Int.eq_spec n Int.zero.
- subst n. exploit transl_cond_int32u_correct. eauto.
  intros (rs' & A & B & C).
  exists rs'; split. eexact A. split; auto. rewrite B; auto.
- assert (DFL:
   exists rs',
   exec_straight ge fn blk
         (loadimm32 X31 n (transl_cond_int32u cmp rd r1 X31 k)) rs m k rs' m
   /\ Val.lessdef (Val.cmpu (Mem.valid_pointer m) cmp rs#r1 (Vint n)) rs'#rd
   /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r).
  { exploit loadimm32_correct; eauto. intros (rs1 & A1 & B1 & C1).
    assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
    clear H0.
    exploit transl_cond_int32u_correct; eauto. intros (rs2 & A2 & B2 & C2).
    exists rs2; split.
    eapply exec_straight_trans. eexact A1. eexact A2.
    split. simpl in B2. rewrite B1, C1 in B2 by auto with asmgen.
    rewrite B2; auto.
    intros; transitivity (rs1 r); auto. }
  destruct cmp.
+ apply DFL.
+ apply DFL.
+ exploit (opimm32_correct Psltuw Psltiuw
                           (Val.cmpu (Mem.valid_pointer m) Clt) m); eauto.
  intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto. rewrite B1; auto.
+ apply DFL.
+ apply DFL.
+ apply DFL.
Qed.

Lemma transl_condimm_int64s_correct:
  forall blk cmp rd r1 n k rs m,
  r1 <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
  exec_straight ge fn blk (transl_condimm_int64s cmp rd r1 n k)
                  rs m k rs' m
  /\ Val.lessdef (Val.maketotal (Val.cmpl cmp rs#r1 (Vlong n))) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r.
Proof.
  intros. unfold transl_condimm_int64s.
  predSpec Int64.eq Int64.eq_spec n Int64.zero.
- subst n. exploit transl_cond_int64s_correct. eauto.
    intros (rs' & A & B & C).
  exists rs'; eauto.
- assert (DFL:
    exists rs',
      exec_straight ge fn blk (loadimm64 X31 n
                        (transl_cond_int64s cmp rd r1 X31 k)) rs m k rs' m
   /\ Val.lessdef (Val.maketotal (Val.cmpl cmp rs#r1 (Vlong n))) rs'#rd
   /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r).
  { exploit loadimm64_correct; eauto. intros (rs1 & A1 & B1 & C1).
    assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
    clear H0.
    exploit transl_cond_int64s_correct; eauto. intros (rs2 & A2 & B2 & C2).
    exists rs2; split.
    eapply exec_straight_trans. eexact A1. eexact A2.
    split. simpl in B2. rewrite B1, C1 in B2 by auto with asmgen. auto.
    intros; transitivity (rs1 r); auto. }
  destruct cmp.
+ unfold xorimm64.
  exploit (opimm64_correct Pxorl Pxoril Val.xorl); eauto.
  intros (rs1 & A1 & B1 & C1).
  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
  clear H0.
  exploit transl_cond_int64s_correct; eauto. intros (rs2 & A2 & B2 & C2).
  exists rs2; split.
  eapply exec_straight_trans. eexact A1. eexact A2.
  split. simpl in B2; rewrite B1 in B2; simpl in B2. destruct (rs#r1); auto.
  unfold Val.cmpl in B2; simpl in B2; rewrite Int64.xor_is_zero in B2. exact B2.
  intros; transitivity (rs1 r); auto.
+ unfold xorimm64.
  exploit (opimm64_correct Pxorl Pxoril Val.xorl); eauto.
  intros (rs1 & A1 & B1 & C1).
  assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
  clear H0.
  exploit transl_cond_int64s_correct; eauto. intros (rs2 & A2 & B2 & C2).
  exists rs2; split.
  eapply exec_straight_trans. eexact A1. eexact A2.
  split. simpl in B2; rewrite B1 in B2; simpl in B2. destruct (rs#r1); auto.
  unfold Val.cmpl in B2; simpl in B2; rewrite Int64.xor_is_zero in B2.
  exact B2.
  intros; transitivity (rs1 r); auto.
+ exploit (opimm64_correct Psltl Psltil
                   (fun v1 v2 => Val.maketotal (Val.cmpl Clt v1 v2))); eauto.
  intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto. rewrite B1; auto.
+ predSpec Int64.eq Int64.eq_spec n (Int64.repr Int64.max_signed).
* subst n. exploit loadimm32_correct; eauto. intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto.
  unfold Val.cmpl; destruct (rs#r1); simpl; auto. rewrite B1.
  unfold Int64.lt. rewrite zlt_false. auto.
  change (Int64.signed (Int64.repr Int64.max_signed)) with Int64.max_signed.
  generalize (Int64.signed_range i); omega.
* exploit (opimm64_correct Psltl Psltil (fun v1 v2 => Val.maketotal (Val.cmpl Clt v1 v2))); eauto. intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto.
  rewrite B1. unfold Val.cmpl; simpl; destruct (rs#r1); simpl; auto.
  unfold Int64.lt. replace (Int64.signed (Int64.add n Int64.one)) with (Int64.signed n + 1).
  destruct (zlt (Int64.signed n) (Int64.signed i)).
  rewrite zlt_false by omega. auto.
  rewrite zlt_true by omega. auto.
  rewrite Int64.add_signed. symmetry; apply Int64.signed_repr.
  assert (Int64.signed n <> Int64.max_signed).
  { red; intros E. elim H2. rewrite <- (Int64.repr_signed n).
    rewrite E. auto. }
  generalize (Int64.signed_range n); omega.
+ apply DFL.
+ apply DFL.
Qed.

Lemma transl_condimm_int64u_correct:
  forall blk cmp rd r1 n k rs m,
    r1 <> X31 ->
    valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
    exec_straight ge fn blk (transl_condimm_int64u cmp rd r1 n k)
                  rs m k rs' m
    /\ Val.lessdef (Val.maketotal (Val.cmplu (Mem.valid_pointer m)
                                             cmp rs#r1 (Vlong n))) rs'#rd
    /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r.
Proof.
  intros. unfold transl_condimm_int64u.
  predSpec Int64.eq Int64.eq_spec n Int64.zero.
  - subst n. exploit transl_cond_int64u_correct. eauto.
    intros (rs' & A & B & C).
  exists rs'; split. eexact A. split; auto. rewrite B; auto.
- assert (DFL:
    exists rs',
      exec_straight ge fn blk (loadimm64 X31 n (transl_cond_int64u cmp rd r1 X31 k)) rs m k rs' m
   /\ Val.lessdef (Val.maketotal (Val.cmplu (Mem.valid_pointer m) cmp rs#r1 (Vlong n))) rs'#rd
   /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r).
  { exploit loadimm64_correct; eauto. intros (rs1 & A1 & B1 & C1).
    assert (valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT)) as K0.
      eapply msk_straight_preserve; eauto.
    clear H0.
    exploit transl_cond_int64u_correct; eauto. intros (rs2 & A2 & B2 & C2).
    exists rs2; split.
    eapply exec_straight_trans. eexact A1. eexact A2.
    split. simpl in B2. rewrite B1, C1 in B2 by auto with asmgen.
    rewrite B2; auto.
    intros; transitivity (rs1 r); auto. }
  destruct cmp.
+ apply DFL.
+ apply DFL.
+ exploit (opimm64_correct Psltul Psltiul (fun v1 v2 =>
         Val.maketotal (Val.cmplu (Mem.valid_pointer m) Clt v1 v2)) m); eauto.
  intros (rs1 & A1 & B1 & C1).
  exists rs1; split. eexact A1. split; auto. rewrite B1; auto.
+ apply DFL.
+ apply DFL.
+ apply DFL.
Qed.


Lemma transl_cond_op_correct:
  forall blk cond rd args k c rs m,
  transl_cond_op cond rd args k = OK c ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
  exec_straight ge fn blk c rs m k rs' m
  /\ Val.lessdef (Val.of_optbool (eval_condition cond
                                       (map rs (map preg_of args)) m)) rs'#rd
  /\ forall r, r <> PC -> r <> rd -> r <> X31 -> r <> MSK_CNT ->
               r <> MSK_BRN -> rs'#r = rs#r.
Proof.

  assert (MKTOT: forall ob, Val.of_optbool ob =
                            Val.maketotal (option_map Val.of_bool ob)).
  { destruct ob as [[]|]; reflexivity. }
  intros until m; intros TR MM.
  destruct cond; simpl in TR; ArgsInv.
+ (* cmp *)
  exploit transl_cond_int32s_correct; eauto. intros (rs' & A & B & C).
  exists rs'; eauto.
+ (* cmpu *)
  exploit transl_cond_int32u_correct; eauto. intros (rs' & A & B & C).
  exists rs'; repeat split; eauto. rewrite B; auto.
+ (* cmpimm *)
  apply transl_condimm_int32s_correct; eauto with asmgen.
+ (* cmpuimm *)
  apply transl_condimm_int32u_correct; eauto with asmgen.
+ (* cmpl *)
  exploit transl_cond_int64s_correct; eauto. intros (rs' & A & B & C).
  exists rs'; repeat split; eauto. rewrite MKTOT; eauto.
+ (* cmplu *)
  exploit transl_cond_int64u_correct; eauto. intros (rs' & A & B & C).
  exists rs'; repeat split; eauto. rewrite B, MKTOT; eauto.
+ (* cmplimm *)
  exploit transl_condimm_int64s_correct; eauto.
  instantiate (1 := x); eauto with asmgen.
  intros (rs' & A & B & C).
  exists rs'; repeat split; eauto. rewrite MKTOT; eauto.
+ (* cmpluimm *)
  exploit transl_condimm_int64u_correct; eauto.
  instantiate (1 := x); eauto with asmgen.
  intros (rs' & A & B & C).
  exists rs'; repeat split; eauto. rewrite MKTOT; eauto.
+ (* cmpf *)
  destruct (transl_cond_float c0 rd x x0) as [insn normal] eqn:TR.
  fold (Val.cmpf c0 (rs x) (rs x0)).
  set (v := Val.cmpf c0 (rs x) (rs x0)).
  destruct normal; inv EQ2.
* econstructor; split.
  apply exec_straight_one. auto.
  eapply transl_cond_float_correct with (v := v); eauto. auto. auto.
  split; intros; Simpl.
* econstructor; split.
  eapply exec_straight_two. auto.
  2: { eapply transl_cond_float_correct with (v := Val.notbool v); eauto. }

  Simpl.
  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity.
  auto. auto. auto. auto.
  split; intros; Simpl. unfold v, Val.cmpf.
  destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; auto.
+ (* notcmpf *)
  destruct (transl_cond_float c0 rd x x0) as [insn normal] eqn:TR.
  rewrite Val.notbool_negb_3. fold (Val.cmpf c0 (rs x) (rs x0)).
  set (v := Val.cmpf c0 (rs x) (rs x0)).
  destruct normal; inv EQ2.
* econstructor; split.
  eapply exec_straight_two. auto.
  2: { eapply transl_cond_float_correct with (v := v); eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity.
  auto. auto. auto. auto.
  split; intros; Simpl. unfold v, Val.cmpf.
  destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; auto.
* econstructor; split.
  apply exec_straight_one. auto.
  eapply transl_cond_float_correct with (v := Val.notbool v); eauto. auto.
  auto.
  split; intros; Simpl.
+ (* cmpfs *)
  destruct (transl_cond_single c0 rd x x0) as [insn normal] eqn:TR.
  fold (Val.cmpfs c0 (rs x) (rs x0)).
  set (v := Val.cmpfs c0 (rs x) (rs x0)).
  destruct normal; inv EQ2.
* econstructor; split.
  apply exec_straight_one. auto.
  eapply transl_cond_single_correct with (v := v); eauto. auto. auto.
  split; intros; Simpl.
* econstructor; split.
  eapply exec_straight_two. auto.
  2: { eapply transl_cond_single_correct with (v := Val.notbool v); eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity.
  auto. auto. auto. auto.
  split; intros; Simpl. unfold v, Val.cmpfs.
  destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; auto.
+ (* notcmpfs *)
  destruct (transl_cond_single c0 rd x x0) as [insn normal] eqn:TR.
  rewrite Val.notbool_negb_3. fold (Val.cmpfs c0 (rs x) (rs x0)).
  set (v := Val.cmpfs c0 (rs x) (rs x0)).
  destruct normal; inv EQ2.
* econstructor; split.
  eapply exec_straight_two. auto.
  2: { eapply transl_cond_single_correct with (v := v); eauto. }

  unfold nextinstr.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity.
  auto. auto. auto. auto.
  split; intros; Simpl. unfold v, Val.cmpfs.
  destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; auto.
* econstructor; split.
  apply exec_straight_one. auto.
  eapply transl_cond_single_correct with (v := Val.notbool v); eauto. auto.
  auto.
  split; intros; Simpl.
Qed.

(** Some arithmetic properties. *)

Remark cast32unsigned_from_cast32signed:
  forall i, Int64.repr (Int.unsigned i) =
            Int64.zero_ext 32 (Int64.repr (Int.signed i)).
Proof.
  intros. apply Int64.same_bits_eq; intros.
  rewrite Int64.bits_zero_ext, !Int64.testbit_repr by tauto.
  rewrite Int.bits_signed by tauto. fold (Int.testbit i i0).
  change Int.zwordsize with 32.
  destruct (zlt i0 32). auto. apply Int.bits_above. auto.
Qed.

(* Translation of arithmetic operations *)

Lemma transl_op_correct:
  forall blk op args res k (rs: regset) m v c,
  transl_op op args res k = OK c ->
  eval_operation ge (rs#SP) op (map rs (map preg_of args)) m = Some v ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m
  /\ Val.lessdef v rs'#(preg_of res)
  /\ forall r, data_preg r = true ->
      r <> preg_of res -> preg_notin r (destroyed_by_op op) -> rs' r = rs r.
Proof.
  assert (SAME: forall v1 v2, v1 = v2 -> Val.lessdef v2 v1).
  { intros; subst; auto. }                                                      Opaque Int.eq.
  intros until c; intros TR EV MM.
  unfold transl_op in TR.
  destruct op; ArgsInv; simpl in EV;
    SimplEval EV; try TranslOpSimplA.
- (* move *)
  destruct (preg_of res), (preg_of m0); inv TR.
  econstructor. split. eapply exec_straight_one. auto.
  simpl. reflexivity.
  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

  econstructor. split. eapply exec_straight_one. auto.
  simpl. reflexivity.
  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

- (* intconst *)
  exploit loadimm32_correct; eauto. intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.
- (* longconst *)
  exploit loadimm64_correct; eauto. intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.
- (* floatconst *)
  destruct (Float.eq_dec n Float.zero).
+ subst n. econstructor; split.
  apply exec_straight_one. simpl; eauto. auto.
  split; intros; Simpl.

  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

+ econstructor; split.
  apply exec_straight_one. simpl; eauto. auto.
  split; intros; Simpl.

  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

- (* singleconst *)
  destruct (Float32.eq_dec n Float32.zero).
+ subst n. econstructor; split.
  apply exec_straight_one. simpl; eauto. auto.
  split; intros; Simpl.

  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

+ econstructor; split.
  apply exec_straight_one. simpl; eauto. auto.
  split; intros; Simpl.

  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

- (* addrsymbol *)
  destruct (Archi.pic_code tt && negb (Ptrofs.eq ofs Ptrofs.zero)).
+ set (rs1 := nextinstr (rs#x <- (Genv.symbol_address ge id Ptrofs.zero))).
  exploit (addptrofs_correct blk x x ofs k rs1 m); eauto with asmgen.

  subst rs1. Simpl. unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs2 & A & B & C).
  exists rs2; split.
  apply exec_straight_step with rs1 m; auto.
  split. replace ofs with (Ptrofs.add Ptrofs.zero ofs)
    by (apply Ptrofs.add_zero_l).
  rewrite Genv.shift_symbol_address.
  replace (rs1 x) with (Genv.symbol_address ge id Ptrofs.zero) in B
    by (unfold rs1; Simpl).
  exact B.
  intros. rewrite C by eauto with asmgen. unfold rs1; Simpl.
+ (* TranslOpSimpl. *)
  econstructor. split. eapply exec_straight_one. auto.
  simpl. reflexivity.
  Simpl.
  unfold nextinstr. Simpl.
  split. Simpl.
  intros. Simpl.

- (* stackoffset *)
  exploit addptrofs_correct. instantiate (1 := X2); auto with asmgen.
  eauto. intros (rs' & A & B & C).
  exists rs'; split; eauto. auto with asmgen.
- (* cast8signed *)
  econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl;eauto. }
  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl;eauto. auto. auto. auto. auto.
  split; intros; Simpl.
  assert (A: Int.ltu (Int.repr 24) Int.iwordsize = true) by auto.
  destruct (rs x0); auto; simpl. rewrite A; simpl. rewrite A.
  apply Val.lessdef_same. f_equal. apply Int.sign_ext_shr_shl.
  split; reflexivity.
- (* cast16signed *)
  econstructor; split.
  eapply exec_straight_two. auto.
  2: { simpl;eauto. }
  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl;eauto. simpl;eauto. auto. auto. auto.
  split; intros; Simpl.
  assert (A: Int.ltu (Int.repr 16) Int.iwordsize = true) by auto.
  destruct (rs x0); auto; simpl. rewrite A; simpl. rewrite A.
  apply Val.lessdef_same. f_equal. apply Int.sign_ext_shr_shl.
  split; reflexivity.

- (* addimm *)
  exploit (opimm32_correct Paddw Paddiw Val.add); auto.
  instantiate (1 := x0); eauto with asmgen.
  eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* andimm *)
  exploit (opimm32_correct Pandw Pandiw Val.and); auto.
  instantiate (1 := x0); eauto with asmgen.
  eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* orimm *)
  exploit (opimm32_correct Porw Poriw Val.or); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.
(* - admit. *)
- (* xorimm *)
  exploit (opimm32_correct Pxorw Pxoriw Val.xor); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* shrximm *)
  clear H. exploit Val.shrx_shr_2; eauto. intros E; subst v; clear EV.
  destruct (Int.eq n Int.zero).
+ econstructor; split. apply exec_straight_one. auto. simpl; eauto.
  auto. auto.
  split; intros; Simpl.
+ change (Int.repr 32) with Int.iwordsize.
  set (n' := Int.sub Int.iwordsize n).
  econstructor; split.
  eapply exec_straight_step. auto. simpl; reflexivity. auto. auto.
  eapply exec_straight_step.

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity. auto. auto.
  eapply exec_straight_step.

  unfold nextinstr. Simpl.
  inversion MM; subst.
  rewrite H.
  rewrite H0.

  eapply valid_mask_incr2.

  simpl; reflexivity. auto. auto.
  apply exec_straight_one.

  unfold nextinstr. Simpl.

  eapply valid_mask_at_pc_lemma3; eauto.

  simpl; reflexivity. auto. auto.
  split; intros; Simpl.

- (* longofintu *)
  econstructor; split.
  eapply exec_straight_three. auto.
  3: { simpl; eauto. }

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  2: { simpl; eauto. }

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma2; eauto.

  simpl; eauto. auto. auto. auto. auto. auto. auto.
  split; intros; Simpl. destruct (rs x0); auto. simpl.
  assert (A: Int.ltu (Int.repr 32) Int64.iwordsize' = true) by auto.
  rewrite A; simpl. rewrite A. apply Val.lessdef_same. f_equal.
  rewrite cast32unsigned_from_cast32signed. apply Int64.zero_ext_shru_shl.
  compute; auto.

- (* addlimm *)
  exploit (opimm64_correct Paddl Paddil Val.addl); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* andimm *)
  exploit (opimm64_correct Pandl Pandil Val.andl); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.
(* - admit.  *)
- (* orimm *)
  exploit (opimm64_correct Porl Poril Val.orl); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* xorimm *)
  exploit (opimm64_correct Pxorl Pxoril Val.xorl); auto.
  instantiate (1 := x0); eauto with asmgen. eauto.
  intros (rs' & A & B & C).
  exists rs'; split; eauto. rewrite B; auto with asmgen.

- (* shrxlimm *)
  clear H. exploit Val.shrxl_shrl_2; eauto. intros E; subst v; clear EV.
  destruct (Int.eq n Int.zero).
+ econstructor; split. apply exec_straight_one. auto. simpl; eauto.
  auto. auto.
  split; intros; Simpl.
+ change (Int.repr 64) with Int64.iwordsize'.
  set (n' := Int.sub Int64.iwordsize' n).
  econstructor; split.
  eapply exec_straight_step. auto. simpl; reflexivity. auto. auto.
  eapply exec_straight_step.

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; reflexivity. auto.  auto.

  eapply exec_straight_step.

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma2; eauto.

  simpl; reflexivity. auto. auto.

  apply exec_straight_one.

  unfold nextinstr. Simpl.
  eapply valid_mask_at_pc_lemma3; eauto.

  simpl; reflexivity. auto. auto.
  split; intros; Simpl.

- (* cond *)
  exploit transl_cond_op_correct; eauto. intros (rs' & A & B & C).
  exists rs'; split. eexact A. split. auto. intros. eapply C.
  eauto with asmgen. auto. eauto with asmgen. eauto with asmgen.
  eauto with asmgen.
Qed.


(** Memory accesses *)

Lemma indexed_memory_access_correct:
  forall blk mk_instr base ofs k rs m,
  base <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists base' ofs' rs',
    exec_straight_opt ge fn blk
                      (indexed_memory_access mk_instr base ofs k) rs m
                       (mk_instr base' ofs' :: k) rs' m
  /\ Val.offset_ptr rs'#base' (eval_offset ge ofs') =
     Val.offset_ptr rs#base ofs
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT-> rs'#r = rs#r.
Proof.
  unfold indexed_memory_access; intros.
  destruct Archi.ptr64 eqn:SF.
- generalize (make_immed64_sound (Ptrofs.to_int64 ofs)); intros EQ.
  destruct (make_immed64 (Ptrofs.to_int64 ofs)).
+ econstructor; econstructor; econstructor; split.
  apply exec_straight_opt_refl.
  split; auto. simpl. subst imm.
  rewrite Ptrofs.of_int64_to_int64 by auto. auto.
+ econstructor; econstructor; econstructor; split.
  constructor. eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. simpl.
  destruct (rs base); eauto; simpl; eauto.
  rewrite SF. simpl.
  rewrite Ptrofs.add_assoc. f_equal. f_equal.
  rewrite <- (Ptrofs.of_int64_to_int64 SF ofs). rewrite EQ.
  symmetry; auto with ptrofs.
+ econstructor; econstructor; econstructor; split.
  constructor. eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. unfold eval_offset.
  destruct (rs base); auto; simpl. rewrite SF. simpl.
  rewrite Ptrofs.add_zero. subst imm.
  rewrite Ptrofs.of_int64_to_int64 by auto. auto.
- generalize (make_immed32_sound (Ptrofs.to_int ofs)); intros EQ.
  destruct (make_immed32 (Ptrofs.to_int ofs)).
+ econstructor; econstructor; econstructor; split.
  apply exec_straight_opt_refl.
  split; auto. simpl. subst imm. rewrite Ptrofs.of_int_to_int by auto. auto.
+ econstructor; econstructor; econstructor; split.
  constructor. eapply exec_straight_two. auto.
  2: { simpl; eauto. }

  unfold nextinstr; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  simpl; eauto. auto. auto. auto. auto.
  split; intros; Simpl. destruct (rs base); auto; simpl. rewrite SF. simpl.
  rewrite Ptrofs.add_assoc. f_equal. f_equal.
  rewrite <- (Ptrofs.of_int_to_int SF ofs). rewrite EQ.
  symmetry; auto with ptrofs.
Qed.


Lemma indexed_load_access_correct:
  forall chunk (mk_instr: ireg -> offset -> instruction) rd m blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr base ofs) rs m =
      exec_load ge chunk rs m rd base ofs) ->
  forall (base: ireg) ofs k (rs: regset) v,
  Mem.loadv chunk m (Val.offset_ptr rs#base ofs) = Some v ->
  base <> X31 -> rd <> PC -> rd <> MSK_CNT ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
    exec_straight ge fn blk
              (indexed_memory_access mk_instr base ofs k) rs m k rs' m
  /\ rs'#rd = v
  /\ forall r, r <> PC -> r <> X31 -> r <> rd -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.

  intros until blk; intros EXEC.
  intros until v; intros LOAD NOT31 NOTPC NOTMSK MM.
  exploit indexed_memory_access_correct; eauto.
  intros (base' & ofs' & rs' & A & B & C).
  econstructor; split.
  eapply exec_straight_opt_right. eexact A. apply exec_straight_one.

  eapply msk_straight_opt_preserve; eauto.

  rewrite EXEC.
  unfold exec_load. rewrite B. rewrite LOAD.
  eauto. Simpl.
  unfold nextinstr.
  Simpl.
  split; intros; Simpl.
Qed.


Lemma indexed_store_access_correct:
  forall chunk (mk_instr: ireg -> offset -> instruction) r1 m blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr base ofs) rs m =
      exec_store ge chunk rs m r1 base ofs) ->
  forall (base: ireg) ofs k (rs: regset) m',
  Mem.storev chunk m (Val.offset_ptr rs#base ofs) (rs#r1) = Some m' ->
  base <> X31 -> r1 <> X31 -> r1 <> PC -> r1 <> MSK_CNT ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
    exec_straight ge fn blk
                  (indexed_memory_access mk_instr base ofs k) rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros until blk;
    intros EXEC; intros until m'.
  intros STORE NOT31 NOT31' NOTPC NOTMSK MM.
  exploit indexed_memory_access_correct; eauto.
  intros (base' & ofs' & rs' & A & B & C).
  econstructor; split.
  eapply exec_straight_opt_right. eexact A. apply exec_straight_one.
  eapply msk_straight_opt_preserve; eauto.
  rewrite EXEC.
  unfold exec_store. rewrite B, C, STORE.
  eauto. auto. auto. auto. auto. auto.
  intros; Simpl.
Qed.


Lemma loadind_correct:
  forall (base: ireg) ofs ty dst k c (rs: regset) m v blk,
  loadind base ofs ty dst k = OK c ->
  Mem.loadv (chunk_of_type ty) m (Val.offset_ptr rs#base ofs) = Some v ->
  base <> X31 -> preg_of dst <> PC -> preg_of dst <> MSK_CNT ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m
  /\ rs'#(preg_of dst) = v
  /\ forall r, r <> PC -> r <> X31 -> r <> preg_of dst -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  intros until blk; intros TR LOAD NOT31 NOTPC NOTMSK MM.
  assert (A: exists mk_instr,
                c = indexed_memory_access mk_instr base ofs k
             /\ forall base' ofs' rs',
                   exec_instr ge blk fn (mk_instr base' ofs') rs' m =
                   exec_load ge (chunk_of_type ty) rs' m
                             (preg_of dst) base' ofs').
  { unfold loadind in TR.
    destruct ty, (preg_of dst); inv TR; econstructor; split; eauto. }
  destruct A as (mk_instr & B & C). subst c.
  eapply indexed_load_access_correct. eauto with asmgen.
  auto. auto. auto. auto. auto.
Qed.


Lemma storeind_correct:
  forall (base: ireg) ofs ty src k c (rs: regset) m m' blk,
  storeind src base ofs ty k = OK c ->
  Mem.storev (chunk_of_type ty) m
             (Val.offset_ptr rs#base ofs) rs#(preg_of src) = Some m' ->
  base <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros until blk; intros TR STORE NOT31.
  assert (A: exists mk_instr,
                c = indexed_memory_access mk_instr base ofs k
             /\ forall base' ofs' rs',
                   exec_instr ge blk fn (mk_instr base' ofs') rs' m =
                   exec_store ge (chunk_of_type ty) rs' m (preg_of src)
                              base' ofs').
  { unfold storeind in TR.
    destruct ty, (preg_of src); inv TR; econstructor; split; eauto. }
  destruct A as (mk_instr & B & C). subst c.
  eapply indexed_store_access_correct; eauto with asmgen.
Qed.


Lemma loadind_ptr_correct:
  forall (base: ireg) ofs (dst: ireg) k (rs: regset) m v blk,
  Mem.loadv Mptr m (Val.offset_ptr rs#base ofs) = Some v ->
  base <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (loadind_ptr base ofs dst k) rs m k rs' m
  /\ rs'#dst = v
  /\ forall r, r <> PC -> r <> X31 -> r <> dst -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  intros. eapply indexed_load_access_correct; eauto with asmgen.
  intros. unfold Mptr. destruct Archi.ptr64; auto.
Qed.

Lemma loadind_mskptr_correct:
  forall (base: ireg) ofs (dst: mskreg) k (rs: regset) m v blk,
  Mem.loadv Mptr m (Val.offset_ptr rs#base ofs) = Some v ->
  base <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  Archi.ptr64 = false ->

  exists rs',
     exec_straight ge fn blk (loadind_mskptr base ofs dst k) rs m k rs' m
  /\ rs'#dst = v
  /\ forall r, r <> PC -> r <> X31 -> r <> dst -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  intros. eapply indexed_load_access_correct; eauto with asmgen.
  intros. unfold Mptr. destruct Archi.ptr64; auto.
  simpl. inversion H2.
Qed.


Lemma storeind_ptr_correct:
  forall (base: ireg) ofs (src: ireg) k (rs: regset) m m' blk,
  Mem.storev Mptr m (Val.offset_ptr rs#base ofs) rs#src = Some m' ->
  base <> X31 -> src <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk (storeind_ptr src base ofs k) rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  intros. eapply indexed_store_access_correct with (r1 := src);
            eauto with asmgen.
  intros. unfold Mptr. destruct Archi.ptr64; auto.
Qed.

Lemma storeind_mskptr_correct:
  forall (base: ireg) ofs (src: mskreg) k (rs: regset) m m' blk,
  Mem.storev Mptr m (Val.offset_ptr rs#base ofs) rs#src = Some m' ->
  base <> X31 ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  Archi.ptr64 = false ->

  exists rs',
     exec_straight ge fn blk (storeind_mskptr src base ofs k) rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros.
  eapply indexed_store_access_correct with (r1 := src); eauto with asmgen.
  intros. unfold Mptr. destruct Archi.ptr64; auto. simpl.
  inversion H2.
Qed.


Lemma transl_memory_access_correct:
  forall mk_instr addr args k c (rs: regset) m v blk,
  transl_memory_access mk_instr addr args k = OK c ->
  eval_addressing ge rs#SP addr (map rs (map preg_of args)) = Some v ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists base ofs rs',
     exec_straight_opt ge fn blk c rs m (mk_instr base ofs :: k) rs' m
  /\ Val.offset_ptr rs'#base (eval_offset ge ofs) = v
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT
                -> rs'#r = rs#r.
Proof.
  intros until blk; intros TR EV MM.
  unfold transl_memory_access in TR; destruct addr; ArgsInv.
- (* indexed *)
  inv EV. apply indexed_memory_access_correct; eauto with asmgen.
- (* global *)
  simpl in EV. inv EV. inv TR.
  econstructor; econstructor; econstructor; split.
  constructor. apply exec_straight_one. auto.
  simpl; eauto. auto. auto.
  split; intros; Simpl. unfold eval_offset. apply low_high_half.
- (* stack *)
  inv TR. inv EV. apply indexed_memory_access_correct; eauto with asmgen.
Qed.


Lemma transl_load_access_correct:
  forall chunk (mk_instr: ireg -> offset -> instruction)
         addr args k c rd (rs: regset) m v v' blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr base ofs) rs m =
      exec_load ge chunk rs m rd base ofs) ->
  transl_memory_access mk_instr addr args k = OK c ->
  eval_addressing ge rs#SP addr (map rs (map preg_of args)) = Some v ->
  Mem.loadv chunk m v = Some v' ->
  rd <> PC -> rd <> MSK_CNT ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m
  /\ rs'#rd = v'
  /\ forall r, r <> PC -> r <> X31 -> r <> rd -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros until blk; intros INSTR TR EV LOAD NOTPC NOTMC MM.
  exploit transl_memory_access_correct; eauto.
  intros (base & ofs & rs' & A & B & C).
  econstructor; split.
  eapply exec_straight_opt_right. eexact A. apply exec_straight_one.
  eapply msk_straight_opt_preserve; eauto.
  rewrite INSTR. unfold exec_load. rewrite B, LOAD. reflexivity. Simpl.
  unfold nextinstr. rewrite Pregmap.gss.  rewrite Pregmap.gso.
  rewrite Pregmap.gso. reflexivity.
  eauto. eauto.
  split; intros; Simpl.
Qed.


Lemma transl_store_access_correct:
  forall chunk (mk_instr: ireg -> offset -> instruction)
         addr args k c r1 (rs: regset) m v m' blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr base ofs) rs m =
      exec_store ge chunk rs m r1 base ofs) ->
  transl_memory_access mk_instr addr args k = OK c ->
  eval_addressing ge rs#SP addr (map rs (map preg_of args)) = Some v ->
  Mem.storev chunk m v rs#r1 = Some m' ->
  r1 <> PC -> r1 <> X31 -> r1 <> MSK_CNT ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros until blk; intros INSTR TR EV STORE NOTPC NOT31 NOTMSK MM.
  exploit transl_memory_access_correct; eauto.
  intros (base & ofs & rs' & A & B & C).
  econstructor; split.
  eapply exec_straight_opt_right. eexact A. apply exec_straight_one.
  eapply msk_straight_opt_preserve; eauto.
  rewrite INSTR. unfold exec_store. rewrite B, C, STORE by auto.
  reflexivity. auto.
  auto.
  intros; Simpl.
Qed.


Lemma transl_load_correct:
  forall chunk addr args dst k c (rs: regset) m a v blk,
  transl_load chunk addr args dst k = OK c ->
  eval_addressing ge rs#SP addr (map rs (map preg_of args)) = Some a ->
  Mem.loadv chunk m a = Some v ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m
  /\ rs'#(preg_of dst) = v
  /\ forall r, r <> PC -> r <> X31 -> r <> preg_of dst -> r <> MSK_CNT
               -> rs'#r = rs#r.
Proof.
  intros until blk; intros TR EV LOAD.
  assert (A: exists mk_instr,
      transl_memory_access mk_instr addr args k = OK c
   /\ forall base ofs rs,
               exec_instr ge blk fn (mk_instr base ofs) rs m =
               exec_load ge chunk rs m (preg_of dst) base ofs).
  { unfold transl_load in TR; destruct chunk; ArgsInv; econstructor;
      (split; [eassumption|auto]). }
  destruct A as (mk_instr & B & C).
  eapply transl_load_access_correct; eauto with asmgen.
Qed.


Lemma transl_store_correct:
  forall chunk addr args src k c (rs: regset) m a m' blk,
  transl_store chunk addr args src k = OK c ->
  eval_addressing ge rs#SP addr (map rs (map preg_of args)) = Some a ->
  Mem.storev chunk m a rs#(preg_of src) = Some m' ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs',
     exec_straight ge fn blk c rs m k rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs'#r = rs#r.
Proof.
  intros until blk; intros TR EV STORE.
  assert (A: exists mk_instr chunk',
      transl_memory_access mk_instr addr args k = OK c
   /\ (forall base ofs rs,
          exec_instr ge blk fn (mk_instr base ofs) rs m =
          exec_store ge chunk' rs m (preg_of src) base ofs)
   /\ Mem.storev chunk m a rs#(preg_of src) = Mem.storev chunk' m a
                                                         rs#(preg_of src)).
  { unfold transl_store in TR; destruct chunk; ArgsInv;
      (econstructor; econstructor; split; [eassumption | split;
                               [ intros; simpl; reflexivity | auto]]).
    destruct a; auto. apply Mem.store_signed_unsigned_8.
    destruct a; auto. apply Mem.store_signed_unsigned_16.
  }
  destruct A as (mk_instr & chunk' & B & C & D).
  rewrite D in STORE; clear D.
  eapply transl_store_access_correct; eauto with asmgen.
Qed.


(** Function epilogues *)

Lemma loadind_ptr_mskptr_correct:
  forall (base: ireg) ofs1 ofs2 (dst: ireg) (mdst: mskreg) k
         (rs: regset) m v1 v2 blk,
  Mem.loadv Mptr m (Val.offset_ptr rs#base ofs1) = Some v1 ->
  Mem.loadv Mptr m (Val.offset_ptr rs#base ofs2) = Some v2 ->
  base <> X31 ->
  dst <> X31 ->
  base <> dst ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  Archi.ptr64 = false ->
  exists rs',
    exec_straight ge fn blk (loadind_ptr base ofs1 dst
                 (loadind_mskptr base ofs2 mdst k))
                  rs m k rs' m
  /\ rs'#dst = v1
  /\ rs'#mdst = v2
  /\ forall r, r <> PC -> r <> X31 -> r <> dst -> r <> mdst ->
               r <> MSK_CNT ->
               rs'#r = rs#r.
Proof.
  intros.

  exploit (loadind_ptr_correct base ofs1 dst
       (loadind_mskptr base ofs2 mdst k)
       rs m v1 blk).
  assumption.
  assumption.
  assumption.

  intros (rs' & A & B & C).

  assert (rs' base = rs base) as qq.
  eapply C.
  discriminate.
  congruence.
  congruence.
  discriminate.
  assert (Mem.loadv Mptr m (Val.offset_ptr (rs' base) ofs2) = Some v2) as H6.
  rewrite qq.
  assumption.

  exploit (loadind_mskptr_correct base ofs2 mdst k rs' m v2 blk).
  assumption.
  assumption.
(*  assumption. *)

  eapply msk_straight_preserve; eauto.
  assumption.

  intros (rs2 & A2 & B2 & C2).

  econstructor.
  split.
  eapply exec_straight_trans.
  eexact A.
  eexact A2.

  split.
  rewrite C2.
  assumption.
  congruence.

  congruence.

  congruence.
  congruence.

  split.
  assumption.

  intros.
  rewrite C2.
  eapply C.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
Qed.


Lemma make_epilogue_correct:
  forall ge0 f m stk soff cs m' ms rs k tm blk mr,
  Archi.ptr64 = false ->
  load_stack m (Vptr stk soff) Tptr f.(fn_link_ofs) = Some (parent_sp cs) ->
  load_stack m (Vptr stk soff) Tptr f.(fn_retaddr_ofs) = Some (parent_ra cs) ->
  load_stack m (Vptr stk soff) Tptr f.(fn_retmsk_ofs) = Some (parent_msk cs) ->
  Mem.free m stk 0 f.(fn_stacksize) = Some m' ->
  agree ms (Vptr stk soff) rs ->
  Mem.extends m tm ->
  match_stack ge0 cs ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs', exists tm',
     exec_straight ge fn blk (make_epilogue f mr k) rs tm k rs' tm'
  /\ agree ms (parent_sp cs) rs'
  /\ Mem.extends m' tm'
  /\ rs'#mr = parent_msk cs
  /\ rs'#RA = parent_ra cs
  /\ rs'#SP = parent_sp cs
  /\ (forall r, r <> PC -> r <> RA -> r <> SP ->
                r <> X31 -> r <> mr ->
                r <> MSK_CNT ->
                rs'#r = rs#r).
Proof.
  intros until mr. intros h34 LP LRA LMSK FREE AG MEXT MCS MM.

  exploit Mem.loadv_extends. eauto. eexact LP. auto. simpl.
     intros (parent' & LP' & LDP').
  exploit Mem.loadv_extends. eauto. eexact LRA. auto. simpl.
     intros (ra' & LRA' & LDRA').
  exploit Mem.loadv_extends. eauto. eexact LMSK. auto. simpl.
     intros (msk' & LMSK' & LDMSK').

  exploit lessdef_parent_sp; eauto. intros EQ; subst parent'; clear LDP'.
  exploit lessdef_parent_ra; eauto. intros EQ; subst ra'; clear LDRA'.
  exploit lessdef_parent_msk; eauto. intros EQ. subst msk'; clear LDMSK'.

  exploit Mem.free_parallel_extends; eauto. intros (tm' & FREE' & MEXT').
  unfold make_epilogue.
  rewrite chunk_of_Tptr in *.

  exploit (loadind_ptr_mskptr_correct SP (fn_retaddr_ofs f) (fn_retmsk_ofs f)
                                      RA mr (Pfreeframe (fn_stacksize f)
                                                        (fn_link_ofs f) :: k)
                                      rs tm).
  rewrite <- (sp_val _ _ _ AG). simpl. eexact LRA'.

  rewrite <- (sp_val _ _ _ AG). simpl. eexact LMSK'.

  congruence.
  congruence.
  congruence.
  assumption.
  assumption.

  intros (rs1 & A1 & B1 & C1 & D1).
  econstructor; econstructor; split.
  eapply exec_straight_trans.
  instantiate (1:=blk) in A1.
  eexact A1. apply exec_straight_one.

  eapply msk_straight_preserve; eauto.

  simpl.
  rewrite (D1 X2) by auto with asmgen.
  rewrite <- (sp_val _ _ _ AG). simpl; rewrite LP'.
    rewrite FREE'. eauto. auto.

  unfold nextinstr.
  Simpl.

  split. apply agree_nextinstr. apply agree_set_other; auto with asmgen.
    apply agree_change_sp with (Vptr stk soff).
    apply agree_exten with rs; auto. intros; apply D1; auto with asmgen.
    eapply parent_sp_def; eauto.
  split. auto.
  split. unfold nextinstr.
  rewrite Pregmap.gso.
  rewrite Pregmap.gso.
  rewrite Pregmap.gso.
  rewrite Pregmap.gso.
  assumption.
  congruence.
  congruence.
  congruence.
  congruence.

  split. Simpl.
  split. Simpl.
  intros. Simpl.
Qed.



(**************************************************************************)
(************* NOT USED **************************************************)


Lemma transl_cbranch_correct_2a:
  forall cond args lbl k c m ms b sp rs m' blk,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some b ->
  agree ms sp rs ->
  Mem.extends m m' ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs', exists insn,
   exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
           (insn :: k) rs' m'
  /\ exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b)
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros until blk; intros TRANSL EVAL AG MEXT MM.
  set (vl' := map rs (map preg_of args)).
  assert (EVAL': eval_condition cond vl' m' = Some b).
  { apply eval_condition_lessdef with (map ms args) m; auto. eapply preg_vals; eauto. }
  clear EVAL MEXT AG.

  eapply transl_cbranch_correct_Aux6a.

  remember (label_pos lbl 0 (fn_code fn)) as w.
  destruct w.

  rename z into zpos.

  econstructor. split. auto.

  split. simpl. unfold exec_loadCodeMSK_byLabel.

  rewrite <- Heqw.
  unfold exec_loadCodeMSK. unfold nextinstr. rewrite Pregmap.gso.
  rewrite Pregmap.gso.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  reflexivity.
  discriminate.
  discriminate.

  Simpl.

  split. auto.
  split. auto.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  assert (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs3#r = rs#r) as DD.
  intros.
  subst rs3.
  subst rs2.
  Simpl.

  destruct cond; simpl in TRANSL; ArgsInv.

- exists rs3, (transl_cbranch_int32s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
- exists rs3, (transl_cbranch_int32u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                 (transl_cbranch_int32s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                      (transl_cbranch_int32u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- exists rs3, (transl_cbranch_int64s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
- exists rs3, (transl_cbranch_int64u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
+ exploit (loadimm64_correct blk X31 n
                    (transl_cbranch_int64s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
+ exploit (loadimm64_correct blk X31 n
      (transl_cbranch_int64u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros. Simpl.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpf_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpfs_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.

-
  econstructor. split. auto.

  split. simpl. unfold exec_loadCodeMSK_byLabel.

  rewrite <- Heqw.
  unfold exec_loadCodeMSK. unfold nextinstr. rewrite Pregmap.gso.
  rewrite Pregmap.gso.
  reflexivity.

  discriminate.
  discriminate.

  Simpl.

  split. auto.
  split. auto.

  set (rs2 := rs # MSK_BRN <- (Vint (generate_mskA blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  assert (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs3#r = rs#r) as DD.
  intros.
  subst rs2.
  subst rs3.
  Simpl.

  destruct cond; simpl in TRANSL; ArgsInv.

+ exists rs3, (transl_cbranch_int32s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
+ exists rs3, (transl_cbranch_int32u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
+ predSpec Int.eq Int.eq_spec n Int.zero.
* subst n. exists rs3, (transl_cbranch_int32s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
* exploit (loadimm32_correct blk X31 n
                 (transl_cbranch_int32s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
+ predSpec Int.eq Int.eq_spec n Int.zero.
* subst n. exists rs3, (transl_cbranch_int32u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
* exploit (loadimm32_correct blk X31 n
                      (transl_cbranch_int32u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
+ exists rs3, (transl_cbranch_int64s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
+ exists rs3, (transl_cbranch_int64u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
+ predSpec Int64.eq Int64.eq_spec n Int64.zero.
* subst n. exists rs3, (transl_cbranch_int64s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
* exploit (loadimm64_correct blk X31 n
                    (transl_cbranch_int64s c0 x X31 lbl :: k) rs3); eauto.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
+ predSpec Int64.eq Int64.eq_spec n Int64.zero.
* subst n. exists rs3, (transl_cbranch_int64u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
* exploit (loadimm64_correct blk X31 n
      (transl_cbranch_int64u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  intros.
  rewrite <- DD. auto. auto. auto. auto. auto.
+ destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros. Simpl.
+ destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpf_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  intros; Simpl.
+ destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.
+ destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpfs_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3; subst rs2; Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  intros; Simpl.
Qed.


Lemma transl_cbranch_correct_Aux6A:
  forall lbl k c b rs m' blk zpos,
    (exists  (rs0: regset),
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                            (exists rs2 insn,
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs2 m' /\
    exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b) /\
    rs2 # MSK_BRN = Vint (encrypt_msk zpos blk) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs2#r = rs#r) ))
    ->
    label_pos lbl 0 (fn_code fn) = Some zpos ->
    exists (rs1 rs2 : regset) (insn : instruction),
      exec_straight_opt ge fn blk
                        (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs2 m'
    /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
    /\ exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b)
    /\ (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r).
  intros.
  destruct H as [rs0 H].
  destruct H.
  destruct H1.
  destruct H2.
  destruct H3.
  destruct H4 as [rs2 H4].
  destruct H4 as [insn H4].
  destruct H4.
  destruct H5.
  destruct H6.
  econstructor 1 with (x:= nextinstr rs # MSK_BRN <-
                                     (Vint (encrypt_msk zpos blk))).
  econstructor 1 with (x:=rs2).
  econstructor 1 with (x:=insn).
  split.
  eapply exec_straight_opt_one; eauto.
  split; auto.
  split; auto.
  intros.
  unfold nextinstr.
  Simpl.

  assert ({r = MSK_BRN} + {r <> MSK_BRN}) as E.
  eapply preg_eq.

  destruct E.
  inversion e; subst.
  Simpl.
  Simpl.
Qed.


Lemma transl_cbranch_correct_1A:
  forall cond args lbl k c m ms b sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some b ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs1, exists rs2, exists insn,
  exec_straight_opt ge fn blk
                (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs m'
           (insn :: k) rs2 m'
  /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
  /\ exec_instr ge blk fn insn rs2 m' = eval_branch fn lbl rs2 m' (Some b)
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r.
Proof.
  intros until zpos; intros TRANSL EVAL AG MEXT LL MM.
  set (vl' := map rs (map preg_of args)).
  assert (EVAL': eval_condition cond vl' m' = Some b).
  { apply eval_condition_lessdef with (map ms args) m; auto. eapply preg_vals; eauto. }
  clear EVAL MEXT AG.

  eapply transl_cbranch_correct_Aux6A.

  econstructor. split. auto.

  split. simpl. unfold exec_loadCodeMSK_byLabel. rewrite LL.
  unfold exec_loadCodeMSK. unfold nextinstr. rewrite Pregmap.gso.
  rewrite Pregmap.gso.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  reflexivity.
  discriminate.
  discriminate.

  Simpl.

  split. auto.
  split. auto.

  set (rs2 := rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))).

  set (rs3 :=  (rs2 # PC <- (Val.offset_ptr (rs PC) Ptrofs.one)) # MSK_CNT <-
    (next_msk_val_v (rs PC) (rs MSK_CNT))).

  assert (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs3#r = rs#r) as DD.
  intros.
  subst rs3.
  subst rs2.
  Simpl.

  destruct cond; simpl in TRANSL; ArgsInv.

- exists rs3, (transl_cbranch_int32s c0 x x0 lbl).
  intuition auto. constructor.
  apply transl_cbranch_int32s_correct; auto.
- exists rs3, (transl_cbranch_int32u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32s_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                 (transl_cbranch_int32s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- predSpec Int.eq Int.eq_spec n Int.zero.
+ subst n. exists rs3, (transl_cbranch_int32u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int32u_correct; auto.
+ exploit (loadimm32_correct blk X31 n
                      (transl_cbranch_int32u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int32u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int32u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- exists rs3, (transl_cbranch_int64s c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
- exists rs3, (transl_cbranch_int64u c0 x x0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64s c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64s_correct; auto.
+ exploit (loadimm64_correct blk X31 n
                    (transl_cbranch_int64s c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64s c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64s_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- predSpec Int64.eq Int64.eq_spec n Int64.zero.
+ subst n. exists rs3, (transl_cbranch_int64u c0 x X0 lbl).
  intuition auto. constructor. apply transl_cbranch_int64u_correct; auto.
+ exploit (loadimm64_correct blk X31 n
      (transl_cbranch_int64u c0 x X31 lbl :: k) rs3); eauto.

  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.

  intros (rs' & A & B & C).
  exists rs', (transl_cbranch_int64u c0 x X31 lbl).
  split. constructor; eexact A. split; auto.
  apply transl_cbranch_int64u_correct; auto.
  simpl; rewrite B, C; eauto with asmgen.
  split.
  rewrite C; eauto; discriminate.
  intros.
  rewrite <- DD; auto.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  split. Simpl.
  intros. Simpl.
- destruct (transl_cond_float c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpf_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpf_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpf c0 rs#x rs#x0 else Val.notbool (Val.cmpf c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpf. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_float_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; Simpl; reflexivity.
  split. Simpl.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (eqb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL'. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  split. Simpl.
  intros; Simpl.
- destruct (transl_cond_single c0 X31 x x0) as [insn normal] eqn:TC; inv EQ2.
  assert (EVAL'': Val.cmpfs_bool c0 (rs x) (rs x0) = Some (negb b)).
  { destruct (Val.cmpfs_bool c0 (rs x) (rs x0)) as [[]|]; inv EVAL'; auto. }
  set (v := if normal then Val.cmpfs c0 rs#x rs#x0 else Val.notbool (Val.cmpfs c0 rs#x rs#x0)).
  assert (V: v = Val.of_bool (xorb normal b)).
  { unfold v, Val.cmpfs. rewrite EVAL''. destruct normal, b; reflexivity. }
  econstructor; econstructor.
  split. constructor. apply exec_straight_one.
  subst rs3.
  subst rs2.
  Simpl.
  eapply valid_mask_at_pc_lemma1; eauto.
  eapply transl_cond_single_correct with (v := v); eauto. auto.
  split. split. rewrite V; destruct normal, b; reflexivity.
  split. Simpl.
  intros; Simpl.
 - auto.
Qed.



Lemma transl_cbranch_correct_trueA:
  forall cond args lbl k c m ms sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some true ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs1, exists rs2, exists insn,
   exec_straight_opt ge fn blk
                          (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c)
                          rs m' (insn :: k) rs2 m'
  /\ rs1 = nextinstr rs # MSK_BRN <- (Vint (encrypt_msk zpos blk))
  /\ exec_instr ge blk fn insn rs2 m' = goto_label fn lbl rs2 m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> rs2#r = rs1#r.
Proof.
  intros. eapply transl_cbranch_correct_1A with (b := true); eauto.
Qed.

Lemma cons_discriminate :
  forall (T: Type) (ls: list T) (d: T), d::ls = ls -> False.
Proof.
  intros.
  assert (length (d::ls) = length ls).
  { rewrite H; auto. }
  simpl in *; lia.
Qed.

Lemma transl_cbranch_correct_Aux1:
  forall lbl k c rs m' blk
         rs' insn (P: regset -> instruction -> Prop),
    (exists (rs0: regset),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\ P rs' insn)
    ->
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' (insn :: k) rs' m' /\ P rs' insn.

intros.
destruct H as [rs0 H].
destruct H as [H H0].
destruct H0 as [H0 H1].
split; auto.
inversion H; subst.
eapply cons_discriminate in H6; intuition.
clear H.

inversion H0; subst.
clear H0.
econstructor.
auto.

clear H0.
econstructor.

eapply exec_straight_trans. eauto. auto.
Qed.


Lemma transl_cbranch_correct_Aux3:
  forall lbl k c rs m' blk
         rs' insn (P: regset -> instruction -> Prop),
    (exists (rs0: regset),
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\ P rs' insn)
    ->
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' (insn :: k) rs' m' /\ P rs' insn.

intros.
destruct H as [rs0 H].
destruct H as [H H0].
destruct H0 as [H0 H1].
split; auto.

inversion H0; subst.
clear H0.
econstructor.
auto.

clear H0.
econstructor.

eapply exec_straight_trans. eauto. auto.
Qed.


Lemma transl_cbranch_correct_Aux2:
  forall lbl k c b rs m' blk,
    (exists rs' insn (rs0: regset),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r) )
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r).
  intros.
  destruct H.
  destruct H.
  destruct H.
  destruct H.
  destruct H0.
  econstructor 1 with (x:=x). econstructor 1 with (x:=x0).
  eapply (transl_cbranch_correct_Aux1 lbl k c rs m' blk
                                      x x0
      (fun x x0 => exec_instr ge blk fn x0 x m' =
                                eval_branch fn lbl x m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> x r = rs r))).
  econstructor 1 with (x:=x1).
  intuition.
Qed.


Lemma transl_cbranch_correct_Aux4:
  forall lbl k c b rs m' blk,
    (exists rs' insn (rs0: regset),
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r) )
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r).
  intros.
  destruct H.
  destruct H.
  destruct H.
  destruct H.
  destruct H0.
  econstructor 1 with (x:=x). econstructor 1 with (x:=x0).
  eapply (transl_cbranch_correct_Aux3 lbl k c rs m' blk
                                      x x0
      (fun x x0 => exec_instr ge blk fn x0 x m' =
                                eval_branch fn lbl x m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> x r = rs r))).
  econstructor 1 with (x:=x1).
  intuition.
Qed.


Lemma transl_cbranch_correct_Aux6:
  forall lbl k c b rs m' blk,
    (exists  (rs0: regset) rs' insn,
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                        (*
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
*)
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r) )
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> rs' r = rs r).
  intros.
  destruct H as [rs0 H].
  destruct H as [rs' H].
  destruct H as [insn].
  destruct H.
  destruct H0.
  destruct H1.
  destruct H2.
  destruct H3.
  destruct H4.
  econstructor 1 with (x:=rs'). econstructor 1 with (x:=insn).
  eapply (transl_cbranch_correct_Aux5 lbl k c rs m' blk
                                      rs' insn
      (fun x x0 => exec_instr ge blk fn x0 x m' =
                                eval_branch fn lbl x m' (Some b) /\
    (forall r : preg, r <> PC -> r <> X31 -> x r = rs r))).
  econstructor 1 with (x:=rs0).
  intuition.
Qed.



Lemma transl_cbranch_correct_Aux5x:
  forall lbl k rs m' blk
         rs' insn (P: regset -> instruction -> Prop),
  (
  valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
  exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
  Next rs' m'
  /\
  rs' PC = Val.offset_ptr (rs PC) Ptrofs.one /\
  rs' MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT)
 /\ P rs' insn)
    ->
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) ::
                                (insn :: k)) rs
                      m' (insn :: k) rs' m' /\ P rs' insn.

intros.
destruct H as [H H0].
destruct H0 as [H0 H1].
destruct H1 as [H1 H2].
destruct H2 as [H2 H3].

split; auto.

econstructor.
econstructor; eauto.
Qed.



Lemma transl_cbranch_correct_Aux6x:
  forall lbl k b rs m' blk,
    (exists  (rs0: regset),
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                            (exists insn,
    exec_instr ge blk fn insn rs0 m' = eval_branch fn lbl rs0 m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs0#r = rs#r) ))
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) ::
                                insn :: k) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r).
  intros.
  destruct H as [rs0 H].
  destruct H.
  destruct H0.
  destruct H1.
  destruct H2.
  destruct H3 as [insn H3].
  destruct H3.
  econstructor 1 with (x:=rs0). econstructor 1 with (x:=insn).

  split.
  econstructor.
  econstructor; eauto.
  intuition.
Qed.


Lemma transl_cbranch_correct_Aux6b:
  forall lbl k c b rs m' blk,
    (exists  (rs0: regset),
    valid_mask_at_pc (rs PC) (rs MSK_CNT) /\
    exec_instr ge blk fn (LoadCodeMSK_name MSK_BRN (MSK_label lbl)) rs m' =
       Next rs0 m' /\
    rs0 PC = Val.offset_ptr (rs PC) Ptrofs.one /\
    rs0 MSK_CNT = next_msk_val_v (rs PC) (rs MSK_CNT) /\                        (*
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
                      m' c rs0 m' /\
*)
    (exists rs' insn,
    exec_straight_opt ge fn blk c rs0
                      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT
               -> rs'#r = rs#r) ))
    ->
    exists (rs' : regset) (insn : instruction),
    exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c) rs
      m' (insn :: k) rs' m' /\
    exec_instr ge blk fn insn rs' m' = eval_branch fn lbl rs' m' (Some b) /\
    (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT
               -> rs'#r = rs#r).
  intros.
  destruct H as [rs0 H].
  destruct H.
  destruct H0.
  destruct H1.
  destruct H2.
  destruct H3 as [rs' H3].
  destruct H3 as [insn H3].
  destruct H3.
  destruct H4.
  econstructor 1 with (x:=rs'). econstructor 1 with (x:=insn).
  eapply (transl_cbranch_correct_Aux5 lbl k c rs m' blk
                                      rs' insn
      (fun rs' insn => exec_instr ge blk fn insn rs' m' =
                   eval_branch fn lbl rs' m' (Some b) /\
      (forall r, r <> PC -> r <> X31 -> r <> MSK_CNT
               -> rs'#r = rs#r))).
  econstructor 1 with (x:=rs0).
  intuition.
Qed.

Lemma transl_cbranch_correct_true:
  forall cond args lbl k c m ms sp rs m' blk zpos,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some true ->
  agree ms sp rs ->
  Mem.extends m m' ->
  label_pos lbl 0 (fn_code fn) = Some zpos ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
  exists rs', exists insn,
      exec_straight_opt ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c)
                        rs m' (insn :: k) rs' m'
  /\ exec_instr ge blk fn insn rs' m' = goto_label fn lbl rs' m'
  /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
               -> rs'#r = rs#r.
Proof.
  intros. eapply transl_cbranch_correct_1 with (b := true); eauto.
Qed.


Lemma transl_cbranch_correct_false2:
  forall cond args lbl k c m ms sp rs m' blk,
  transl_cbranch cond args lbl k = OK c ->
  eval_condition cond (List.map ms args) m = Some false ->
  agree ms sp rs ->
  Mem.extends m m' ->
  valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
(*  label_pos lbl 0 (fn_code fn) = Some zpos -> *)
  exists rs',
    exec_straight ge fn blk (LoadCodeMSK_name MSK_BRN (MSK_label lbl) :: c)
                  rs m' k rs' m'
     /\ forall r, r <> PC -> r <> X31 -> r <> MSK_CNT -> r <> MSK_BRN
                  -> rs'#r = rs#r.
Proof.
  intros. exploit transl_cbranch_correct_2a; eauto. simpl.
  intros (rs' & insn & A & B & C).
  exists (nextinstr rs').
  split. eapply exec_straight_opt_right; eauto.
  apply exec_straight_one; auto.

  eapply msk_straight_opt_preserve; eauto.
  intros; Simpl.
Qed.



Lemma indexed_load_access_correctZ2:
  forall chunk (mk_instr1 mk_instr2: ireg -> offset -> instruction)
         rd1 rd2 m blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr1 base ofs) rs m =
      exec_load ge chunk rs m rd1 base ofs) ->
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr2 base ofs) rs m =
      exec_load ge chunk rs m rd2 base ofs) ->
  forall (base1 base2: ireg) ofs1 ofs2 k (rs1: regset) v1 v2,
  rd1 <> rd2 ->
  base1 <> X31 -> base2 <> X31 ->
  rd1 <> X31 -> rd2 <> X31 ->
  rd1 <> PC -> rd2 <> PC ->
  rd1 <> MSK_CNT -> rd2 <> MSK_CNT ->
  Mem.loadv chunk m (Val.offset_ptr rs1#base1 ofs1) = Some v1 ->
  valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT) ->
  exists (rs2: regset),
  Mem.loadv chunk m (Val.offset_ptr rs2#base2 ofs2) = Some v2 ->
   (exec_straight ge fn blk (indexed_memory_access mk_instr1 base1 ofs1
          (indexed_memory_access mk_instr2 base2 ofs2 k)) rs1 m
              (indexed_memory_access mk_instr2 base2 ofs2 k) rs2 m)
   /\ exists rs',
      exec_straight ge fn blk (indexed_memory_access mk_instr1 base1 ofs1
            (indexed_memory_access mk_instr2 base2 ofs2 k))
                                                   rs1 m k rs' m
                       /\ rs'#rd1 = v1 /\ rs'#rd2 = v2
     /\ forall r, r <> PC -> r <> X31 -> r <> rd1 -> r <> rd2 -> r <> MSK_CNT ->
                  rs'#r = rs1#r.
Proof.
  intros until blk; intros EXEC1 EXEC2.
  intros until v2. intros NOTEQ NOT31b1 NOT32b2 NOT31o1 NOT31o2
                          NOTPC1 NOTPC2 NOTMSK1 NOTMSK2 LOAD1 MM.

  exploit (indexed_load_access_correct chunk mk_instr1 rd1 m blk EXEC1); eauto.
  intros (rs1' & D1 & E1 & F1).

  econstructor. (* 1 with (x:=rs2). *)

  intro LOAD2.

  split.
  eexact D1.

  exploit (indexed_load_access_correct chunk mk_instr2 rd2 m blk EXEC2); eauto.

  eapply msk_straight_preserve; eauto.

  intros (rs2' & D2 & E2 & F2).

  econstructor.
  split.

  eapply exec_straight_trans.
  eexact D1.
  eexact D2.

  split.
  rewrite <- E1.
  eapply F2. auto. auto. auto. auto.
  split.
  auto.
  intros.
  rewrite F2.
  eapply F1.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
Qed.


Lemma indexed_load_access_correct2:
  forall chunk (mk_instr1 mk_instr2: ireg -> offset -> instruction)
         rd1 rd2 m blk,
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr1 base ofs) rs m =
      exec_load ge chunk rs m rd1 base ofs) ->
  (forall base ofs rs,
      exec_instr ge blk fn (mk_instr2 base ofs) rs m =
      exec_load ge chunk rs m rd2 base ofs) ->
  forall (base1 base2: ireg) ofs1 ofs2 k (rs1: regset) v1 v2,
  rd1 <> rd2 ->
  base1 <> X31 -> base2 <> X31 ->
  rd1 <> X31 -> rd2 <> X31 ->
  rd1 <> PC -> rd2 <> PC ->
  rd1 <> MSK_CNT -> rd2 <> MSK_CNT ->
  Mem.loadv chunk m (Val.offset_ptr rs1#base1 ofs1) = Some v1 ->
  valid_mask_at_pc (rs1 PC) (rs1 MSK_CNT) ->
  exists (rs2: regset),
  Mem.loadv chunk m (Val.offset_ptr rs2#base2 ofs2) = Some v2 ->
  exists rs',
      exec_straight ge fn blk (indexed_memory_access mk_instr1 base1 ofs1
            (indexed_memory_access mk_instr2 base2 ofs2 k))
                                                   rs1 m k rs' m
                       /\ rs'#rd1 = v1 /\ rs'#rd2 = v2
     /\ forall r, r <> PC -> r <> X31 -> r <> rd1 -> r <> rd2 -> r <> MSK_CNT ->
               rs'#r = rs1#r.
Proof.
  intros until blk; intros EXEC1 EXEC2.
  intros until v2. intros NOTEQ NOT31b1 NOT32b2 NOT31o1 NOT31o2
                          NOTPC1 NOTPC2 NOTMSK1 NOTMSK2 LOAD1 MM.

  exploit (indexed_load_access_correct chunk mk_instr1 rd1 m blk EXEC1); eauto.
  intros (rs1' & D1 & E1 & F1).

  econstructor. (* 1 with (x:=rs2). *)

  intro LOAD2.

  assert (valid_mask_at_pc (rs1' PC) (rs1' MSK_CNT)) as qq.
  eapply msk_straight_preserve; eauto.
  clear MM.

(*  split.
  eexact D1.
*)
  exploit (indexed_load_access_correct chunk mk_instr2 rd2 m blk EXEC2); eauto.
  intros (rs2' & D2 & E2 & F2).

  econstructor.
  split.

  eapply exec_straight_trans.
  eexact D1.
  eexact D2.

  split.
  rewrite <- E1.
  eapply F2. auto. auto. auto. auto.
  split.
  auto.
  intros.
  rewrite F2.
  eapply F1.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
  auto.
Qed.

End CONSTRUCTORS.
