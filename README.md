# CompCert
The formally-verified C compiler.

## Overview
The CompCert C verified compiler is a compiler for a large subset of the
C programming language that generates code for the PowerPC, ARM, x86 and
RISC-V processors.

The distinguishing feature of CompCert is that it has been formally
verified using the Coq proof assistant: the generated assembly code is
formally guaranteed to behave as prescribed by the semantics of the
source C code.

For more information on CompCert (supported platforms, supported C
features, installation instructions, using the compiler, etc), please
refer to the [Web site](https://compcert.org/) and especially
the [user's manual](https://compcert.org/man/).

## Verimag version for IntrinSec target

This is an experimental version of CompCert for the IntrinSec target.
See also explanations in our [PriSC paper](https://popl22.sigplan.org/details/prisc-2022-papers/8/A-CompCert-backend-with-symbolic-encryption). The two experimental extensions of our backend described in this paper are in branch `intrinsec_reset` and in branch `abstract_cfi`.

       
The people responsible for this version are

* Sylvain Boulmé (Grenoble-INP, Verimag)
* Paolo Torrini (Grenoble-INP, Verimag)

For inquiries on this very specific version, contact: 
 Sylvain.Boulme@univ-grenoble-alpes.fr

## Testing the IntrinSec target of CompCert

First, compile CompCert for `intrinsec`:

1. you need a GCC compiler for RISC-V 32 (e.g. `riscv32-unknown-elf-gcc` installed from `sudo apt-get -y install gcc-riscv64-linux-gnu`).
2. you need to configure CompCert compilation for `intrinsec` (e.g. use [`config_intrinsec.sh`](config_intrinsec.sh)).
3. then, run `make -j`.

Then, you may compile a C file into the IntrinSec assembly. For example, this produce a `nsieve.s` assembly file:

   ```./ccomp -S test/c/nsieve.c -o nsieve.s```

In order to run it, you need an IntrinSec assembler and an IntrinSec emulator, which we are not authorized to provide. If you are interested in, please directly contact Olivier Savry (`olivier.savry@cea.fr`).

## License
CompCert is not free software.  This non-commercial release can only
be used for evaluation, research, educational and personal purposes.
A commercial version of CompCert, without this restriction and with
professional support and extra features, can be purchased from
[AbsInt](https://www.absint.com).  See the file `LICENSE` for more
information.

## Copyright
The CompCert verified compiler is Copyright Institut National de
Recherche en Informatique et en Automatique (INRIA) and 
AbsInt Angewandte Informatik GmbH.


## Contact
General discussions on CompCert take place on the
[compcert-users@inria.fr](https://sympa.inria.fr/sympa/info/compcert-users)
mailing list.

For inquiries on the commercial version of CompCert, please contact
info@absint.com
