(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*           Paolo Torrini      Grenoble-INP, VERIMAG                  *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Correctness proof for Asm generation: machine-independent framework *)

Require Import Coqlib.
Require Intv.
Require Import AST.
Require Import Errors.
Require Import Integers.
Require Import Floats.
Require Import Values.
Require Import Memory.
Require Import Globalenvs.
Require Import Events.
Require Import Smallstep.
Require Import Locations.
Require Import Mach.
Require Import Asm.
Require Import Asmgen.
Require Import Conventions.
Require Import Znat.
Require Import Zpow_facts.
Require Import Lia.

(** * Processor registers and register states *)

Hint Extern 2 (_ <> _) => congruence: asmgen.

Lemma ireg_of_eq:
  forall r r', ireg_of r = OK r' -> preg_of r = IR r'.
Proof.
  unfold ireg_of; intros. destruct (preg_of r); inv H; auto.
Qed.

Lemma freg_of_eq:
  forall r r', freg_of r = OK r' -> preg_of r = FR r'.
Proof.
  unfold freg_of; intros. destruct (preg_of r); inv H; auto.
Qed.

Lemma preg_of_injective:
  forall r1 r2, preg_of r1 = preg_of r2 -> r1 = r2.
Proof.
  destruct r1; destruct r2; simpl; intros; reflexivity || discriminate.
Qed.

Lemma preg_of_data:
  forall r, data_preg (preg_of r) = true.
Proof.
  intros. destruct r; reflexivity.
Qed.
Hint Resolve preg_of_data: asmgen.

Lemma data_diff:
  forall r r',
  data_preg r = true -> data_preg r' = false -> r <> r'.
Proof.
  congruence.
Qed.
Hint Resolve data_diff: asmgen.

Lemma preg_of_not_SP:
  forall r, preg_of r <> SP.
Proof.
  intros. unfold preg_of; destruct r; simpl; congruence.
Qed.

Lemma preg_of_not_PC:
  forall r, preg_of r <> PC.
Proof.
  intros. apply data_diff; auto with asmgen.
Qed.

Hint Resolve preg_of_not_SP preg_of_not_PC: asmgen.

Lemma nextinstr_pc:
  forall rs, (nextinstr rs)#PC = Val.offset_ptr rs#PC Ptrofs.one.
Proof.
  intros.
  unfold nextinstr.
  eapply Pregmap.gso.
  intuition.
  inversion H.
Qed.

Lemma nextinstr_msk_cnt:
  forall rs, (nextinstr rs)#MSK_CNT = (next_msk_val_v (rs PC) (rs MSK_CNT)).
Proof.
  intros.
  eapply Pregmap.gss.
Qed.

Lemma nextinstr_inv:
  forall r rs, r <> PC /\ r <> MSK_CNT -> (nextinstr rs)#r = rs#r.
Proof.
  intros. destruct H.
  unfold nextinstr.
  rewrite Pregmap.gso. eapply Pregmap.gso.
  auto. auto.
Qed.

Lemma nextinstr_inv1:
  forall r rs, data_preg r = true -> (nextinstr rs)#r = rs#r.
Proof.
  intros. apply nextinstr_inv.
  unfold data_preg in H.
  split.
  destruct r.
  discriminate.
  discriminate.
  discriminate.
  discriminate.
  destruct r.
  discriminate.
  discriminate.
  discriminate.
  discriminate.
Qed.

Lemma nextinstr_set_preg:
  forall rs m v,
  (nextinstr (rs#(preg_of m) <- v))#PC = Val.offset_ptr rs#PC Ptrofs.one.
Proof.
  intros. unfold nextinstr. rewrite Pregmap.gso. rewrite Pregmap.gss.
  rewrite Pregmap.gso. auto. apply not_eq_sym. apply preg_of_not_PC.
  discriminate.
Qed.

Lemma undef_regs_other:
  forall r rl rs,
  (forall r', In r' rl -> r <> r') ->
  undef_regs rl rs r = rs r.
Proof.
  induction rl; simpl; intros. auto.
  rewrite IHrl by auto. rewrite Pregmap.gso; auto.
Qed.

Fixpoint preg_notin (r: preg) (rl: list mreg) : Prop :=
  match rl with
  | nil => True
  | r1 :: nil => r <> preg_of r1
  | r1 :: rl => r <> preg_of r1 /\ preg_notin r rl
  end.

Remark preg_notin_charact:
  forall r rl,
  preg_notin r rl <-> (forall mr, In mr rl -> r <> preg_of mr).
Proof.
  induction rl; simpl; intros.
  tauto.
  destruct rl.
  simpl. split. intros. intuition congruence. auto.
  rewrite IHrl. split.
  intros [A B]. intros. destruct H. congruence. auto.
  auto.
Qed.

Lemma undef_regs_other_2:
  forall r rl rs,
  preg_notin r rl ->
  undef_regs (map preg_of rl) rs r = rs r.
Proof.
  intros. apply undef_regs_other. intros.
  exploit list_in_map_inv; eauto. intros [mr [A B]]. subst.
  rewrite preg_notin_charact in H. auto.
Qed.

(** * Agreement between Mach registers and processor registers *)

Record agree (ms: Mach.regset) (sp: val) (rs: Asm.regset) : Prop := mkagree {
  agree_sp: rs#SP = sp;
  agree_sp_def: sp <> Vundef;
  agree_mregs: forall r: mreg, Val.lessdef (ms r) (rs#(preg_of r))
}.

Lemma preg_val:
  forall ms sp rs r, agree ms sp rs -> Val.lessdef (ms r) rs#(preg_of r).
Proof.
  intros. destruct H. auto.
Qed.

Lemma preg_vals:
  forall ms sp rs, agree ms sp rs ->
  forall l, Val.lessdef_list (map ms l) (map rs (map preg_of l)).
Proof.
  induction l; simpl. constructor. constructor. eapply preg_val; eauto. auto.
Qed.

Lemma sp_val:
  forall ms sp rs, agree ms sp rs -> sp = rs#SP.
Proof.
  intros. destruct H; auto.
Qed.

Lemma ireg_val:
  forall ms sp rs r r',
  agree ms sp rs ->
  ireg_of r = OK r' ->
  Val.lessdef (ms r) rs#r'.
Proof.
  intros. rewrite <- (ireg_of_eq _ _ H0). eapply preg_val; eauto.
Qed.

Lemma freg_val:
  forall ms sp rs r r',
  agree ms sp rs ->
  freg_of r = OK r' ->
  Val.lessdef (ms r) (rs#r').
Proof.
  intros. rewrite <- (freg_of_eq _ _ H0). eapply preg_val; eauto.
Qed.

Lemma agree_exten:
  forall ms sp rs rs',
  agree ms sp rs ->
  (forall r, data_preg r = true -> rs'#r = rs#r) ->
  agree ms sp rs'.
Proof.
  intros. destruct H. split; auto.
  rewrite H0; auto. auto.
  intros. rewrite H0; auto. apply preg_of_data.
Qed.

(** Preservation of register agreement under various assignments. *)

Lemma agree_set_mreg:
  forall ms sp rs r v rs',
  agree ms sp rs ->
  Val.lessdef v (rs'#(preg_of r)) ->
  (forall r', data_preg r' = true -> r' <> preg_of r -> rs'#r' = rs#r') ->
  agree (Regmap.set r v ms) sp rs'.
Proof.
  intros. destruct H. split; auto.
  rewrite H1; auto. apply not_eq_sym. apply preg_of_not_SP.
  intros. unfold Regmap.set. destruct (RegEq.eq r0 r). congruence.
  rewrite H1. auto. apply preg_of_data.
  red; intros; elim n. eapply preg_of_injective; eauto.
Qed.

Corollary agree_set_mreg_parallel:
  forall ms sp rs r v v',
  agree ms sp rs ->
  Val.lessdef v v' ->
  agree (Regmap.set r v ms) sp (Pregmap.set (preg_of r) v' rs).
Proof.
  intros. eapply agree_set_mreg; eauto. rewrite Pregmap.gss; auto.
  intros; apply Pregmap.gso; auto.
Qed.

Lemma agree_set_other:
  forall ms sp rs r v,
  agree ms sp rs ->
  data_preg r = false ->
  agree ms sp (rs#r <- v).
Proof.
  intros. apply agree_exten with rs. auto.
  intros. apply Pregmap.gso. congruence.
Qed.

(* REV P *)
Lemma agree_nextinstr:
  forall ms sp rs,
  agree ms sp rs -> agree ms sp (nextinstr rs).
Proof.
  intros. unfold nextinstr.
  apply agree_set_other. apply agree_set_other. auto. auto. auto.
Qed.

Lemma agree_set_pair:
  forall sp p v v' ms rs,
  agree ms sp rs ->
  Val.lessdef v v' ->
  agree (Mach.set_pair p v ms) sp (set_pair (map_rpair preg_of p) v' rs).
Proof.
  intros. destruct p; simpl.
- apply agree_set_mreg_parallel; auto.
- apply agree_set_mreg_parallel. apply agree_set_mreg_parallel; auto.
  apply Val.hiword_lessdef; auto. apply Val.loword_lessdef; auto.
Qed.

Lemma agree_undef_nondata_regs:
  forall ms sp rl rs,
  agree ms sp rs ->
  (forall r, In r rl -> data_preg r = false) ->
  agree ms sp (undef_regs rl rs).
Proof.
  induction rl; simpl; intros. auto.
  apply IHrl. apply agree_exten with rs; auto.
  intros. apply Pregmap.gso. red; intros; subst.
  assert (data_preg a = false) by auto. congruence.
  intros. apply H0; auto.
Qed.

Lemma agree_undef_regs:
  forall ms sp rl rs rs',
  agree ms sp rs ->
  (forall r', data_preg r' = true -> preg_notin r' rl -> rs'#r' = rs#r') ->
  agree (Mach.undef_regs rl ms) sp rs'.
Proof.
  intros. destruct H. split; auto.
  rewrite <- agree_sp0. apply H0; auto.
  rewrite preg_notin_charact. intros. apply not_eq_sym. apply preg_of_not_SP.
  intros. destruct (In_dec mreg_eq r rl).
  rewrite Mach.undef_regs_same; auto.
  rewrite Mach.undef_regs_other; auto. rewrite H0; auto.
  apply preg_of_data.
  rewrite preg_notin_charact. intros; red; intros. elim n.
  exploit preg_of_injective; eauto. congruence.
Qed.

Lemma agree_undef_regs2:
  forall ms sp rl rs rs',
  agree (Mach.undef_regs rl ms) sp rs ->
  (forall r', data_preg r' = true -> preg_notin r' rl -> rs'#r' = rs#r') ->
  agree (Mach.undef_regs rl ms) sp rs'.
Proof.
  intros. destruct H. split; auto.
  rewrite <- agree_sp0. apply H0; auto.
  rewrite preg_notin_charact. intros. apply not_eq_sym. apply preg_of_not_SP.
  intros. destruct (In_dec mreg_eq r rl).
  rewrite Mach.undef_regs_same; auto.
  rewrite H0; auto.
  apply preg_of_data.
  rewrite preg_notin_charact. intros; red; intros. elim n.
  exploit preg_of_injective; eauto. congruence.
Qed.

Lemma agree_set_undef_mreg:
  forall ms sp rs r v rl rs',
  agree ms sp rs ->
  Val.lessdef v (rs'#(preg_of r)) ->
  (forall r', data_preg r' = true -> r' <> preg_of r ->
              preg_notin r' rl -> rs'#r' = rs#r') ->
  agree (Regmap.set r v (Mach.undef_regs rl ms)) sp rs'.
Proof.
  intros.
  apply agree_set_mreg with (rs'#(preg_of r) <- (rs#(preg_of r))); auto.
  apply agree_undef_regs with rs; auto.
  intros. unfold Pregmap.set. destruct (PregEq.eq r' (preg_of r)).
  congruence. auto.
  intros. rewrite Pregmap.gso; auto.
Qed.

Lemma agree_undef_caller_save_regs:
  forall ms sp rs,
  agree ms sp rs ->
  agree (Mach.undef_caller_save_regs ms) sp (Asm.undef_caller_save_regs rs).
Proof.
  intros. destruct H.
  unfold Mach.undef_caller_save_regs, Asm.undef_caller_save_regs; split.
- unfold proj_sumbool; rewrite dec_eq_true. auto.
- auto.
- intros. unfold proj_sumbool. rewrite dec_eq_false by (apply preg_of_not_SP).
  destruct (in_dec preg_eq (preg_of r)
           (List.map preg_of (List.filter is_callee_save all_mregs))); simpl.
+ apply list_in_map_inv in i. destruct i as (mr & A & B).
  assert (r = mr) by (apply preg_of_injective; auto). subst mr; clear A.
  apply List.filter_In in B. destruct B as [C D]. rewrite D. auto.
+ destruct (is_callee_save r) eqn:CS; auto.
  elim n. apply List.in_map. apply List.filter_In.
  auto using all_mregs_complete.
Qed.

Lemma agree_change_sp:
  forall ms sp rs sp',
  agree ms sp rs -> sp' <> Vundef ->
  agree ms sp' (rs#SP <- sp').
Proof.
  intros. inv H. split; auto.
  intros. rewrite Pregmap.gso; auto with asmgen.
Qed.

(** Connection between Mach and Asm calling conventions for external
    functions. *)

Lemma extcall_arg_match:
  forall ms sp rs m m' l v,
  agree ms sp rs ->
  Mem.extends m m' ->
  Mach.extcall_arg ms m sp l v ->
  exists v', Asm.extcall_arg rs m' l v' /\ Val.lessdef v v'.
Proof.
  intros. inv H1.
  exists (rs#(preg_of r)); split. constructor. eapply preg_val; eauto.
  unfold load_stack in H2.
  exploit Mem.loadv_extends; eauto. intros [v' [A B]].
  rewrite (sp_val _ _ _ H) in A.
  exists v'; split; auto.
  econstructor. eauto. assumption.
Qed.

Lemma extcall_arg_pair_match:
  forall ms sp rs m m' p v,
  agree ms sp rs ->
  Mem.extends m m' ->
  Mach.extcall_arg_pair ms m sp p v ->
  exists v', Asm.extcall_arg_pair rs m' p v' /\ Val.lessdef v v'.
Proof.
  intros. inv H1.
- exploit extcall_arg_match; eauto. intros (v' & A & B).
    exists v'; split; auto. constructor; auto.
- exploit extcall_arg_match. eauto. eauto. eexact H2. intros (v1 & A1 & B1).
  exploit extcall_arg_match. eauto. eauto. eexact H3. intros (v2 & A2 & B2).
  exists (Val.longofwords v1 v2); split. constructor; auto.
  apply Val.longofwords_lessdef; auto.
Qed.

Lemma extcall_args_match:
  forall ms sp rs m m', agree ms sp rs -> Mem.extends m m' ->
  forall ll vl,
  list_forall2 (Mach.extcall_arg_pair ms m sp) ll vl ->
  exists vl', list_forall2 (Asm.extcall_arg_pair rs m') ll vl' /\
              Val.lessdef_list vl vl'.
Proof.
  induction 3; intros.
  exists (@nil val); split. constructor. constructor.
  exploit extcall_arg_pair_match; eauto. intros [v1' [A B]].
  destruct IHlist_forall2 as [vl' [C D]].
  exists (v1' :: vl'); split; constructor; auto.
Qed.

Lemma extcall_arguments_match:
  forall ms m m' sp rs sg args,
  agree ms sp rs -> Mem.extends m m' ->
  Mach.extcall_arguments ms m sp sg args ->
  exists args', Asm.extcall_arguments rs m' sg args' /\
                Val.lessdef_list args args'.
Proof.
  unfold Mach.extcall_arguments, Asm.extcall_arguments; intros.
  eapply extcall_args_match; eauto.
Qed.

(** Translation of arguments and results to builtins. *)

Remark builtin_arg_match:
  forall ge (rs: regset) sp m a v,
  eval_builtin_arg ge (fun r => rs (preg_of r)) sp m a v ->
  eval_builtin_arg ge rs sp m (map_builtin_arg preg_of a) v.
Proof.
  induction 1; simpl; eauto with barg.
Qed.

Lemma builtin_args_match:
  forall ge ms sp rs m m', agree ms sp rs -> Mem.extends m m' ->
  forall al vl, eval_builtin_args ge ms sp m al vl ->
  exists vl', eval_builtin_args ge rs sp m'
                (map (map_builtin_arg preg_of) al) vl'
           /\ Val.lessdef_list vl vl'.
Proof.
  induction 3; intros; simpl.
  exists (@nil val); split; constructor.
  exploit (@eval_builtin_arg_lessdef _ ge ms (fun r => rs (preg_of r))); eauto.
  intros; eapply preg_val; eauto.
  intros (v1' & A & B).
  destruct IHlist_forall2 as [vl' [C D]].
  exists (v1' :: vl'); split; constructor; auto. apply builtin_arg_match; auto.
Qed.

Lemma agree_set_res:
  forall res ms sp rs v v',
  agree ms sp rs ->
  Val.lessdef v v' ->
  agree (Mach.set_res res v ms) sp
        (Asm.set_res (map_builtin_res preg_of res) v' rs).
Proof.
  induction res; simpl; intros.
- eapply agree_set_mreg; eauto. rewrite Pregmap.gss. auto.
  intros. apply Pregmap.gso; auto.
- auto.
- apply IHres2. apply IHres1. auto.
  apply Val.hiword_lessdef; auto.
  apply Val.loword_lessdef; auto.
Qed.

Lemma set_res_other:
  forall r res v rs,
  data_preg r = false ->
  set_res (map_builtin_res preg_of res) v rs r = rs r.
Proof.
  induction res; simpl; intros.
- apply Pregmap.gso. red; intros; subst r.
  rewrite preg_of_data in H; discriminate.
- auto.
- rewrite IHres2, IHres1; auto.
Qed.

(** * Correspondence between Mach code and Asm code *)

Lemma find_instr_in:
  forall c pos i,
  find_instr pos c = Some i -> In i c.
Proof.
  induction c; simpl. intros; discriminate.
  intros until i. case (zeq pos 0); intros.
  left; congruence. right; eauto.
Qed.

(** The ``code tail'' of an instruction list [c] is the list of instructions
  starting at PC [pos]. *)

Inductive code_tail: Z -> code -> code -> Prop :=
  | code_tail_0: forall c,
      code_tail 0 c c
  | code_tail_S: forall pos i c1 c2,
      code_tail pos c1 c2 ->
      code_tail (pos + 1) (i :: c1) c2.

Lemma code_tail_pos:
  forall pos c1 c2, code_tail pos c1 c2 -> pos >= 0.
Proof.
  induction 1. omega. omega.
Qed.

Lemma find_instr_tail:
  forall c1 i c2 pos,
  code_tail pos c1 (i :: c2) ->
  find_instr pos c1 = Some i.
Proof.
  induction c1; simpl; intros.
  inv H.
  destruct (zeq pos 0). subst pos.
  inv H. auto. generalize (code_tail_pos _ _ _ H4). intro.
  omegaContradiction.
  inv H. congruence. replace (pos0 + 1 - 1) with pos0 by omega.
  eauto.
Qed.

Remark code_tail_bounds_1:
  forall fn ofs c,
  code_tail ofs fn c -> 0 <= ofs <= list_length_z fn.
Proof.
  induction 1; intros; simpl.
  generalize (list_length_z_pos c). omega.
  rewrite list_length_z_cons. omega.
Qed.

Remark code_tail_bounds_2:
  forall fn ofs i c,
  code_tail ofs fn (i :: c) -> 0 <= ofs < list_length_z fn.
Proof.
  assert (forall ofs fn c, code_tail ofs fn c ->
          forall i c', c = i :: c' -> 0 <= ofs < list_length_z fn).
  induction 1; intros; simpl.
  rewrite H. rewrite list_length_z_cons.
  generalize (list_length_z_pos c'). omega.
  rewrite list_length_z_cons. generalize (IHcode_tail _ _ H0). omega.
  eauto.
Qed.

Lemma code_tail_next:
  forall fn ofs i c,
  code_tail ofs fn (i :: c) ->
  code_tail (ofs + 1) fn c.
Proof.
  assert (forall ofs fn c, code_tail ofs fn c ->
          forall i c', c = i :: c' -> code_tail (ofs + 1) fn c').
  induction 1; intros.
  subst c. constructor. constructor.
  constructor. eauto.
  eauto.
Qed.

Lemma code_tail_next_int:
  forall fn ofs i c,
  list_length_z fn <= Ptrofs.max_unsigned ->
  code_tail (Ptrofs.unsigned ofs) fn (i :: c) ->
  code_tail (Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one)) fn c.
Proof.
  intros. rewrite Ptrofs.add_unsigned, Ptrofs.unsigned_one.
  rewrite Ptrofs.unsigned_repr. apply code_tail_next with i; auto.
  generalize (code_tail_bounds_2 _ _ _ _ H0). omega.
Qed.

(** [transl_code_at_pc pc fb f c ep tf tc] holds if the code pointer [pc] points
  within the Asm code generated by translating Mach function [f],
  and [tc] is the tail of the generated code at the position corresponding
  to the code pointer [pc]. *)

Inductive transl_code_at_pc (ge: Mach.genv):
  val -> block -> Mach.function -> Mach.code -> bool ->
  Asm.function -> Asm.code -> Prop :=
  transl_code_at_pc_intro:
    forall b ofs f c ep tf tc,
    Genv.find_funct_ptr ge b = Some(Internal f) ->
    transf_function f = Errors.OK tf ->
    transl_code f c ep = OK tc ->
    code_tail (Ptrofs.unsigned ofs) (fn_code tf) tc ->
    transl_code_at_pc ge (Vptr b ofs) b f c ep tf tc.

(** Equivalence between [transl_code] and [transl_code']. *)

Local Open Scope error_monad_scope.

Lemma transl_code_rec_transl_code:
  forall f il ep k,
  transl_code_rec f il ep k = (do c <- transl_code f il ep; k c).
Proof.
  induction il; simpl; intros.
  auto.
  rewrite IHil.
  destruct (transl_code f il (it1_is_parent ep a)); simpl; auto.
Qed.

Lemma transl_code'_transl_code:
  forall f il ep,
  transl_code' f il ep = transl_code f il ep.
Proof.
  intros. unfold transl_code'. rewrite transl_code_rec_transl_code.
  destruct (transl_code f il ep); auto.
Qed.

(** Predictor for return addresses in generated Asm code.

  The [return_address_offset] predicate defined here is used in the
  semantics for Mach to determine the return addresses that are
  stored in activation records. *)

(** Consider a Mach function [f] and a sequence [c] of Mach instructions
  representing the Mach code that remains to be executed after a
  function call returns.  The predicate [return_address_offset f c ofs]
  holds if [ofs] is the integer offset of the PPC instruction
  following the call in the Asm code obtained by translating the
  code of [f]. Graphically:
<<
     Mach function f    |--------- Mcall ---------|
         Mach code c    |                |--------|
                        |                 \        \
                        |                  \        \
                        |                   \        \
     Asm code           |                    |--------|
     Asm function       |------------- Pcall ---------|

                        <-------- ofs ------->
>>
*)

Definition return_address_offset (f: Mach.function) (c: Mach.code)
           (ofs: ptrofs) : Prop :=
  forall tf tc,
  transf_function f = OK tf ->
  transl_code f c false = OK tc ->
  code_tail (Ptrofs.unsigned ofs) (fn_code tf) tc.

Definition return_mask (fb: block) (ofs: ptrofs) (msk: val) : Prop :=
  msk = Vint (encrypt_msk (Ptrofs.unsigned ofs) fb).


(** We now show that such an offset always exists if the Mach code [c]
  is a suffix of [f.(fn_code)].  This holds because the translation
  from Mach to PPC is compositional: each Mach instruction becomes
  zero, one or several PPC instructions, but the order of instructions
  is preserved. *)

Lemma is_tail_code_tail:
  forall c1 c2, is_tail c1 c2 -> exists ofs, code_tail ofs c2 c1.
Proof.
  induction 1. exists 0; constructor.
  destruct IHis_tail as [ofs CT]. exists (ofs + 1); constructor; auto.
Qed.

Section RETADDR_EXISTS.

Hypothesis transl_instr_tail:
  forall f i ep k c, transl_instr f i ep k = OK c -> is_tail k c.
Hypothesis transf_function_inv:
  forall f tf, transf_function f = OK tf ->
               exists tc, exists ep, transl_code f (Mach.fn_code f) ep =
                                     OK tc /\ is_tail tc (fn_code tf).
Hypothesis transf_function_len:
  forall f tf, transf_function f =
               OK tf -> list_length_z (fn_code tf) <= Ptrofs.max_unsigned.

Lemma transl_code_tail:
  forall f c1 c2, is_tail c1 c2 ->
  forall tc2 ep2, transl_code f c2 ep2 = OK tc2 ->
  exists tc1, exists ep1, transl_code f c1 ep1 = OK tc1 /\ is_tail tc1 tc2.
Proof.
  induction 1; simpl; intros.
  exists tc2; exists ep2; split; auto with coqlib.
  monadInv H0. exploit IHis_tail; eauto. intros [tc1 [ep1 [A B]]].
  exists tc1; exists ep1; split. auto.
  apply is_tail_trans with x. auto. eapply transl_instr_tail; eauto.
Qed.

Lemma return_address_exists:
  forall f sg ros c, is_tail (Mcall sg ros :: c) f.(Mach.fn_code) ->
  exists ra, return_address_offset f c ra.
Proof.
  intros. destruct (transf_function f) as [tf|] eqn:TF.
+ exploit transf_function_inv; eauto. intros (tc1 & ep1 & TR1 & TL1).
  exploit transl_code_tail; eauto. intros (tc2 & ep2 & TR2 & TL2).
Opaque transl_instr.
  monadInv TR2.
  assert (TL3: is_tail x (fn_code tf)).
  { apply is_tail_trans with tc1; auto.
    apply is_tail_trans with tc2; auto.
    eapply transl_instr_tail; eauto. }
  exploit is_tail_code_tail. eexact TL3. intros [ofs CT].
  exists (Ptrofs.repr ofs). red; intros.
  rewrite Ptrofs.unsigned_repr. congruence.
  exploit code_tail_bounds_1; eauto.
  apply transf_function_len in TF. omega.
+ exists Ptrofs.zero; red; intros. congruence.
Qed.

End RETADDR_EXISTS.

Remark code_tail_no_bigger:
  forall pos c1 c2, code_tail pos c1 c2 -> (length c2 <= length c1)%nat.
Proof.
  induction 1; simpl; omega.
Qed.

Remark code_tail_unique:
  forall fn c pos pos',
  code_tail pos fn c -> code_tail pos' fn c -> pos = pos'.
Proof.
  induction fn; intros until pos'; intros ITA CT; inv ITA; inv CT; auto.
  generalize (code_tail_no_bigger _ _ _ H3); simpl; intro; omega.
  generalize (code_tail_no_bigger _ _ _ H3); simpl; intro; omega.
  f_equal. eauto.
Qed.

Lemma return_address_offset_correct:
  forall ge b ofs fb f c tf tc ofs',
  transl_code_at_pc ge (Vptr b ofs) fb f c false tf tc ->
  return_address_offset f c ofs' ->
  ofs' = ofs.
Proof.
  intros. inv H. red in H0.
  exploit code_tail_unique. eexact H12. eapply H0; eauto. intro.
  rewrite <- (Ptrofs.repr_unsigned ofs).
  rewrite <- (Ptrofs.repr_unsigned ofs').
  congruence.
Qed.

(** The [find_label] function returns the code tail starting at the
  given label.  A connection with [code_tail] is then established. *)

Fixpoint find_label (lbl: label) (c: code) {struct c} : option code :=
  match c with
  | nil => None
  | instr :: c' =>
      if is_label lbl instr then Some c' else find_label lbl c'
  end.

Lemma label_pos_code_tail:
  forall lbl c pos c',
  find_label lbl c = Some c' ->
  exists pos',
  label_pos lbl pos c = Some pos'
  /\ code_tail (pos' - pos) c c'
  /\ pos < pos' <= pos + list_length_z c.
Proof.
  induction c.
  simpl; intros. discriminate.
  simpl; intros until c'.
  case (is_label lbl a).
  intro EQ; injection EQ; intro; subst c'.
  exists (pos + 1). split. auto. split.
  replace (pos + 1 - pos) with (0 + 1) by omega. constructor. constructor.
  rewrite list_length_z_cons. generalize (list_length_z_pos c). omega.
  intros. generalize (IHc (pos + 1) c' H). intros [pos' [A [B C]]].
  exists pos'. split. auto. split.
  replace (pos' - pos) with ((pos' - (pos + 1)) + 1) by omega.
  constructor. auto.
  rewrite list_length_z_cons. omega.
Qed.

(** Helper lemmas to reason about
- the "code is tail of" property
- correct translation of labels. *)

Definition tail_nolabel (k c: code) : Prop :=
  is_tail k c /\ forall lbl, find_label lbl c = find_label lbl k.

Lemma tail_nolabel_refl:
  forall c, tail_nolabel c c.
Proof.
  intros; split. apply is_tail_refl. auto.
Qed.

Lemma tail_nolabel_trans:
  forall c1 c2 c3, tail_nolabel c2 c3 -> tail_nolabel c1 c2 ->
                   tail_nolabel c1 c3.
Proof.
  intros. destruct H; destruct H0; split.
  eapply is_tail_trans; eauto.
  intros. rewrite H1; auto.
Qed.

Definition nolabel (i: instruction) :=
  match i with Plabel _ => False | _ => True end.

Hint Extern 1 (nolabel _) => exact I : labels.

Lemma tail_nolabel_cons:
  forall i c k,
  nolabel i -> tail_nolabel k c -> tail_nolabel k (i :: c).
Proof.
  intros. destruct H0. split.
  constructor; auto.
  intros. simpl. rewrite <- H1. destruct i; reflexivity || contradiction.
Qed.

Hint Resolve tail_nolabel_refl: labels.

Remark tail_nolabel_find_label:
  forall lbl k c, tail_nolabel k c -> find_label lbl c = find_label lbl k.
Proof.
  intros. destruct H. auto.
Qed.

Remark tail_nolabel_is_tail:
  forall k c, tail_nolabel k c -> is_tail k c.
Proof.
  intros. destruct H. auto.
Qed.

(************************* LTAC ********************************************)

Ltac TailNoLabel :=
  eauto with labels;
  match goal with
  | [ |- tail_nolabel _ (_ :: _) ] =>
    apply tail_nolabel_cons; [auto; exact I | TailNoLabel]
  | [ H: Error _ = OK _ |- _ ] => discriminate
  | [ H: assertion_failed = OK _ |- _ ] => discriminate
  | [ H: OK _ = OK _ |- _ ] => inv H; TailNoLabel
  | [ H: bind _ _ = OK _ |- _ ] => monadInv H;  TailNoLabel
  | [ H: (if ?x then _ else _) = OK _ |- _ ] => destruct x; TailNoLabel
  | [ H: match ?x with nil => _ | _ :: _ => _ end = OK _ |- _ ] =>
    destruct x; TailNoLabel
  | _ => idtac
  end.

Ltac Simplif :=
  ((rewrite nextinstr_inv by eauto with asmgen)
  || (rewrite nextinstr_inv1 by eauto with asmgen)
  || (rewrite Pregmap.gss)
  || (rewrite nextinstr_pc)
  || (rewrite Pregmap.gso by eauto with asmgen)); auto with asmgen.

Ltac Simpl := repeat Simplif.

Ltac ArgsInv :=
  repeat (match goal with
  | [ H: Error _ = OK _ |- _ ] => discriminate
  | [ H: match ?args with nil => _ | _ :: _ => _ end = OK _ |- _ ] =>
    destruct args
  | [ H: bind _ _ = OK _ |- _ ] => monadInv H
  | [ H: match _ with left _ => _ | right _ => assertion_failed end = OK _ |- _ ] => monadInv H; ArgsInv
  | [ H: match _ with true => _ |
                 false => assertion_failed end = OK _ |- _ ] =>
    monadInv H; ArgsInv
  end);
  subst;
  repeat (match goal with
  | [ H: ireg_of _ = OK _ |- _ ] => simpl in *; rewrite (ireg_of_eq _ _ H) in *
  | [ H: freg_of _ = OK _ |- _ ] => simpl in *; rewrite (freg_of_eq _ _ H) in *
  end).

Ltac SimplEval H :=
  match type of H with
  | Some _ = None _ => discriminate
  | Some _ = Some _ => inv H
  | ?a = Some ?b => let A := fresh in assert (A: Val.maketotal a = b)
                      by (rewrite H; reflexivity)
end.



(** * Execution of straight-line code *)

Section STRAIGHTLINE.

Variable ge: genv.
Variable fn: function.

(** Straight-line code is composed of processor instructions that execute
  in sequence (no branches, no function calls and returns).
  The following inductive predicate relates the machine states
  before and after executing a straight-line sequence of instructions.
  Instructions are taken from the first list instead of being fetched
  from memory. *)

Inductive exec_straight: block -> code -> regset -> mem ->
                         code -> regset -> mem -> Prop :=
  | exec_straight_one:
      forall blk i1 c rs1 m1 rs2 m2,
      valid_mask_at_pc (rs1#PC) (rs1#MSK_CNT) ->
      exec_instr ge blk fn i1 rs1 m1 = Next rs2 m2 ->
      rs2#PC = Val.offset_ptr rs1#PC Ptrofs.one ->
      rs2#MSK_CNT = (next_msk_val_v rs1#PC rs1#MSK_CNT) ->
      exec_straight blk (i1 :: c) rs1 m1 c rs2 m2
  | exec_straight_step:
      forall blk i c rs1 m1 rs2 m2 c' rs3 m3,
      valid_mask_at_pc (rs1#PC) (rs1#MSK_CNT) ->
      exec_instr ge blk fn i rs1 m1 = Next rs2 m2 ->
      rs2#PC = Val.offset_ptr rs1#PC Ptrofs.one ->
      rs2#MSK_CNT = (next_msk_val_v rs1#PC rs1#MSK_CNT) ->
      exec_straight blk c rs2 m2 c' rs3 m3 ->
      exec_straight blk (i :: c) rs1 m1 c' rs3 m3.


(* REV S *)
Lemma exec_straight_trans:
  forall blk c1 rs1 m1 c2 rs2 m2 c3 rs3 m3,
  exec_straight blk c1 rs1 m1 c2 rs2 m2 ->
  exec_straight blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight blk c1 rs1 m1 c3 rs3 m3.
Proof.
  induction 1; intros.
  apply exec_straight_step with rs2 m2; auto.
  apply exec_straight_step with rs2 m2; auto.
Qed.

Lemma exec_straight_two:
  forall blk i1 i2 c rs1 m1 rs2 m2 rs3 m3,
  valid_mask_at_pc (rs1#PC) (rs1#MSK_CNT) ->
  valid_mask_at_pc (rs2#PC) (rs2#MSK_CNT) ->
  exec_instr ge blk fn i1 rs1 m1 = Next rs2 m2 ->
  exec_instr ge blk fn i2 rs2 m2 = Next rs3 m3 ->
  rs2#PC = Val.offset_ptr rs1#PC Ptrofs.one ->
  rs3#PC = Val.offset_ptr rs2#PC Ptrofs.one ->
  rs2#MSK_CNT = (next_msk_val_v rs1#PC rs1#MSK_CNT) ->
  rs3#MSK_CNT = (next_msk_val_v rs2#PC rs2#MSK_CNT) ->
  exec_straight blk (i1 :: i2 :: c) rs1 m1 c rs3 m3.
Proof.
  intros. apply exec_straight_step with rs2 m2; auto.
  apply exec_straight_one; auto.
Qed.

Lemma exec_straight_three:
  forall blk i1 i2 i3 c rs1 m1 rs2 m2 rs3 m3 rs4 m4,
  valid_mask_at_pc (rs1#PC) (rs1#MSK_CNT) ->
  valid_mask_at_pc (rs2#PC) (rs2#MSK_CNT) ->
  valid_mask_at_pc (rs3#PC) (rs3#MSK_CNT) ->
  exec_instr ge blk fn i1 rs1 m1 = Next rs2 m2 ->
  exec_instr ge blk fn i2 rs2 m2 = Next rs3 m3 ->
  exec_instr ge blk fn i3 rs3 m3 = Next rs4 m4 ->
  rs2#PC = Val.offset_ptr rs1#PC Ptrofs.one ->
  rs3#PC = Val.offset_ptr rs2#PC Ptrofs.one ->
  rs4#PC = Val.offset_ptr rs3#PC Ptrofs.one ->
  rs2#MSK_CNT = (next_msk_val_v rs1#PC rs1#MSK_CNT) ->
  rs3#MSK_CNT = (next_msk_val_v rs2#PC rs2#MSK_CNT) ->
  rs4#MSK_CNT = (next_msk_val_v rs3#PC rs3#MSK_CNT) ->
  exec_straight blk (i1 :: i2 :: i3 :: c) rs1 m1 c rs4 m4.
Proof.
  intros. apply exec_straight_step with rs2 m2; auto.
  eapply exec_straight_two.
  exact H0. exact H1. eauto. auto. auto. auto. auto. auto.
Qed.

(** The following lemmas show that straight-line executions
  (predicate [exec_straight]) correspond to correct Asm executions. *)

Inductive exec_straight_opt: block -> code -> regset -> mem -> code -> regset -> mem -> Prop :=
  | exec_straight_opt_refl: forall blk c rs m,
      exec_straight_opt blk c rs m c rs m
  | exec_straight_opt_intro: forall blk c1 rs1 m1 c2 rs2 m2,
      exec_straight blk c1 rs1 m1 c2 rs2 m2 ->
      exec_straight_opt blk c1 rs1 m1 c2 rs2 m2.

Remark exec_straight_opt_right:
  forall blk c3 rs3 m3 c1 rs1 m1 c2 rs2 m2,
  exec_straight_opt blk c1 rs1 m1 c2 rs2 m2 ->
  exec_straight blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight blk c1 rs1 m1 c3 rs3 m3.
Proof.
  destruct 1; intros. auto. eapply exec_straight_trans; eauto.
Qed.

Remark exec_straight_opt_one:
  forall blk i1 c3 rs3 m3 rs1 m1 c2 rs2 m2,
  valid_mask_at_pc (rs1#PC) (rs1#MSK_CNT) ->
  rs2 PC = Val.offset_ptr (rs1 PC) Ptrofs.one ->
  rs2 MSK_CNT = next_msk_val_v (rs1 PC) (rs1 MSK_CNT) ->
  exec_instr ge blk fn i1 rs1 m1 = Next rs2 m2 ->
  exec_straight_opt blk c2 rs2 m2 c3 rs3 m3 ->
  exec_straight_opt blk (i1::c2) rs1 m1 c3 rs3 m3.
Proof.
  intros. inversion H3; subst.
  econstructor.
  econstructor; auto.
  econstructor.
  econstructor; eauto.
Qed.

(**************************************************************)

Lemma transf_function_no_overflow:
  forall f tf,
    transf_function f = OK tf ->
    list_length_z tf.(fn_code) <= Ptrofs.max_unsigned.
Proof.
  intros. monadInv H.
  destruct (zlt Ptrofs.max_unsigned (list_length_z x.(fn_code))); inv EQ0.
  omega.
Qed.

Lemma wordsize_not_zero': Ptrofs.wordsize <> 0%nat.
  unfold Ptrofs.wordsize.
  unfold Wordsize_Ptrofs.wordsize.
  destruct Archi.ptr64.
  auto.
  auto.
Qed.

(**** auxiliary functions **************************************)

Lemma wordsize_pos : (1 <= Ptrofs.wordsize)%nat.
  assert (forall n, n = Ptrofs.wordsize -> (0 < n)%nat).
  intros.
  destruct n.
  assert (Ptrofs.wordsize <> 0%nat).
  apply wordsize_not_zero'.
  intuition.
  intuition.
  intuition.
Qed.

Lemma two_power_nat_le_inj : forall (n1 n2: nat), (n1 <= n2)%nat ->
                                        two_power_nat n1 <= two_power_nat n2.
  intros.

  repeat rewrite two_power_nat_equiv.
  assert (Z.of_nat n1 <= Z.of_nat n2).
  intuition.

  eapply Zpower_le_monotone.
  intuition.
  intuition.
Qed.

Lemma two_power_nat_lt_inj : forall (n1 n2: nat), (n1 < n2)%nat ->
                                        two_power_nat n1 < two_power_nat n2.
  intros.

  repeat rewrite two_power_nat_equiv.
  assert (Z.of_nat n1 < Z.of_nat n2).
  intuition.

  eapply Zpower_lt_monotone.
  intuition.
  intuition.
Qed.

Lemma modulus_ge_2 : 2 <= Ptrofs.modulus.
  unfold Ptrofs.modulus.
  assert ((0 < Ptrofs.wordsize)%nat).
  apply wordsize_pos.

  assert (2 = two_power_nat 1).
  intuition.
  rewrite H0.

  eapply two_power_nat_le_inj.
  intuition.
Qed.

Lemma max_unsigned_pos : 1 <= Ptrofs.max_unsigned.
  unfold Ptrofs.max_unsigned.
  assert (2 <= Ptrofs.modulus).
  eapply modulus_ge_2.
  intuition.
Qed.

Lemma list_length_z_nil (A: Type) : @list_length_z A nil = 0.
  unfold list_length_z.
  simpl.
  auto.
Qed.

Lemma list_length_eq1 (A: Type) (ls: list A) : list_length_z ls =
                                            Z.of_nat (Datatypes.length ls).
  induction ls.
  simpl.
  eapply list_length_z_nil.
  simpl.
  rewrite list_length_z_cons.
  rewrite IHls.
  unfold Pos.of_succ_nat.
  clear IHls.
  generalize (Datatypes.length ls).
  intros.
  unfold Z.of_nat.
  destruct n.
  omega.
  simpl.
  f_equal.
  rewrite Pos.add_1_r.
  f_equal.
Qed.


Lemma list_length_eq2 (A: Type) (ls: list A) :
  Datatypes.length ls = Z.to_nat(list_length_z ls).
  rewrite list_length_eq1.
  rewrite Nat2Z.id.
  auto.
Qed.

Lemma list_length_z_pos (A: Type) (ls: list A) :
  0 <= list_length_z ls.
  induction ls.
  unfold list_length_z.
  simpl.
  intuition.
  rewrite list_length_z_cons.
  omega.
Qed.


Lemma inj_le_r (n m: Z) :
  0<=n -> 0<=m -> (n<=m -> (Z.to_nat n <= Z.to_nat m)%nat).
  intros.
  assert (n<=m <-> (Z.to_nat n <= Z.to_nat m)%nat).
  eapply Z2Nat.inj_le.
  auto.
  auto.
  intuition.
Qed.

Lemma inj_le_l (n m: Z) :
  0<=n -> 0<=m -> ((Z.to_nat n <= Z.to_nat m)%nat -> n<=m).
  intros.
  assert (n<=m <-> (Z.to_nat n <= Z.to_nat m)%nat).
  eapply Z2Nat.inj_le.
  auto.
  auto.
  intuition.
Qed.

Lemma lemma_aux4 (c: code) :
         (Datatypes.length c <= Z.to_nat Ptrofs.max_unsigned)%nat ->
              list_length_z c <= Ptrofs.max_unsigned.
  assert (0 <= list_length_z c).
  eapply list_length_z_pos.
  assert (1 <= Ptrofs.max_unsigned).
  eapply max_unsigned_pos.
  assert (0 <= Ptrofs.max_unsigned).
  intuition.
  clear H0.

  rewrite list_length_eq2 in *.

  revert H.
  revert H1.
  generalize (list_length_z c).
  generalize (Ptrofs.max_unsigned).
  intros.

  eapply inj_le_l.
  auto.
  auto.
  auto.
Qed.


Lemma nat_inj x y : 0 < x -> 0 < y -> Z.to_nat x = Z.to_nat y -> x = y.
  intros.
  eapply Z2Nat.inj.
  intuition.
  intuition.
  auto.
Qed.

Lemma add_to_nat_Lemma1 : forall i1 i2: ptrofs,
    Ptrofs.add_carry i1 i2 Ptrofs.zero = Ptrofs.zero ->
    (Ptrofs.unsigned i1) + (Ptrofs.unsigned i2) < Ptrofs.modulus ->
    Z.to_nat (Ptrofs.unsigned (Ptrofs.add i1 i2)) =
      ((Z.to_nat (Ptrofs.unsigned i1)) + (Z.to_nat (Ptrofs.unsigned i2)))%nat.
  intros.
  rewrite Ptrofs.unsigned_add_carry.
  destruct i1.
  destruct i2.
  simpl in *.
  rewrite <- Z2Nat.inj_add.
  f_equal.
  rewrite H.
  rewrite Ptrofs.unsigned_zero.
  simpl.
  omega.
  omega.
  omega.
Qed.

Lemma NatAdd_aux1 :
  forall (intval: Z) (intrange: -1 < intval < Ptrofs.modulus),
    intval < Ptrofs.max_unsigned ->
    (Nat.add (Z.to_nat
         (Ptrofs.unsigned
            {| Ptrofs.intval := intval; Ptrofs.intrange := intrange |}))
       (Z.to_nat (Ptrofs.unsigned Ptrofs.one)) = Nat.add (Z.to_nat intval) 1).

  intros.
  simpl.
  unfold Ptrofs.one.
  f_equal.
  assert (Ptrofs.unsigned (Ptrofs.repr 1) = 1).
  rewrite Ptrofs.unsigned_repr.
  reflexivity.
  omega.
  rewrite H0.
  simpl.
  reflexivity.
Qed.

Lemma add_carry_aux1: forall ofs: ptrofs,
      Ptrofs.intval ofs < Ptrofs.max_unsigned ->
      Ptrofs.add_carry ofs Ptrofs.one Ptrofs.zero = Ptrofs.zero.

  intros.
  unfold Ptrofs.add_carry.

  destruct (zlt
      (Ptrofs.unsigned ofs + Ptrofs.unsigned Ptrofs.one +
       Ptrofs.unsigned Ptrofs.zero) Ptrofs.modulus).
  reflexivity.

  unfold Ptrofs.max_unsigned in H.
  destruct ofs.
  simpl in g.
  assert (intval < Ptrofs.modulus - 1) as g1.
  auto.
  clear H.

  rewrite Ptrofs.unsigned_zero in g.
  assert (intval + Ptrofs.unsigned Ptrofs.one + 0 > Ptrofs.modulus - 1) as g0.
  omega.
  clear g.
  rewrite Ptrofs.unsigned_one in g0.
  omega.
Qed.


Lemma modulus_aux1 : forall ofs,
          Ptrofs.intval ofs < Ptrofs.max_unsigned ->
          Ptrofs.unsigned ofs + Ptrofs.unsigned Ptrofs.one < Ptrofs.modulus.
  intros.
  destruct ofs; simpl in *.

  unfold Ptrofs.max_unsigned in H.
  rewrite Ptrofs.unsigned_one.
  omega.
Qed.


Lemma zero_aux1: forall intval:Z, -1 < intval ->
    0%nat = Z.to_nat intval -> 0 = intval.
  intros.
  rewrite <- Z2Nat.inj_0 in H0.
  apply Z2Nat.inj in H0.
  assumption.
  intuition.
  intuition.
Qed.


(*********************************************************************)

Lemma pos_lt_aux1 : forall z n, z - 1 < Z.of_nat n ->
                          z < Z.pos (Pos.of_succ_nat n).
  intros.
  assert (z - 1 + 1 < Z.of_nat n + 1) as H0.
  intuition.
  assert (z - 1 + 1 = z) as H1.
  intuition.
  rewrite H1 in H0.
  assert (Z.of_nat n + 1 = Z.pos (Pos.of_succ_nat n)).
  unfold Z.of_nat.
  destruct n.
  intuition.
  simpl.
  f_equal.
  assert (forall m, (m + 1)%positive = Pos.succ m).
  intro.
  unfold Pos.succ.
  destruct m.
  simpl.
  reflexivity.
  simpl.
  reflexivity.
  simpl.
  reflexivity.
  rewrite H2.
  reflexivity.
  rewrite <- H2.
  assumption.
Qed.


Lemma find_instr_aux1 : forall z c i,
      find_instr z c = Some i -> z < list_length_z c.
  intros.
  revert H.
  revert z.
  revert i.
  induction c.
  simpl.
  intros.
  discriminate H.
  simpl. intros.
  destruct (zeq z 0).
  rewrite e.
  rewrite list_length_eq1.
  simpl.
  intuition.
  specialize (IHc i (z - 1) H).
  rewrite list_length_eq1 in *.
  simpl in *.
  eapply pos_lt_aux1.
  assumption.
Qed.


Lemma NatAdd_aux2 :
  forall (w: ptrofs),
    (Nat.add (Z.to_nat (Ptrofs.unsigned w))
             (Z.to_nat (Ptrofs.unsigned Ptrofs.one)) =
     Nat.add (Z.to_nat (Ptrofs.unsigned w)) 1).

  intros.
  destruct w.
  simpl.
  unfold Ptrofs.one.
  f_equal.
  assert (Ptrofs.unsigned (Ptrofs.repr 1) = 1).
  rewrite Ptrofs.unsigned_repr.
  reflexivity.
  unfold Ptrofs.max_unsigned.
  assert (2 <= Ptrofs.modulus) as H.
  eapply modulus_ge_2.
  omega.
  rewrite H.
  simpl.
  reflexivity.
Qed.

Lemma NatAdd_aux3 :
  forall (w: ptrofs),
    (Nat.add (Z.to_nat (Ptrofs.unsigned w))
             (Z.to_nat (Ptrofs.unsigned Ptrofs.one)) =
     Datatypes.S (Z.to_nat (Ptrofs.unsigned w))).
  intros.
  assert (Datatypes.S (Z.to_nat (Ptrofs.unsigned w)) =
                       Nat.add (Z.to_nat (Ptrofs.unsigned w)) 1).
  intuition.
  rewrite  H.
  eapply NatAdd_aux2.
Qed.


Lemma aux2 : forall ofs, Ptrofs.intval ofs < Ptrofs.max_unsigned ->
      Ptrofs.intval ofs < Ptrofs.intval (Ptrofs.add ofs Ptrofs.one).
  intros.
  unfold Ptrofs.add.

  destruct ofs.
  simpl in *.
  rewrite Ptrofs.unsigned_one.
  intuition.

  rewrite Ptrofs.unsigned_repr_eq.

  rewrite Zmod_small.
  intuition.
  intuition.
  unfold Ptrofs.max_unsigned in H.

  intuition.
Qed.

Lemma aux1 (ofs: ptrofs) :
    Ptrofs.intval ofs < Ptrofs.max_unsigned ->
    (Datatypes.S (Z.to_nat (Ptrofs.intval ofs)) =
          Z.to_nat (Ptrofs.intval (Ptrofs.add ofs Ptrofs.one))).
  rewrite <- NatAdd_aux3.
  intro.
  rewrite add_to_nat_Lemma1.
  reflexivity.

  rewrite add_carry_aux1.
  reflexivity.

  assumption.

  unfold Ptrofs.max_unsigned in H.
  rewrite Ptrofs.unsigned_one.
  unfold Ptrofs.unsigned.
  intuition.
Qed.


(************************************************************************)

Lemma zero_simpl1 : Ptrofs.unsigned Ptrofs.one = Z.one.

  unfold Ptrofs.one.
  rewrite Ptrofs.unsigned_repr.
  intuition.
  intuition.
  eapply max_unsigned_pos.
Qed.


Lemma contr_aux1 : forall ofs,
  Ptrofs.unsigned ofs < Ptrofs.max_unsigned ->
  Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one) > Ptrofs.max_unsigned -> False.
Proof.
  intros.
  rewrite Ptrofs.add_unsigned in H0.
  rewrite zero_simpl1 in H0.
  destruct ofs.
  simpl in *.
  unfold Z.one in *.
  intuition.
  rewrite Ptrofs.unsigned_repr in H0.
  intuition.
  intuition.
Qed.

Lemma int_ge_simpl : forall x y: Z, x >= y -> x > y \/ x = y.
Proof.
   intros; destruct (Z.eq_dec x y); lia.
Qed.

Lemma add_one_rel_simpl1 :
  forall ofs, Ptrofs.unsigned ofs < Ptrofs.max_unsigned ->
      Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one) >= Ptrofs.max_unsigned ->
      Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one) = Ptrofs.max_unsigned.
Proof.
  intros.
  eapply int_ge_simpl in H0.
  destruct H0.
  assert (False).
  eapply contr_aux1.
  eassumption.
  assumption.
  intuition.
  assumption.
Qed.

Lemma add_one_rel_simpl2 :
  forall ofs, Ptrofs.unsigned ofs >= Ptrofs.max_unsigned ->
    Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one) < Ptrofs.max_unsigned ->
    Ptrofs.unsigned ofs = Ptrofs.max_unsigned.
  intros.
  eapply int_ge_simpl in H.
  destruct H.

  destruct ofs.
  simpl in *.
  unfold Ptrofs.max_unsigned in H.
  assert (intval >= Ptrofs.modulus).
  intuition.
  assert False.
  intuition.
  intuition.

  rewrite H.
  reflexivity.
Qed.


Lemma max_unsigned_aux1 :
          Ptrofs.max_unsigned + Ptrofs.unsigned Ptrofs.one = Ptrofs.modulus.

  unfold Ptrofs.max_unsigned.
  rewrite Ptrofs.unsigned_one.
  omega.
Qed.


Lemma repr_modulus_zero : Ptrofs.unsigned (Ptrofs.repr Ptrofs.modulus) = 0.
  rewrite Ptrofs.unsigned_repr_eq.
  generalize Ptrofs.modulus.
  intro.
  eapply Z_mod_same_full. (* from AZrith.Zdiv) *)
Qed.


Lemma one_is_one1 : Ptrofs.unsigned (Ptrofs.repr 1) = 1.
  rewrite Ptrofs.unsigned_repr.
  reflexivity.
  intuition.
  eapply max_unsigned_pos.
Qed.

Lemma modulus_plus_one1:
  Ptrofs.unsigned (Ptrofs.add (Ptrofs.repr Ptrofs.modulus)
                                         Ptrofs.one) = 1.
  unfold Ptrofs.add.
  rewrite repr_modulus_zero.
  simpl.
  rewrite zero_simpl1.
  simpl.
  eapply one_is_one1.
Qed.


Lemma max_unsigned_simpl1: forall v, v < Ptrofs.modulus ->
                     v >= Ptrofs.max_unsigned -> v = Ptrofs.max_unsigned.
  intros.
  unfold Ptrofs.max_unsigned in *.
  assert (v + 1 >= Ptrofs.modulus - 1 + 1).
  intuition.
  assert (v + 1 >= Ptrofs.modulus).
  intuition.
  intuition.
Qed.

Lemma max_unsigned_contr1: forall ofs,
    Ptrofs.unsigned ofs >= Ptrofs.max_unsigned ->
    Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one) >= Ptrofs.max_unsigned ->
    False.
  intros.
  destruct ofs.
  simpl in *.
  assert (intval = Ptrofs.max_unsigned).
  eapply max_unsigned_simpl1.
  intuition.
  assumption.
  inversion H1; subst.
  clear H H2.
  unfold Ptrofs.add in H0.
  simpl in *.
  rewrite max_unsigned_aux1 in H0.
  rewrite Ptrofs.unsigned_repr_eq in H0.
  rewrite Z_mod_same_full in H0.
  generalize max_unsigned_pos.
  intro.
  intuition.
Qed.

(***************************************************************************)

Lemma valid_mask_zero : forall f,
      valid_mask_at_pc (Vptr f Ptrofs.zero) (Vint (generate_mskA f)).

  intros.
  econstructor.
  reflexivity.
  f_equal.
Qed.


Lemma valid_mask_incr2_aux : forall (fb0 : Values.block)
  (ofs0 : ptrofs),
  next_msk_val_v (Val.offset_ptr (Vptr fb0 ofs0) Ptrofs.one)
    (next_msk_val_v (Vptr fb0 ofs0)
       (Vint (encrypt_msk (Ptrofs.unsigned ofs0) fb0))) =
  Vint
    (encrypt_msk
       (Ptrofs.unsigned (Ptrofs.add (Ptrofs.add ofs0 Ptrofs.one) Ptrofs.one))
       fb0).

  intros.

  unfold next_msk_val_v.
  unfold next_msk_val.
  simpl.

  unfold encrypt_msk.
(*  unfold encrypt_block_msk. *)
  simpl.

  destruct (zlt (Ptrofs.unsigned ofs0) Ptrofs.max_unsigned).
  simpl.
  destruct (zlt (Ptrofs.unsigned (Ptrofs.add ofs0 Ptrofs.one))
                Ptrofs.max_unsigned).

  f_equal.

  unfold Ptrofs.unsigned.

  unfold encrypt_msk.
(*  unfold encrypt_block_msk. *)
  simpl.
  rewrite <- aux1.
  simpl.
  rewrite <- aux1.
  simpl.
  reflexivity.
  assumption.
  assumption.

  unfold encrypt_msk.
(*  unfold encrypt_block_msk. *)
  simpl.
  f_equal.

  assert (Ptrofs.unsigned (Ptrofs.add ofs0 Ptrofs.one) =
          Ptrofs.max_unsigned)
    as H11.
  eapply add_one_rel_simpl1.
  assumption.
  assumption.

  unfold Ptrofs.add at 1.
  rewrite H11.

  rewrite max_unsigned_aux1.

  rewrite repr_modulus_zero.
  simpl.
  reflexivity.

  destruct (zlt (Ptrofs.unsigned (Ptrofs.add ofs0 Ptrofs.one))
                Ptrofs.max_unsigned).
  f_equal.

  assert (Ptrofs.unsigned ofs0 = Ptrofs.max_unsigned) as H11.
  eapply add_one_rel_simpl2.
  assumption.
  assumption.

  unfold Ptrofs.add at 2.
  rewrite H11.

  rewrite max_unsigned_aux1.
  rewrite modulus_plus_one1.

  simpl.
  reflexivity.

  assert False.
  eapply (max_unsigned_contr1 ofs0).
  assumption.
  assumption.
  intuition.
Qed.


Lemma valid_mask_incr2 : forall fb0 ofs0,
    valid_mask_at_pc
    (Val.offset_ptr (Val.offset_ptr (Vptr fb0 ofs0) Ptrofs.one) Ptrofs.one)
    (next_msk_val_v (Val.offset_ptr (Vptr fb0 ofs0) Ptrofs.one)
       (next_msk_val_v (Vptr fb0 ofs0)
          (Vint (encrypt_msk (Ptrofs.unsigned ofs0) fb0)))).
  intros.

  econstructor.
  reflexivity.
  eapply valid_mask_incr2_aux.
Qed.

Lemma valid_mask_at_pc_lemma2 (rs: regset)
   (H : valid_mask_at_pc (rs PC) (rs MSK_CNT)) :
    valid_mask_at_pc
    (Val.offset_ptr (Val.offset_ptr (rs PC) Ptrofs.one) Ptrofs.one)
    (next_msk_val_v (Val.offset_ptr (rs PC) Ptrofs.one)
       (next_msk_val_v (rs PC) (rs MSK_CNT))).
  inversion H; subst.
  rewrite H0.
  rewrite H1.

  eapply valid_mask_incr2.
Qed.


Lemma one_incr_mask_lemma : forall fb0 ofs,
    next_msk_val_v (Vptr fb0 ofs)
                   (Vint (encrypt_msk (Ptrofs.unsigned ofs) fb0)) =
    Vint (encrypt_msk (Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one)) fb0).

    intros.

    remember (zlt (Ptrofs.intval ofs) Ptrofs.max_unsigned) as q.
    destruct q.

    + unfold encrypt_msk.
      simpl.

      destruct (zlt (Ptrofs.unsigned ofs) Ptrofs.max_unsigned).
      f_equal.

      unfold Ptrofs.unsigned.

      rewrite <- aux1.
      simpl.

      destruct (zeq (Ptrofs.intval ofs) 0).
      reflexivity.

      reflexivity.
      assumption.
      intuition.

    + simpl.
      rewrite Ptrofs.unsigned_add_carry.

      unfold Ptrofs.add_carry.
      rewrite Ptrofs.unsigned_zero.
      rewrite Ptrofs.unsigned_one.

      assert (Ptrofs.intval ofs = Ptrofs.max_unsigned) as K0.
      {- destruct ofs.
         simpl in *.
         unfold Ptrofs.max_unsigned in *.
         intuition.
      }

      unfold Ptrofs.unsigned.
      rewrite K0.

      assert (Ptrofs.max_unsigned + 1 + 0 = Ptrofs.modulus) as K1.
      {- unfold Ptrofs.max_unsigned.
         intuition.
      }
      rewrite K1.

      destruct (zlt Ptrofs.max_unsigned Ptrofs.max_unsigned).
      intuition.

      destruct (zlt Ptrofs.modulus Ptrofs.modulus).
      intuition.

      assert (Ptrofs.intval Ptrofs.one = Ptrofs.unsigned Ptrofs.one) as K3.
      {- reflexivity. }

      rewrite K3.
      rewrite Ptrofs.unsigned_one.

      assert (Ptrofs.max_unsigned + 1 - 1 * Ptrofs.modulus = 0) as K4.
      {- intuition. }
      rewrite K4.

      unfold encrypt_msk.
      simpl.
      reflexivity.
Qed.


Lemma valid_mask_at_pc_lemma1 (rs: regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT)) :
  valid_mask_at_pc (Val.offset_ptr (rs PC) Ptrofs.one)
                   (next_msk_val_v (rs PC) (rs MSK_CNT)).
  inversion H; subst.
  econstructor.
  unfold Val.offset_ptr.
  rewrite H0; auto.
  rewrite H0.
  rewrite H1.
  simpl.
  eapply one_incr_mask_lemma.
Qed.


Lemma valid_mask_at_pc_lemma3 (rs: regset)
 (H : valid_mask_at_pc (rs PC) (rs MSK_CNT)) :
    valid_mask_at_pc
    (Val.offset_ptr
       (Val.offset_ptr (Val.offset_ptr (rs PC) Ptrofs.one) Ptrofs.one)
       Ptrofs.one)
    (next_msk_val_v
       (Val.offset_ptr (Val.offset_ptr (rs PC) Ptrofs.one) Ptrofs.one)
       (next_msk_val_v (Val.offset_ptr (rs PC) Ptrofs.one)
                       (next_msk_val_v (rs PC) (rs MSK_CNT)))).
  inversion H; subst.
  rewrite H0.
  rewrite H1.

  econstructor; eauto.
  simpl; eauto.

  simpl.

  rewrite <- valid_mask_incr2_aux.
  f_equal.
  f_equal.
  rewrite <- one_incr_mask_lemma.
  simpl.
  auto.
Qed.


(***************** main crypto lemmas ************************************)

Lemma msk_straight_preserve :
      forall fb c rs m c' rs' m',
      valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
      exec_straight fb c rs m c' rs' m' ->
      valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
  intros.
  induction H0.
  inversion H; subst.
  rewrite H2.
  rewrite H3.
  rewrite H4.
  rewrite H5.
  econstructor.
  reflexivity.
  eapply one_incr_mask_lemma.

  inversion H0; subst.
  eapply IHexec_straight.
  rewrite H2.
  rewrite H3.
  rewrite H5.
  rewrite H6.
  econstructor.
  reflexivity.
  eapply one_incr_mask_lemma.
Qed.


Lemma msk_straight_opt_preserve :
      forall fb c rs m c' rs' m',
      valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
      exec_straight_opt fb c rs m c' rs' m' ->
      valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
  intros.
  inversion H0; subst; auto.
  eapply msk_straight_preserve; eauto.
Qed.


(*********************************************************************)

Lemma nextinstr_preserve_lemma2a
  (rd r0 : ireg)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr rs # rd <- (rs r0)) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    simpl in H0.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma2
  (rd : ireg)
  (v : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr rs # rd <- v) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    simpl in H0.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.


Lemma nextinstr_preserve_lemma3
  (rd : freg)
  (v : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr rs # rd <- v) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    simpl in H0.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.


Lemma nextinstr_preserve_lemma1
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr rs) m = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    inversion H0; subst.
    unfold nextinstr.
    rewrite H1.
    rewrite H2.
    econstructor.
    Simpl.
    simpl. reflexivity.
    Simpl.
    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma7
  (rd : mskreg)
  (v : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr rs # rd <- v) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    simpl in H0.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.


Lemma nextinstrTail_preserve_lemma1
  (fb : block)
  (f : function)
  (symb : ident)
  (sg : signature)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstrTail rs (Genv.symbol_address ge symb Ptrofs.zero)) m =
        Next rs' m')
  (K: valid_mask_at_pc (rs' PC) (rs MSK_BRN)) :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    simpl in H0.
    inversion H; subst.
    inversion H0; subst.
    unfold nextinstrTail.
    Simpl.
Qed.

Lemma nextinstrTail_preserve_lemma2
  (rs : preg -> val)
  (m : mem)
  (v: val)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstrTail rs v) m =
        Next rs' m')
  (K: valid_mask_at_pc (rs' PC) (rs MSK_BRN)) :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    simpl in H0.
    inversion H; subst.
    inversion H0; subst.
    unfold nextinstrTail.
    Simpl.
Qed.

Lemma nextinstrCall_preserve_lemma2
  (rs : preg -> val)
  (m : mem)
  (v: val)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstrCall rs v) m =
        Next rs' m')
  (K: valid_mask_at_pc (rs' PC) (rs MSK_BRN)) :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    simpl in H0.
    inversion H; subst.
    inversion H0; subst.
    unfold nextinstrCall.
    Simpl.
Qed.

Lemma goto_preserve_lemma1
  (f : function)
  (l : label)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : goto_label f l rs m = Next rs' m')
  (K : valid_mask_at_pc (rs' PC) (rs MSK_BRN)) :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    inversion K; subst.
    unfold goto_label in H0.
    destruct f.
    simpl in H0.
    destruct (label_pos l 0 fn_code).
    rewrite H1 in H0.
    inversion H0; subst.
    unfold nextinstrTail.
    Simpl.
    inversion H0.
Qed.


Lemma branch_preserve_lemma1
  (fb : block)
  (f : function)
  (rs1 rs2 : ireg0)
  (l : label)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : eval_branch f l rs m
         (Val.cmpu_bool (Mem.valid_pointer m) Ceq rs ## rs1 rs ## rs2) =
       Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT) \/
  (valid_mask_at_pc (rs' PC) (rs MSK_BRN) ->
   valid_mask_at_pc (rs' PC) (rs' MSK_CNT)).
    inversion H; subst.
    unfold eval_branch in H0.
    destruct (Val.cmpu_bool (Mem.valid_pointer m) Ceq rs ## rs1 rs ## rs2).
    + destruct b.
      * right. intro K.
        eapply goto_preserve_lemma1; eauto.
      * left.
        eapply nextinstr_preserve_lemma1; eauto.
    + inversion H0.
Qed.

Lemma branch_preserve_lemma2
  (fb : block)
  (f : function)
  (l : label)
  (bb: option bool)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : eval_branch f l rs m bb = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT) \/
  (valid_mask_at_pc (rs' PC) (rs MSK_BRN) ->
   valid_mask_at_pc (rs' PC) (rs' MSK_CNT)).
    inversion H; subst.
    unfold eval_branch in H0.
    destruct bb.
    + destruct b.
      * right. intro K.
        eapply goto_preserve_lemma1; eauto.
      * left.
        eapply nextinstr_preserve_lemma1; eauto.
    + inversion H0.
Qed.

Lemma load_preserve_lemma1
  (rd ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_load ge c rs m rd ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT) \/
  (valid_mask_at_pc (rs' PC) (rs MSK_BRN) ->
   valid_mask_at_pc (rs' PC) (rs' MSK_CNT)).
    inversion H; subst.
    unfold exec_load in H0.
    destruct (Mem.loadv c m (Val.offset_ptr (rs ra) (eval_offset ge ofs))).
    left.
    eapply nextinstr_preserve_lemma2; eauto.
    inversion H0.
Qed.

Lemma load_preserve_lemma2
  (rd ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_load ge c rs m rd ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_load in H0.
    destruct (Mem.loadv c m (Val.offset_ptr (rs ra) (eval_offset ge ofs))).
    eapply nextinstr_preserve_lemma2; eauto.
    inversion H0.
Qed.

Lemma load_preserve_lemma3
  (rd : freg)
  (ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_load ge c rs m rd ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_load in H0.
    destruct (Mem.loadv c m (Val.offset_ptr (rs ra) (eval_offset ge ofs))).
    eapply nextinstr_preserve_lemma3; eauto.
    inversion H0.
Qed.


Lemma load_preserve_lemma4
  (rd : mskreg)
  (ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_load ge c rs m rd ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_load in H0.
    destruct (Mem.loadv c m (Val.offset_ptr (rs ra) (eval_offset ge ofs))).
    eapply nextinstr_preserve_lemma7; eauto.
    inversion H0.
Qed.


Lemma store_preserve_lemma2
  (r0 ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_store ge c rs m r0 ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_store in H0.
    destruct (Mem.storev c m
           (Val.offset_ptr (rs ra) (eval_offset ge ofs))
           (rs r0)).
    eapply nextinstr_preserve_lemma1; eauto.
    inversion H0.
Qed.

Lemma store_preserve_lemma3
  (r0 : freg)
  (ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_store ge c rs m r0 ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_store in H0.
    destruct (Mem.storev c m
           (Val.offset_ptr (rs ra) (eval_offset ge ofs))
           (rs r0)).
    eapply nextinstr_preserve_lemma1; eauto.
    inversion H0.
Qed.


Lemma store_preserve_lemma4
  (r0 : mskreg)
  (ra : ireg)
  (c : memory_chunk)
  (ofs : offset)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_store ge c rs m r0 ra ofs = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    inversion H; subst.
    unfold exec_store in H0.
    destruct (Mem.storev c m
           (Val.offset_ptr (rs ra) (eval_offset ge ofs))
           (rs r0)).
    eapply nextinstr_preserve_lemma1; eauto.
    inversion H0.
Qed.


Lemma nextinstr_preserve_lemma4a
  (b : block)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next
         (nextinstr
            ((rs # X30 <- (rs X2)) # X2 <- (Vptr b Ptrofs.zero)) # X31 <-
            Vundef) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma4
  (v1 v2 v3 : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next
         (nextinstr
            ((rs # X30 <- v1) # X2 <- v2) # X31 <- v3) m =
        Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.

    inversion H0; subst.

    econstructor.
    unfold nextinstr.
    Simpl.

    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.

    unfold nextinstr.
    Simpl.

    rewrite H1.
    rewrite H2.
    simpl.

    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma5a
  (v1 v2 : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr (rs # X2 <- v1) # X31 <- v2) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.
    inversion H0; subst.
    unfold nextinstr.
    econstructor.
    Simpl.
    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.
    Simpl.
    rewrite H1.
    rewrite H2.
    simpl.
    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma5
  (rd1 rd2 : ireg)
  (v1 v2 : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr (rs # rd1 <- v1) # rd2 <- v2) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.
    inversion H0; subst.
    unfold nextinstr.
    econstructor.
    Simpl.
    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.
    Simpl.
    rewrite H1.
    rewrite H2.
    simpl.
    eapply one_incr_mask_lemma.
Qed.

Lemma nextinstr_preserve_lemma6
  (rd1 : ireg)
  (rd2 : freg)
  (v1 v2 : val)
  (rs : preg -> val)
  (m m': mem)
  (rs' : regset)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstr (rs # rd1 <- v1) # rd2 <- v2) m = Next rs' m') :
    valid_mask_at_pc (rs' PC) (rs' MSK_CNT).

    inversion H; subst.
    inversion H0; subst.
    unfold nextinstr.
    econstructor.
    Simpl.
    unfold Val.offset_ptr.
    rewrite H1.
    reflexivity.
    Simpl.
    rewrite H1.
    rewrite H2.
    simpl.
    eapply one_incr_mask_lemma.
Qed.


Lemma nextinstrTail_preserve_lemma3
  (rs : preg -> val)
  (m : mem)
  (v: val)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : Next (nextinstrTail ((rs # X5 <- Vundef) # X31 <- Vundef) v) m =
        Next rs' m')
  (K: valid_mask_at_pc (rs' PC) (rs MSK_BRN)) :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
    simpl in H0.
    inversion H; subst.
    inversion H0; subst.
    unfold nextinstrTail.
    Simpl.
Qed.


Lemma btbl_preserve_lemma1
  (fb : block)
  (f : function)
  (r : ireg)
  (tbl : list label)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_instr ge fb f (Pbtbl r tbl) rs m = Next rs' m') :
  (valid_mask_at_pc (rs' PC) (rs MSK_BRN) ->
   valid_mask_at_pc (rs' PC) (rs' MSK_CNT)).
     simpl in H0.
     destruct (rs r).
     - inversion H0.
     - destruct (list_nth_z tbl (Int.unsigned i)).

       + intro K.
         inversion H; subst.
         inversion K; subst.
         unfold goto_label in H0.
         destruct (label_pos l 0 (fn_code f)).
         destruct ((rs # X5 <- Vundef) # X31 <- Vundef PC).
         inversion H0.
         inversion H0.
         inversion H0.
         inversion H0.
         inversion H0.
         eapply nextinstrTail_preserve_lemma3; eauto.
         inversion H0.
       + inversion H0.
     - inversion H0.
     - inversion H0.
     - inversion H0.
     - inversion H0.
Qed.

Lemma LoadCodeMSK_name_preserve_lemma
  (fb : block)
  (f : function)
  (m0 : mskreg)
  (m1 : mskname)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_instr ge fb f (LoadCodeMSK_name m0 m1) rs m = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
Proof.
     simpl in H0.
     inversion H; subst.
     destruct m1.
     unfold exec_loadCodeMSK_byLabel in H0.
     destruct (label_pos l 0 (fn_code f)).
     unfold exec_loadCodeMSK in H0.
     inversion H0; subst.

     unfold nextinstr.

     econstructor.
     Simpl.
     rewrite H1.
     simpl.
     reflexivity.

     Simpl.
     rewrite H1.
     rewrite H2.
     Simpl.

     simpl.
     eapply one_incr_mask_lemma.

     unfold exec_loadCodeMSK in H0.
     inversion H0; subst.
     unfold nextinstr.
     Simpl.
     econstructor.
     rewrite H1.
     simpl.
     reflexivity.

     rewrite H1.
     rewrite H2.
     Simpl.
     simpl.
     eapply one_incr_mask_lemma.

     unfold exec_loadCodeMSK_byIdent in H0.
     destruct (Genv.find_symbol ge i).
     unfold exec_loadCodeMSK in H0.
     inversion H0; subst.

     unfold nextinstr.
     Simpl.
     eapply valid_mask_at_pc_lemma1; auto.
     inversion H0.
Qed.

Lemma exec_loadCodeMSK_byReg_preserve_lemma
  (f : function)
  (m0 : mskreg)
  (i : ireg)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_loadCodeMSK_byReg i m0 rs m = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
Proof.
     inversion H; subst.
     unfold exec_loadCodeMSK_byReg in H0.
     destruct (rs i).
     inversion H0.
     inversion H0.
     inversion H0.
     inversion H0.
     inversion H0.
     destruct (Ptrofs.eq i0 Ptrofs.zero).
     unfold exec_loadCodeMSK in H0.
     inversion H0; subst.
     unfold nextinstr.
     Simpl.
     eapply valid_mask_at_pc_lemma1; auto.
     inversion H0.
Qed.

Lemma freeframe_preserve_lemma
  (fb : block)
  (f : function)
  (sz : Z)
  (pos : ptrofs)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_instr ge fb f (Pfreeframe sz pos) rs m = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
Proof.
    simpl in H0.
    inversion H; subst.
    destruct (Mem.loadv Mptr m (Val.offset_ptr (rs X2) pos)).
    2: { inversion H0. }
    destruct (rs X2).
    inversion H0.
    inversion H0.
    inversion H0.
    inversion H0.
    inversion H0.
    destruct (Mem.free m b 0 sz).
    eapply nextinstr_preserve_lemma5; eauto.
    inversion H0.
Qed.

Lemma allocframe_preserve_lemma
  (fb : block)
  (f : function)
  (sz : Z)
  (pos : ptrofs)
  (rs : preg -> val)
  (m : mem)
  (rs' : regset)
  (m' : mem)
  (H : valid_mask_at_pc (rs PC) (rs MSK_CNT))
  (H0 : exec_instr ge fb f (Pallocframe sz pos) rs m = Next rs' m') :
  valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
Proof.
    simpl in H0.
    destruct (Mem.alloc m 0 sz).
    destruct (Mem.store Mptr m0 b
                        (Ptrofs.unsigned (Ptrofs.add Ptrofs.zero pos))
                        (rs X2)).
    eapply nextinstr_preserve_lemma4; eauto.
    inversion H0.
Qed.


(*******************************************************************)


Lemma msk_preserve2 :
      forall fb f i rs m rs' m',
      valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
      exec_instr ge fb f i rs m = Next rs' m' ->
      (valid_mask_at_pc (rs' PC) (rs' MSK_CNT) \/
       (valid_mask_at_pc (rs' PC) (rs MSK_BRN) ->
        valid_mask_at_pc (rs' PC) (rs' MSK_CNT))).
  intros fb f i rs m rs' m'.
  intros H H0.

  induction i.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. right. intro.
    eapply goto_preserve_lemma1; eauto.

  - simpl in H0. right. intro.
    eapply nextinstrTail_preserve_lemma2; eauto.

  - simpl in H0. right. intro.
    eapply nextinstrTail_preserve_lemma2; eauto.

  - simpl in H0. right. intro.
    eapply nextinstrCall_preserve_lemma2; eauto.

  - simpl in H0. right. intro.
    eapply nextinstrCall_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0.
    eapply branch_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    inversion H0.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0. left.
    eapply nextinstr_preserve_lemma3; eauto.

  - left.
    eapply allocframe_preserve_lemma; eauto.

  - left.
    eapply freeframe_preserve_lemma; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma1; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma2; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma2; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma5; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma6; eauto.

   - simpl in H0. left.
     eapply nextinstr_preserve_lemma6; eauto.

   - right.
     eapply btbl_preserve_lemma1; eauto.

   - simpl in H0. left.
     inversion H0.

   - simpl in H0. left.
     inversion H0.

   - left.
     eapply LoadCodeMSK_name_preserve_lemma; eauto.

   - simpl in H0. left.
     eapply exec_loadCodeMSK_byReg_preserve_lemma; eauto.

   - simpl in H0. left.
     eapply load_preserve_lemma4; eauto.

   - simpl in H0. left.
     eapply store_preserve_lemma4; eauto.
Qed.


Definition is_jump (i: instruction) : bool :=
  match i with
  | Pj_l _ => true
  | Pj_s _ _ => true
  | Pj_r _ _ => true
  | Pjal_s _ _ => true
  | Pjal_r _ _ => true
  | Pbtbl _ _ => true
  | _ => false
  end.

Definition no_jump (i: instruction) : Prop :=
  match (is_jump i) with
  | true => False
  | _ => True
  end.

Definition is_branch (i: instruction) : bool :=
  match i with
  | Pbeqw _ _ _ => true
  | Pbnew _ _ _ => true
  | Pbltw _ _ _ => true
  | Pbltuw _ _ _ => true
  | Pbgew _ _ _ => true
  | Pbgeuw _ _ _ => true
  | Pbeql _ _ _ => true
  | Pbnel _ _ _ => true
  | Pbltl _ _ _ => true
  | Pbltul _ _ _ => true
  | Pbgel _ _ _ => true
  | Pbgeul _ _ _ => true
  | _ => false
  end.

Definition no_branch (i: instruction) : Prop :=
  match (is_branch i) with
  | true => False
  | _ => True
  end.


Lemma msk_preserve :
      forall fb f i rs m rs' m',
      no_jump i -> no_branch i ->
      valid_mask_at_pc (rs PC) (rs MSK_CNT) ->
      exec_instr ge fb f i rs m = Next rs' m' ->
      valid_mask_at_pc (rs' PC) (rs' MSK_CNT).
  intros fb f i rs m rs' m'.
  intros C1 C2 H H0.

  unfold no_jump, is_jump in C1.
  unfold no_branch, is_branch in C2.

  destruct i; inversion C1; inversion C2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply load_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    eapply store_preserve_lemma2; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0.
    eapply load_preserve_lemma3; eauto.

  - simpl in H0.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0.
    eapply store_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    inversion H0.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma2; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - simpl in H0.
    eapply nextinstr_preserve_lemma3; eauto.

  - eapply allocframe_preserve_lemma; eauto.

  - eapply freeframe_preserve_lemma; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma1; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma2; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma2; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma5; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma6; eauto.

   - simpl in H0.
     eapply nextinstr_preserve_lemma6; eauto.

   - simpl in H0.
     inversion H0.

   - simpl in H0.
     inversion H0.

   - eapply LoadCodeMSK_name_preserve_lemma; eauto.

   - simpl in H0.
     eapply exec_loadCodeMSK_byReg_preserve_lemma; eauto.

   - simpl in H0.
     eapply load_preserve_lemma4; eauto.

   - simpl in H0.
     eapply store_preserve_lemma4; eauto.
Qed.



(************************ main RISC-V lemma *****************************)

Lemma exec_straight_steps_1:
  forall blk c (rs: preg -> val) m c' rs' m',
    exec_straight blk c rs m c' rs' m' ->
    list_length_z (fn_code fn) <= Ptrofs.max_unsigned ->
    Genv.find_funct_ptr ge blk = Some (Internal fn) ->
  forall ofs,
  rs#PC = Vptr blk ofs ->
  code_tail (Ptrofs.unsigned ofs) (fn_code fn) c ->
  plus step ge (State rs m) E0 (State rs' m').
Proof.

  intro blk.

  induction 1; intros.
  apply plus_one.
  econstructor; eauto.
  eapply find_instr_tail. eauto.

  eapply plus_left'.
  econstructor; eauto.
  eapply find_instr_tail. eauto.
  apply IHexec_straight with (Ptrofs.add ofs Ptrofs.one).
  exact H4.
  exact H5.
  rewrite H6 in H1.
  simpl in H1.
  exact H1.
  eapply code_tail_next_int with i; eauto.
  traceEq.
Qed.


(*************************************************************)

Lemma exec_straight_steps_2:  (* blk should be the same as b ! *)
  forall blk c rs m c' rs' m',
  exec_straight blk c rs m c' rs' m' ->
  list_length_z (fn_code fn) <= Ptrofs.max_unsigned ->
  forall b ofs,
  rs#PC = Vptr b ofs ->
  Genv.find_funct_ptr ge b = Some (Internal fn) ->
  code_tail (Ptrofs.unsigned ofs) (fn_code fn) c ->
  exists ofs',
     rs'#PC = Vptr b ofs'
  /\ code_tail (Ptrofs.unsigned ofs') (fn_code fn) c'.
Proof.
  intro blk.
  induction 1; intros.
  exists (Ptrofs.add ofs Ptrofs.one).
  rewrite H4 in *.
  simpl in *.
  split.
  exact H1.
  apply code_tail_next_int with i1; auto.
  apply IHexec_straight with (Ptrofs.add ofs Ptrofs.one).
  auto. rewrite H1. rewrite H5. reflexivity. auto.
  apply code_tail_next_int with i; auto.
Qed.

Lemma exec_straight_steps_2a:  (* blk should be the same as b -> fixed *)
  forall blk c rs m c' rs' m',
  exec_straight blk c rs m c' rs' m' ->
  list_length_z (fn_code fn) <= Ptrofs.max_unsigned ->
  forall ofs,
  rs#PC = Vptr blk ofs ->
  Genv.find_funct_ptr ge blk = Some (Internal fn) ->
  code_tail (Ptrofs.unsigned ofs) (fn_code fn) c ->
  exists ofs',
     rs'#PC = Vptr blk ofs'
  /\ code_tail (Ptrofs.unsigned ofs') (fn_code fn) c'.
Proof.
  intro blk.
  induction 1; intros.
  exists (Ptrofs.add ofs Ptrofs.one).
  rewrite H4 in *.
  simpl in *.
  split.
  exact H1.
  apply code_tail_next_int with i1; auto.
  apply IHexec_straight with (Ptrofs.add ofs Ptrofs.one).
  auto. rewrite H1. rewrite H5. reflexivity. auto.
  apply code_tail_next_int with i; auto.
Qed.

Lemma exec_straight_opt_steps_2a:  (* blk should be the same as b -> fixed *)
  forall blk c rs m c' rs' m',
  exec_straight_opt blk c rs m c' rs' m' ->
  list_length_z (fn_code fn) <= Ptrofs.max_unsigned ->
  forall ofs,
  rs#PC = Vptr blk ofs ->
  Genv.find_funct_ptr ge blk = Some (Internal fn) ->
  code_tail (Ptrofs.unsigned ofs) (fn_code fn) c ->
  exists ofs',
     rs'#PC = Vptr blk ofs'
  /\ code_tail (Ptrofs.unsigned ofs') (fn_code fn) c'.
Proof.
  intros until m'. intro H.
  inversion H; subst.
  intros.
  exists ofs.
  intuition.

  clear H.
  induction H0.
  intros.
  exists (Ptrofs.add ofs Ptrofs.one).
  rewrite H1 in *.
  simpl in *.
  split.
  rewrite H4.
  simpl.
  reflexivity.
  apply code_tail_next_int with i1; auto.

  intros.
  apply IHexec_straight with (Ptrofs.add ofs Ptrofs.one).
  auto. rewrite H1. rewrite H5. reflexivity. auto.
  apply code_tail_next_int with i; auto.
Qed.

End STRAIGHTLINE.

(** * Properties of the Mach call stack *)

Section MATCH_STACK.

Variable ge: Mach.genv.

Inductive match_stack: list Mach.stackframe -> Prop :=
  | match_stack_nil:
      match_stack nil
  | match_stack_cons: forall fb sp ra rm c s f tf tc,
      Genv.find_funct_ptr ge fb = Some (Internal f) ->
      transl_code_at_pc ge ra fb f c false tf tc ->
      sp <> Vundef ->
      valid_mask_at_pc ra rm ->
      match_stack s ->
      match_stack (Stackframe fb sp ra rm c :: s).

Lemma parent_sp_def: forall s, match_stack s -> parent_sp s <> Vundef.
Proof.
  induction 1; simpl.
  unfold Vnullptr; destruct Archi.ptr64; congruence.
  auto.
Qed.

Lemma parent_ra_def: forall s, match_stack s -> parent_ra s <> Vundef.
Proof.
  induction 1; simpl.
  unfold Vnullptr; destruct Archi.ptr64; congruence.
  inv H0. congruence.
Qed.


Lemma parent_msk_def: forall s, match_stack s -> parent_msk s <> Vundef.
Proof.
  induction 1; simpl.
  unfold Vnullptr; destruct Archi.ptr64; congruence.
  inv H0. inversion H2; subst.
  congruence.
Qed.


Lemma lessdef_parent_sp:
  forall s v,
  match_stack s -> Val.lessdef (parent_sp s) v -> v = parent_sp s.
Proof.
  intros. inv H0. auto. exploit parent_sp_def; eauto. tauto.
Qed.

Lemma lessdef_parent_ra:
  forall s v,
  match_stack s -> Val.lessdef (parent_ra s) v -> v = parent_ra s.
Proof.
  intros. inv H0. auto. exploit parent_ra_def; eauto. tauto.
Qed.

Lemma lessdef_parent_msk:
  forall s v,
  match_stack s -> Val.lessdef (parent_msk s) v -> v = parent_msk s.
Proof.
  intros. inv H0. auto.
  exploit parent_msk_def; eauto. tauto.
Qed.



(*********************************************************************)
(*************** NOT USED  *******************************************)


Lemma encryptBlock_aux1 : forall n m msk,
      encrypt_blockC n m = Some msk ->
      encrypt_blockC n (next_mskA m) = Some (next_mskA msk).
  intros n.
  induction n.
  intros.
  simpl.
  inversion H; subst.
  reflexivity.
  intros.
  inversion H; subst.
  simpl.
  eapply IHn.
  assumption.
Qed.


Lemma encryptD_aux1 :
  forall n msk0 msk1,
      encrypt_blockD n msk0 = Some msk1 ->
      encrypt_blockD (Datatypes.S n) msk0
                    = next_msk msk1.
  intros.
  simpl.
  rewrite H.
  reflexivity.
Qed.


Lemma encrypt_aux1 :
  forall ofs blk msk
      (H: Ptrofs.intval ofs < Ptrofs.max_unsigned),
      encrypt_msk_blockD (Ptrofs.unsigned ofs) blk = Some msk ->
      encrypt_msk_blockD (Ptrofs.unsigned (Ptrofs.add ofs Ptrofs.one)) blk
                    = next_msk msk.
  unfold encrypt_msk_blockD.
  intros.
  rewrite add_to_nat_Lemma1.
  rewrite NatAdd_aux3.
  simpl.
  rewrite H0.
  reflexivity.

  eapply add_carry_aux1; auto.

  eapply modulus_aux1; auto.
Qed.


End MATCH_STACK.
